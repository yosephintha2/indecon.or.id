<?php

/**
 * The Template for displaying single project
 *
 * @author 		alispx
 * @version     1.0
 */

// <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
// <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
// <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header(); ?>
  
   <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>-->
  
	<style>
/*.collapsible {*/
/*  background-color: #777;*/
/*  color: white;*/
/*  cursor: pointer;*/
/*  padding: 18px;*/
/*  width: 100%;*/
/*  border: none;*/
/*  text-align: left;*/
/*  outline: none;*/
/*  font-size: 15px;*/
/*}*/

/*.active, .collapsible:hover {*/
/*  background-color: #555;*/
/*}*/

/*.contents {*/
/*  padding: 0 18px;*/
/*  display: none;*/
/*  overflow: hidden;*/
/*  background-color: #f1f1f1;*/
/*}*/
</style>

<style>
.containers {
    width:100%;
    /*border:1px solid #d3d3d3;*/
}
.containers div {
    width:100%;
}
.containers .headers {
    background-color:#d3d3d3;
    padding: 10px; 
    padding-left: 20px;
    cursor: pointer;
    font-weight: bold;
    margin-top: 5px;
    /*border:1px solid #ffffff;*/

}
.headers:hover {
  text-decoration: underline;
}
.containers .contents {
    /*display: none;*/
    border:1px solid #d3d3d3;
    padding : 5px; 
    padding-left: 20px;
}
</style>


	<div class="row content">

		<div class="grid-8 column post-content">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				<article <?php hybrid_post_attributes(); ?>>

					<h3 class="title-post"><?php the_title() ?></h3>
					<p class="date">
						<?php echo do_shortcode( '[entry-published before="Since "]' ); ?>
					</p>

					<div class="framebox">
						<?php get_the_image( array( 'size' => 'blog-thumbnail', 'link_to_post' => false ) ); ?>
					</div>

<?php
// if (isset($_GET['post_type'])) {
//   $pt = $_GET['post_type'];
// } else {
//   $pt = 0;
// }

// echo $pt;
?>
  
                    <?php
                    $duration = get_post_meta(get_the_ID(), '_duration', true);
                    $issues = get_post_meta(get_the_ID(), '_issues', true);
                    $location = get_post_meta(get_the_ID(), '_location', true);
                    $donor = get_post_meta(get_the_ID(), '_donor', true);
                    $latitude = get_post_meta(get_the_ID(), '_project_latitude', true);
                    $longitude = get_post_meta(get_the_ID(), '_project_longitude', true);
                    
        //             $location = '-';
        //             $arr_location = array();
        //             $terms = get_terms( 'location_project', 'hide_empty=1&order=DESC' );
        //             foreach ( $terms as $term ) :
						  //  $a = esc_attr($terms[0]->name);
						  //  $arr_location[] = $a;
				    // endforeach;
				    
				    // if(!empty($arr_location)){
				    //     $location = implode(', ', $arr_location);
				    // }
                    ?>
                    
                    <br>
                    <table style="font-weight: bold">
                        <?php if(!empty($duration)){ ?>
                        <tr>
                            <td width="20%">Duration</td><td width="5%"> : </td><td><?php echo $duration ?></td>
                        </tr>
                        <?php } if(!empty($issues)){ ?>
                        <tr>
                            <td width="20%">Issues</td><td width="5%"> : </td><td><?php echo $issues ?></td>
                        </tr>
                        <?php } if(!empty($location)){ ?>
                        <tr>
                            <td width="20%">Location</td><td width="5%"> : </td><td><?php echo $location ?></td>
                        </tr>
                        <?php } if(!empty($donor)){ ?>
                        <tr>
                            <td width="20%">Donor</td><td width="5%"> : </td><td><?php echo $donor ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                    <br>
					 <div class="entry-content">
						<?php the_content(); ?>
					 </div>

				</article>
				
			<?php endwhile; ?>
			<?php endif; ?>
		
		</div>
		
		<?php get_sidebar( 'primary' ); ?>

	</div>
	
	<script>
	$(".headers").click(function () {

    $header = $(this);
    //getting the next element
    $content = $header.next();
    //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
    $content.slideToggle(500, function () {
        //execute this after slideToggle is done
        //change text of header based on visibility of content div
        $header.text(function () {
            //change text based on condition
            //return $content.is(":visible") ? "-" : "+";
        });
    });

});

	</script>
	
<!--<script>-->
<!--var coll = document.getElementsByClassName("collapsible");-->
<!--var i;-->

<!--for (i = 0; i < coll.length; i++) {-->
<!--  coll[i].addEventListener("click", function() {-->
<!--    this.classList.toggle("active");-->
<!--    var contents = this.nextElementSibling;-->
<!--    if (contents.style.display === "block") {-->
<!--      contents.style.display = "none";-->
<!--    } else {-->
<!--      contents.style.display = "block";-->
<!--    }-->
<!--  });-->
<!--}-->
<!--</script>-->
<?php get_footer(); ?>