<?php

/**
 * The Template for displaying single network
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header(); ?>
	
	<div class="row content">

		<div class="grid-8 column post-content">
	
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				<article <?php hybrid_post_attributes(); ?>>

					<h3 class="title-post"><?php the_title() ?></h3>
					<p class="date">
						<?php echo do_shortcode( '[entry-published before="Since "]' ); ?>
					</p>

					<?php 
						$type 			= get_post_meta( get_the_ID(), 'member-type', false );
						$full_name 		= get_post_meta( get_the_ID(), 'full_name', false );
						$profession 	= get_post_meta( get_the_ID(), 'profession', false );
						$division 		= get_post_meta( get_the_ID(), 'division', false );
						$organization 	= get_post_meta( get_the_ID(), 'organization', false );
						$address 		= get_post_meta( get_the_ID(), 'full_address', false );
						$phone_number	= get_post_meta( get_the_ID(), 'phone_number', false );
						$website_url	= get_post_meta( get_the_ID(), 'website_url', false );
						$motivation		= get_post_meta( get_the_ID(), 'motivation', false );
						$description	= get_post_meta( get_the_ID(), 'description', false );
						$contact_of_pj	= get_post_meta( get_the_ID(), 'contact-of-pj', false );
						$address_of_pj	= get_post_meta( get_the_ID(), 'personal_address', false );
						$phone_of_pj	= get_post_meta( get_the_ID(), 'phone-of-pj', false );
						$email_of_pj	= get_post_meta( get_the_ID(), 'pj_email', false );
						$position_of_pj	= get_post_meta( get_the_ID(), 'pj_posision', false );
					 ?>
					 <div class="row">
					 	<div class="grid-12 column">
					 		<table id="table-network">
					 			<tr>
					 				<td valign="top" colspan="2" rowspan="7" width="35%"><?php get_the_image( array( 'size' => 'blog-thumbnail', 'link_to_post' => false ) ); ?></td>
					 				<td width="31%"><strong><?php _e( 'Nama Lengkap : ', 'indecon' ); ?></strong></td>
					 				<td><?php if ( ! empty( $full_name ) ) echo $full_name[0]; ?></td>
					 			</tr>
					 			<tr>
					 				<td><strong><?php _e( 'Profesi : ', 'indecon' ); ?></strong></td>
					 				<td><?php if ( ! empty( $profession ) ) echo $profession[0]; ?></td>
					 			</tr>
					 			<tr>
					 				<td><strong><?php _e( 'Bidang Gerak : ', 'indecon' ); ?></strong></td>
					 				<td><?php if ( ! empty( $division ) ) echo $division[0]; ?></td>
					 			</tr>
					 			<tr>
					 				<td><strong><?php _e( 'Oragnisasi Tempat Bekerja : ', 'indecon' ); ?></strong></td>
					 				<td><?php if ( ! empty( $organization ) ) echo $organization[0]; ?></td>
					 			</tr>
					 			<tr>
					 				<td><strong><?php _e( 'Alamat : ', 'indecon' ); ?></strong></td>
					 				<td><?php if ( ! empty( $address ) ) echo $address[0]; ?></td>
					 			</tr>
					 			<tr>
					 				<td><strong><?php _e( 'No tlp/fax/Handpone : ', 'indecon' ); ?></strong></td>
					 				<td><?php if ( ! empty( $phone_number ) ) echo $phone_number[0]; ?></td>
					 			</tr>
					 			<tr>
					 				<td><strong><?php _e( 'Website/blog (jika ada) : ', 'indecon' ); ?></strong></td>
					 				<td><?php if ( ! empty( $website_url ) ) echo $website_url[0]; ?></td>
					 			</tr>
					 		</table>
					 		<table>
					 			<tr>
					 				<td width="30%"><strong><?php _e( 'Motivasi bergabung: ', 'indecon' ); ?></strong></td>
					 				<td><?php if ( ! empty( $motivation ) ) echo $motivation[0]; ?></td>
					 			</tr>
					 			<tr>
					 				<td><strong><?php _e( 'Deskripsi singkat tentang anda : ', 'indecon' ); ?></strong></td>
					 				<td><?php if ( ! empty( $motivation ) ) echo $motivation[0]; ?></td>
					 			</tr>
					 			<tr>
					 				<td><strong><?php _e( 'Motivasi bergabung: ', 'indecon' ); ?></strong></td>
					 				<td><?php if ( ! empty( $motivation ) ) echo $motivation[0]; ?></td>
					 			</tr>
							</table>

							<?php if ( 'organisasi' == $type[0] ) : ?>
							<h3><?php _e( 'Oganisasi', 'indecon' ); ?></h3>
							<table>
								<tr>
									<td>
					 					<strong><?php _e( 'Nama Kontak : ', 'indecon' ); ?></strong>
					 				</td>
									<td><?php if ( ! empty( $contact_of_pj ) ) echo $contact_of_pj[0]; ?></td>
								</tr>
								<tr>
									<td><strong><?php _e( 'Jabatan di Lembaga : ', 'indecon' ); ?></strong></td>
									<td><?php if ( ! empty( $position_of_pj ) ) echo $position_of_pj[0]; ?></td>
								</tr>
								<tr>
									<td><strong><?php _e( 'Alamat Pribadi : ', 'indecon' ); ?></strong></td>
									<td><?php if ( ! empty( $address_of_pj ) ) echo $address_of_pj[0]; ?></td>
								</tr>
								<tr>
									<td><strong><?php _e( 'No tlp/fax/Handpone : ', 'indecon' ); ?></strong></td>
									<td><?php if ( ! empty( $phone_of_pj ) ) echo $phone_of_pj[0]; ?></td>
								</tr>
								<tr>
									<td><strong><?php _e( 'Email : ', 'indecon' ); ?></strong></td>
									<td colspan="3"><?php if ( ! empty( $email_of_pj ) ) echo $email_of_pj[0]; ?></td>
								</tr>
					 		</table>
							<?php endif; ?>
						</div>
					 </div>
				</article>
				
			<?php endwhile; ?>
			<?php endif; ?>
		
		</div>
		
		<?php get_sidebar( 'primary' ); ?>

	</div>

<?php get_footer(); ?>