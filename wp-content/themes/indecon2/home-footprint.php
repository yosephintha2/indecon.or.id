<?php

/**
 * The Template for displaying foot print
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="section section-pastle">
	<div class="row">
		<div class="grid-4 column">
			<h3><?php _e( 'FOOTPRINT', 'indecon' ); ?></h3>
			<p>
				<?php if ( indecon_option( 'footprint_content' ) ) {
					echo indecon_option( 'footprint_content' );
				} else {
					_e( 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in bookmarksgrove right.', 'indecon' );
				} ?>
			</p>
			<!--<a href="<?php echo get_permalink( indecon_option( 'footprint_target_page' ) ); ?>" class="button button-green"><?php _e( 'VIEW ALL FOOTPRINT', 'indecon' ); ?></a>-->
			<a href="https://www.indecon.or.id/en-footprints/" class="button button-green"><?php _e( 'VIEW ALL FOOTPRINT', 'indecon' ); ?></a>
		</div>
		<div class="grid-4 column">
			<a href="<?php echo esc_url( indecon_option( 'footprint_map_link' ) ); ?>">
				<?php if ( indecon_option( 'footprint_map_image' ) ) : ?>
					<img style="margin-top:50px;" src="<?php echo esc_url( indecon_option( 'footprint_map_image' ) ); ?>" alt="Footprint">
				<?php else : ?>
					<img style="margin-top:50px;" src="<?php echo get_template_directory_uri(); ?>/assets/img/img-footprint.png" alt="Footprint">
				<?php endif ?>
			</a>
		</div>
		<div class="grid-4 column">
			<a href="<?php echo esc_url( indecon_option( 'footprint_banner_link' ) ); ?>">
				<?php if ( indecon_option( 'footprint_banner_image' ) ) : ?>
					<img style="margin-top:50px;" src="<?php echo esc_url( indecon_option( 'footprint_banner_image' ) ); ?>" alt="Footprint">
				<?php else : ?>
					<img style="margin-top:10px;" src="<?php echo get_template_directory_uri(); ?>/assets/img/example/banner-1.jpg" alt="Learning Centre">
				<?php endif ?>
			</a>
		</div>
	</div>
</div>