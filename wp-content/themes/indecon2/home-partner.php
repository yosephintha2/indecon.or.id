<?php

/**
 * The Template for displaying partner section on of homepage
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="row section-partner">
	<div class="grid-12 column text-center">
		<div class="title-center">
			<h4><?php _e( 'OUR PARTNER', 'indecon' ); ?></h4>
		</div>

		<?php $partner_args = array(
				'post_type' 		=> 'partner',
				'posts_per_page'	=> 5,
				'orderby' 			=> 'date',
				'order' 			=> 'DESC'
			); 
		$partners = new WP_Query( $partner_args );
		?>

		<?php if ( $partners->have_posts() ) : ?> 

			<?php while ( $partners->have_posts() ) : $partners->the_post(); ?>
				
				<?php $partner_link = get_post_meta( get_the_ID(), '_partner_link', false ); ?>

				<?php if ( $partner_link ) { ?>
					<a href="<?php echo esc_url( $partner_link[0] ); ?>">
						<?php get_the_image( array( 'size' => 'full', 'link_to_post' => false ) ); ?>
					</a>
				<?php } else { 
					get_the_image( array( 'size' => 'full', 'link_to_post' => false ) ); 
				} ?>

			<?php endwhile; ?>

		<?php endif; ?>
		
	</div>
</div>