<?php 
/*
Template Name: Contact
*/
get_header(); ?>

	<div class="row content">
		<div class="grid-8 column post-content">
			<h3 class="title-gray"><?php _e( 'Contact', 'indecon' ); ?></h3>
			<h5 class="title-gray"><?php _e( 'Our Team', 'indecon' ); ?></h5>
			
			<div class="row">
			
			<?php 
				$args = array(
					'post_type' 		=> 'team',
					'posts_per_page'	=> 8,
					'order' 			=> 'DESC',
					'orderby' 			=> 'date'
					);

				$teams = new WP_Query( $args );

			if ( $teams->have_posts() ) : while ( $teams->have_posts() ) : $teams->the_post(); ?>

				<div class="grid-3-small">
					<div class="thumb-team text-center">
						<div class="infos-team">
							<?php get_the_image( array( 'size' => 'team-thumbnail', 'link_to_post' => false ) ); ?>
							<div class="name-team">
								<h4><?php the_title(); ?></h4>
								<p><?php indecon_post_meta( '_team_position' ); ?></p>
								
								<?php if ( get_post_meta( get_the_ID(), '_team_email', true ) ) : ?>
									<a href="mailto:<?php indecon_post_meta( '_team_email' ); ?>"><span class="icon-email"></span></a>
								<?php endif ?>
								<?php if ( get_post_meta( get_the_ID(), '_team_facebook', true ) ) : ?>
									<a href="http://facebook.com/<?php indecon_post_meta( '_team_facebook' ); ?>"><span class="icon-facebook"></span></a>
								<?php endif ?>
								<?php if ( get_post_meta( get_the_ID(), '_team_twitter', true ) ) : ?>
									<a href="http://twitter.com/<?php indecon_post_meta( '_team_twitter' ); ?>"><span class="icon-twitter"></span></a>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>

			<?php endwhile; ?>
			<?php endif; ?>
				
			</div>

			<hr>
			<h5 class="title-gray"><?php _e( 'Send us an email', 'indecon' ); ?></h5>
			
			<div class="row">
				<div class="grid-12 column">
				<?php echo do_shortcode( '[contact-form-7 id="38" title="Contact form 1"]' ); ?>
				</div>
			</div>

		</div>
		
		<!-- sidebar -->
		<?php get_sidebar( 'primary' ); ?>
	</div>

<?php get_footer(); ?>