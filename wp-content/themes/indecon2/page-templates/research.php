<?php
/*
  Template Name: Research
 */

get_header();
?>

<div class="row content">
    <div class="grid-8 column post-content">

        <h3 class="title-gray"><?php _e('Research', 'indecon'); ?></h3>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <p>
                    <?php get_the_image(array('size' => 'full', 'link_to_post' => false)); ?>
                </p>
                <?php the_content(); ?>

            <?php endwhile; ?>
        <?php endif; ?> 

        <?php $terms = get_terms('tahun_research', 'hide_empty=1&order=DESC'); ?>
        <ul id="myTab" class="nav nav-tabs" style="padding-top: 30px; margin-bottom:10px;">
            <?php
            $count_term = 1;
            $a = "";
            foreach ($terms as $term) :
                $a = esc_attr($terms[0]->name);
                if ($count_term == 1) {
                    ?>
                    <li class="active"><a href="#proj-<?php echo esc_attr($term->name); ?>"><?php echo esc_attr($term->name); ?></a></li>
                <?php } else { ?>

                    <li><a href="#proj-<?php echo esc_attr($term->name); ?>"><?php echo esc_attr($term->name); ?></a></li>
                <?php } ?>

                <?php
                $count_term++;
            endforeach;
            ?>
        </ul>
        <div class="tab-content">

            <?php foreach ($terms as $term) { ?>

                <?php
                $args = array(
                    'post_type' => 'research',
                    'posts_per_page' => 20,
                    'tahun_research' => $term->name,
                    'order' => 'DESC'
                );
                $count_post = 1;
                $active = "";
                $loop = new WP_Query($args);
                while ($loop->have_posts()) : $loop->the_post();
                    if ($a == esc_attr($term->name)) $active = "active";
                        else $active = "";
                        
                    if ($count_post == 1) {
                        ?>

                        <!--<div class="tab-pane active" id="proj-<?php echo esc_attr($term->name); ?>">-->
                        <div class="tab-pane <?php echo $active; ?>" id="proj-<?php echo esc_attr($term->name); ?>">
                            <ul class="list-line" style="list-style-type:none;color:white">

                                <?php
                                $list_args = array(
                                    'post_type' => 'research',
                                    'posts_per_page' => 20,
                                    'tahun_research' => $term->name,
                                    'order' => 'DESC'
                                );
                                $count_post = 1;

                                $lists = new WP_Query($list_args);
                                while ($lists->have_posts()) : $lists->the_post();
                                    ?>
                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                <?php endwhile; ?>

                            </ul>
                        </div>

                    <?php } else { ?>

                        <div class="tab-pane" id="proj-<?php echo esc_attr($term->name); ?>">

                            <ul class="list-line" style="list-style-type:none;color:white">
                                <?php
                                $list_args = array(
                                    'post_type' => 'research',
                                    'posts_per_page' => 20,
                                    'tahun_research' => $term->name,
                                    'order' => 'DESC'
                                );
                                $count_post = 1;

                                $lists = new WP_Query($list_args);
                                while ($lists->have_posts()) : $lists->the_post();
                                    ?>
                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                <?php endwhile; ?>
                            </ul>
                        </div>

                        <?php
                    }
                    ?>

                    <?php
                    $count_post++;
                endwhile;
                ?>

            <?php } ?>

        </div>
    </div>

    <!-- sidebar -->
    <?php get_sidebar('primary'); ?>
</div>

<?php get_footer(); ?>