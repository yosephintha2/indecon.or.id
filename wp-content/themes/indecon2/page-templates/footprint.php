<?php 
/*
Template Name: Foot Print
*/
get_header(); ?>

	<div class="row content">
		<div class="grid-12 column">
		
			<h3 class="title-gray"><?php _e( 'Footprint', 'indecon' ); ?></h3>
			
			<div class="footprint-wrapper">
				
				<?php $args = array( 
						'post_type' 		=> 'footprint',
						'posts_per_page' 	=> -1,
						'post_status' 		=> 'publish'
						); 
				$footprints = new WP_Query( $args );
				?>
				<?php if ( $footprints->have_posts() ) : ?>

					<?php while ( $footprints->have_posts() ) : $footprints->the_post(); ?>
						
					<?php 
						ob_start(); 

						$latitude 	= get_post_meta( get_the_ID(), '_footprint_latitude', false );
						$longitude 	= get_post_meta( get_the_ID(), '_footprint_longitude', false );
						$link 		= get_post_meta( get_the_ID(), '_footprint_link', false );
						if ( ! empty( $link ) ) {
							$footprint_link = '<span><a href="'.esc_url( $link[0] ).'">'.__( 'Read More', 'indecon' ).'</a></span>';
						} else {
							$footprint_link = '';
						}

						$footprint_content = str_replace("\r\n", "<br/>", $post->post_excerpt );
						$footprint_clean = str_replace( "'", "\\'", $footprint_content );

						if ( ! empty( $latitude ) && ! empty( $longitude ) ) {
							$footprint_latitude = $latitude[0];
							$footprint_longitude = $longitude[0];
						} else {
							$footprint_latitude = '-6.2297465';
							$footprint_longitude = '106.829518';
						}
						?>
						var contentString = '<p>'+'<?php echo $footprint_clean; ?>'+'</p>'+'<?php echo $footprint_link; ?>';
						map.addMarker({
							lat: <?php echo $footprint_latitude; ?>,
							lng: <?php echo $footprint_longitude; ?>,
							title: '<?php echo str_replace( ' ', '_', $post->post_title ); ?>',
							details: {
							  database_id: <?php echo get_the_ID(); ?>,
							  author: '<?php echo get_the_author(); ?>'
							},
							infoWindow: {
							  content: contentString,
							}
							
						  });
						
						<?php 

						$markers[] = ob_get_contents();

						ob_end_clean();
					?>
					<?php endwhile; ?>

				<?php endif; ?>
				
				<script type="text/javascript">
					$ = jQuery.noConflict();
					var map;
					$(document).ready(function(){
					  map = new GMaps({
						div: '#footprint-maps',
						lat: -2.548926,
						lng: 118.0148634,
						zoom: 5
					  });
					<?php 
						foreach ( $markers as $marker) {
							echo $marker;
						}
					 ?>
					});
				  </script>
				<div class="map-wrapper">
					<!-- Create Map -->
					<div id="footprint-maps" style="height: 500px;"></div>
				</div>

			</div>
				
		</div>

	</div>

<?php get_footer(); ?>