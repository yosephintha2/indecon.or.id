<?php 
/*
Template Name: Foot Print
*/
get_header(); ?>

	<div class="row content">
		<div class="grid-12 column">
		
			<h3 class="title-gray"><?php _e( 'Thumbnail', 'indecon' ); ?></h3>
			
			<div class="thumbnail-wrapper">
				
				<?php $args = array( 
						'post_type' 		=> 'thumbnail',
						'posts_per_page' 	=> -1,
						'post_status' 		=> 'publish'
						); 
				$thumbnails = new WP_Query( $args );
				?>
				<?php if ( $thumbnails->have_posts() ) : ?>

					<?php while ( $thumbnails->have_posts() ) : $thumbnails->the_post(); ?>
						
					<?php 
						ob_start(); 

						$latitude 	= get_post_meta( get_the_ID(), '_thumbnail_latitude', false );
						$longitude 	= get_post_meta( get_the_ID(), '_thumbnail_longitude', false );
						$link 		= get_post_meta( get_the_ID(), '_thumbnail_link', false );
						if ( ! empty( $link ) ) {
							$thumbnail_link = '<span><a href="'.esc_url( $link[0] ).'">'.__( 'Read More', 'indecon' ).'</a></span>';
						} else {
							$thumbnail_link = '';
						}

						$thumbnail_content = str_replace("\r\n", "<br/>", $post->post_excerpt );
						$thumbnail_clean = str_replace( "'", "\\'", $thumbnail_content );

						if ( ! empty( $latitude ) && ! empty( $longitude ) ) {
							$thumbnail_latitude = $latitude[0];
							$thumbnail_longitude = $longitude[0];
						} else {
							$thumbnail_latitude = '-6.2297465';
							$thumbnail_longitude = '106.829518';
						}
						?>
						var contentString = '<p>'+'<?php echo $thumbnail_clean; ?>'+'</p>'+'<?php echo $thumbnail_link; ?>';
						map.addMarker({
							lat: <?php echo $thumbnail_latitude; ?>,
							lng: <?php echo $thumbnail_longitude; ?>,
							title: '<?php echo str_replace( ' ', '_', $post->post_title ); ?>',
							details: {
							  database_id: <?php echo get_the_ID(); ?>,
							  author: '<?php echo get_the_author(); ?>'
							},
							infoWindow: {
							  content: contentString,
							}
							
						  });
						
						<?php 

						$markers[] = ob_get_contents();

						ob_end_clean();
					?>
					<?php endwhile; ?>

				<?php endif; ?>
				
				<script type="text/javascript">
					$ = jQuery.noConflict();
					var map;
					$(document).ready(function(){
					  map = new GMaps({
						div: '#thumbnail-maps',
						lat: -2.548926,
						lng: 118.0148634,
						zoom: 5
					  });
					<?php 
						foreach ( $markers as $marker) {
							echo $marker;
						}
					 ?>
					});
				  </script>
				<div class="map-wrapper">
					<!-- Create Map -->
					<div id="thumbnail-maps" style="height: 500px;"></div>
				</div>

			</div>
				
		</div>

	</div>

<?php get_footer(); ?>