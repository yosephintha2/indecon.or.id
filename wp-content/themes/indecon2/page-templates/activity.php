<?php
/*
  Template Name: Activities
 */
get_header();
?>

<style>
    h5{
        height: 40px
    }
    .thumb-network img{
        height: 200px;
    }
    .address{
        height: 120px;
    }
</style>
<div class="row content">
    <div class="grid-12 column">
        <h3 class="title-gray"><?php _e('Activities', 'indecon'); ?></h3>

        <?php
        // echo $paged;
        $temp = $wp_query;
        $wp_query = null;
        $wp_query = new WP_Query();
        $wp_query->query('showposts=24&post_type=activity' . '&paged=' . $paged);
        // var_dump($wp_query);
        ?>

        <?php if ($wp_query->have_posts()) : ?> 

            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php echo post_class(); ?>>

                    <div class="grid-4 column">
                        <div class="thumb-network">
                            <?php get_the_image(array('size' => 'network-thumbnail', 'link_to_post' => true)); ?>
                            <h5 class="title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
                            </h5>
                            <p class="address">
                                <?php echo wp_trim_words($post->post_content, 10); ?>
                                <br>
                                <a href="<?php the_permalink(); ?>" class="button button-small button-primary"><?php _e('View', 'indecon'); ?></a>
                            </p>
                        </div>
                    </div>

                </article>

            <?php endwhile; ?>
            <article>
                <div class="grid-12 column mix">
                    <?php get_template_part('pagination'); ?>
                </div>
            </article>
        <?php endif; ?>

        <?php
        $wp_query = null;
        $wp_query = $temp;  // Reset
        ?>

    </div>

</div>

<?php get_footer(); ?>