<?php 
/*
Template Name: Learning
*/
get_header(); // Loads the header.php template. ?>

	<div class="row content">

		<div class="grid-8 column post-content">
		
			<?php 

				if ( is_user_logged_in() ) {
					
					$learning_args = array(
							'post_type' 		=> 'learning',
							'posts_per_page'	=> 5,
							'order' 			=> 'DESC',
						);

				} else {
					
					$learning_args = array(
							'post_type' 		=> 'learning',
							'posts_per_page'	=> 5,
							'order' 			=> 'DESC',
							'meta_key' 			=> '_learning_type',
							'meta_value' 		=> 'public'
						);
				}

				$learning = new WP_Query( $learning_args );

			if ( $learning->have_posts() ) : ?> 

				<?php while ( $learning->have_posts() ) : $learning->the_post(); ?>
			
					<article <?php hybrid_post_attributes(); ?>>

						<h3 class="title-post"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<p class="date">
							<?php echo do_shortcode( '[entry-published]' ); ?> - 
							<?php echo do_shortcode( '[entry-author before="Posted by "]' ); ?>
						</p>

						<div class="framebox">
							<?php get_the_image( array( 'size' => 'learning-thumbnail', 'link_to_post' => false ) ); ?>
						</div>
						
						<p><?php echo wp_trim_words( $post->post_content, 55 ); ?></p>

					</article>
				
			<?php endwhile; ?>
				<?php get_template_part( 'pagination' ); ?>
			<?php else : ?>
				<?php get_template_part( 'not', 'found' ); ?>
			<?php endif; ?>

		</div>

		<?php get_sidebar( 'primary' ); ?>

	</div>

<?php get_footer(); // Loads the footer.php template. ?>