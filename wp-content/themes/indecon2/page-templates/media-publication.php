<?php
/*
  Template Name: Media and Publication
 */
get_header();
?>
<style>
    h5{
        height: 40px
    }
    .thumb-network img{
        height: 200px;
    }
    .address{
        height: 120px;
    }
</style>
<div class="row content">
    <div class="grid-12 column">

        <h3 class="title-gray"><?php _e('Gallery', 'indecon'); ?></h3>

        <button class="button button-primary filter" data-filter="all"><?php _e('All', 'indecon'); ?></button>
        <button class="button button-primary filter" data-filter=".article"><?php _e('Article', 'indecon'); ?></button>
        <button class="button button-primary filter" data-filter=".photos"><?php _e('Photo', 'indecon'); ?></button>
        <button class="button button-primary filter" data-filter=".videos"><?php _e('Video', 'indecon'); ?></button>

        <?php
        // echo $paged;
        $temp = $wp_query;
        $wp_query = null;
        $wp_query = new WP_Query();
        $wp_query->query('showposts=6&post_type=publication' . '&paged=' . $paged);
        ?>

        <?php if ($wp_query->have_posts()) : ?> 

            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                <?php
                $get_publication_categories = get_post_meta(get_the_ID(), '_publication_categories', true);
                if ('videos' == $get_publication_categories) {
                    $publication_category = 'videos';
                } else if ('photos' == $get_publication_categories) {
                    $publication_category = 'photos';
                } else {
                    $publication_category = 'article';
                }
                ?>

                <article id="post-<?php the_ID(); ?>" <?php echo post_class('publication-filter'); ?>>

                    <?php
                    $publication_categories = get_post_meta(get_the_ID(), '_publication_categories', true);

                    if ('videos' == $publication_categories) {
                        ?>

                        <div class="grid-4 column mix <?php echo $publication_category; ?>">
                            <div class="thumb-network">
                                <h5 class="title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
                                </h5>

                                <?php
                                $youtube_video_url = get_post_meta(get_the_ID(), '_youtube_video_url', true);
                                $video_url = esc_url($youtube_video_url);
                                echo do_shortcode("[video width='291' height='200' src='{$video_url}']");
                                ?>

                                <p class="address text-center">
                                    <?php echo wp_trim_words($post->post_content, 10); ?>
                                    <a href="<?php the_permalink(); ?>" class="button button-small button-primary"><?php _e('View', 'indecon'); ?></a>
                                </p>
                            </div>
                        </div>

                    <?php } else if ('photos' == $publication_categories) { ?>

                        <div class="grid-4 column mix <?php echo $publication_category; ?>">
                            <div class="thumb-network">
                                <h5 class="title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
                                </h5>

                                <?php get_the_image(array('size' => 'network-thumbnail', 'link_to_post' => true)); ?>

                                <p class="address text-center">
                                    <!-- <?php echo wp_trim_words($post->post_content, 10); ?> -->
                                    <a href="<?php the_permalink(); ?>" class="button button-small button-primary"><?php _e('View', 'indecon'); ?></a>
                                </p>
                            </div>
                        </div>

                    <?php } else { ?>

                        <div class="grid-4 column mix <?php echo $publication_category; ?>">
                            <div class="thumb-network">
                                <h5 class="title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
                                </h5>

                                <p class="address text-center">
                                    <!-- <?php echo wp_trim_words($post->post_content, 10); ?>
                                    <br> -->
                                    <a href="<?php the_permalink(); ?>" class="button button-small button-primary"><?php _e('View', 'indecon'); ?></a>
                                </p>
                            </div>
                        </div>

                    <?php }
                    ?>

                </article>

            <?php endwhile; ?>
            <article>
                <div class="grid-12 column mix">
                    <?php get_template_part('pagination'); ?>
                </div>
            </article>
        <?php endif; ?>

        <?php
        $wp_query = null;
        $wp_query = $temp;  // Reset
        ?>

    </div>	

</div>

</div>

<?php get_footer(); ?>