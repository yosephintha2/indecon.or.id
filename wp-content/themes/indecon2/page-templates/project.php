<?php 

/*
Template Name: Projects
*/


get_header(); ?>
<style>
ul{
    list-style-type:none !important;
}
</style>
	<div class="row content">
		<div class="grid-8 column post-content">
				
				<h3 class="title-gray"><?php _e( 'Project', 'indecon' ); ?></h3>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<p>
						<?php get_the_image( array( 'size' => 'full', 'link_to_post' => false ) ); ?>
					</p>
					<?php the_content(); ?>

				<?php endwhile; ?>
				<?php endif; ?> 

            <!--
            <h4 class="title-gray">By Location</h4>
            <div class="location-wrapper">
				
				<?php $args = array( 
						'post_type' 		=> 'project',
						'posts_per_page' 	=> -1,
						'post_status' 		=> 'publish',
						); 
				$locations = new WP_Query( $args );
				?>
				<?php if ( $locations->have_posts() ) : ?>

					<?php while ( $locations->have_posts() ) : $locations->the_post(); ?>
						
					<?php
                        
                        $location 	= get_post_meta( get_the_ID(), '_location', false );
						$latitude 	= get_post_meta( get_the_ID(), '_project_latitude', false );
						$longitude 	= get_post_meta( get_the_ID(), '_project_longitude', false );
		
		                if(!empty($latitude) && !empty($longitude)){

						
						if ( ! empty( $latitude ) && ! empty( $longitude ) ) {
							$location_latitude = $latitude[0];
							$location_longitude = $longitude[0];
						} else {
							$location_latitude = '-6.2297465';
							$location_longitude = '106.829518';
						}
						
						ob_start(); 
		
						?>
						
						var contentString = '<a href="<?php echo the_permalink() ?>">'+'<?php echo the_title() ?>'+
						'</a><br><span>'+'<?php echo $location[0]; ?>'+'</span>';
						map.addMarker({
							lat: <?php echo $location_latitude; ?>,
							lng: <?php echo $location_longitude; ?>,
						    title: '<?php echo $post->post_title ?>',
							details: {
							  database_id: <?php echo get_the_ID(); ?>,
							  author: '<?php echo get_the_author(); ?>'
							},
							infoWindow: {
							  content: contentString,
							}
							
						  });
						
						<?php 

						$markers[] = ob_get_contents();

						ob_end_clean();
						}
					?>
					<?php endwhile; ?>

				<?php endif; ?>
				
				<script type="text/javascript">
					$ = jQuery.noConflict();
					var map;
					$(document).ready(function(){
					  map = new GMaps({
						div: '#location-maps',
						lat: -2.548926,
						lng: 118.0148634,
						zoom: 5
					  });
					<?php 
						foreach ( $markers as $marker) {
							echo $marker;
						}
					 ?>
					});
				  </script>
				<div class="map-wrapper">
					<div id="location-maps" style="height: 350px;"></div>
				</div>

			</div>
			<br>
			-->
			
			<!--
			<h4 class="title-gray">By Issues</h4>
			
			<div class="issues-wrapper">
			    <div class="row">
			        <div class="grid-3 column" style="margin-bottom: 5px">
                        <a href="<?php echo esc_url( add_query_arg( 'issues', 'climate-change', site_url( '/projects/project-issues/' ) ) )?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/g.png" alt="Climate Change">
                        </a>   
                    </div>
			        <div class="grid-3 column" style="margin-bottom: 5px">
                        <a href="<?php echo esc_url( add_query_arg( 'issues', 'development-planning', site_url( '/projects/project-issues/' ) ) )?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/f.png" alt="Development Planning">
                        </a>   
                    </div>
			        <div class="grid-3 column" style="margin-bottom: 5px">
                        <a href="<?php echo esc_url( add_query_arg( 'issues', 'gender-youth', site_url( '/projects/project-issues/' ) ) )?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/e.png" alt="Gender & Youth">
                        </a>   
                    </div>
			        <div class="grid-3 column" style="margin-bottom: 5px">
                        <a href="<?php echo esc_url( add_query_arg( 'issues', 'local-economic-development', site_url( '/projects/project-issues/' ) ) )?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/d.png" alt="Local Economic Development">
                        </a>   
                    </div>
			        <div class="grid-3 column" style="margin-bottom: 5px">
                        <a href="<?php echo esc_url( add_query_arg( 'issues', 'tourism-conservation', site_url( '/projects/project-issues/' ) ) )?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/c.png" alt="Tourism & Conservation">
                        </a>   
                    </div>
			        <div class="grid-3 column" style="margin-bottom: 5px">
                        <a href="<?php echo esc_url( add_query_arg( 'issues', 'tourism-planning', site_url( '/projects/project-issues/' ) ) )?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/b.png" alt="Tourism Planning">
                        </a>   
                    </div>
                    <div class="grid-3 column" style="margin-bottom: 5px">
                        <a href="<?php echo esc_url( add_query_arg( 'issues', 'training-standard', site_url( '/projects/project-issues/' ) ) )?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/a.png" alt="Training & Standard">
                        </a>   
                    </div>
                </div>
			</div>
			-->
			
				<?php $terms = get_terms( 'tahun', 'hide_empty=1&order=DESC' ); ?>
				<ul id="myTab" class="nav nav-tabs" style="padding-top: 30px; margin-bottom:10px;">
					<?php 
						$count_term = 1;
						$a = "";
						foreach ( $terms as $term ) :
						    $a = esc_attr($terms[0]->name);
							if ( $count_term == 1 ) { ?>
					  			<li class="active"><a href="#proj-<?php echo esc_attr( $term->name ); ?>"><?php echo esc_attr( $term->name ); ?></a></li>
							<?php } else { ?>
					  			<li><a href="#proj-<?php echo esc_attr( $term->name ); ?>"><?php echo esc_attr( $term->name ); ?></a></li>
							<?php } ?>

					<?php $count_term++; endforeach; ?>
				</ul>
				<div class="tab-content">

					<?php foreach ( $terms as $term ) { ?>
						
						<?php $args = array( 
									'post_type' 		=> 'project', 
									'posts_per_page'	=> 20,
									'tahun_project'		=> $term->name,
									'order' 			=> 'DESC' 
									);
						$count_post = 1;
						$active = "";
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); 
                            if ($a == esc_attr($term->name)) $active = "active";
                                else $active = "";
                                
							if ( $count_post == 1 ) { ?>

                                <div class="tab-pane <?php echo $active; ?>" id="proj-<?php echo esc_attr($term->name); ?>">
									<ul class="list-line" style="list-style-type:none;color:white">

										<?php $list_args = array( 
														'post_type' 		=> 'project', 
														'posts_per_page'	=> 20,
														'tahun_project'		=> $term->name,
														'order' 			=> 'DESC' 
														);
											$count_post = 1;
											
											$lists = new WP_Query( $list_args );
											while ( $lists->have_posts() ) : $lists->the_post(); ?>
									  			<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
									  		<?php endwhile; ?>

									</ul>
								</div>

							<?php } else { ?>

								<div class="tab-pane" id="proj-<?php echo esc_attr( $term->name ); ?>">
									<ul class="list-line" style="list-style-type:none;color:white">
									  	<?php $list_args = array( 
														'post_type' 		=> 'project', 
														'posts_per_page'	=> 20,
														'tahun_project' 	=> $term->name,
														'order' 			=> 'DESC' 
														);
											$count_post = 1;
											
											$lists = new WP_Query( $list_args );
											while ( $lists->have_posts() ) : $lists->the_post(); ?>
									  			<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
									  		<?php endwhile; ?>
									</ul>
								</div>
								
							<?php } ?>
			
						<?php $count_post++; endwhile; ?>

					<?php } ?>
					
				 </div>
		</div>
		
		<!-- sidebar -->
		<?php get_sidebar( 'project' ); ?>
	</div>

<?php get_footer(); ?>