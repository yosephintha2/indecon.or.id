<?php
/*
  Template Name: Activities
 */
get_header();
?>

<style>
    h5{
        height: 40px
    }
    .thumb-network img{
        height: 200px;
    }
    .address{
        height: 120px;
    }
</style>

<?php
$my_issues = get_query_var( 'issues' );
if($my_issues == 'climate-change')
    $my_issues = 'Climate Change';
else if($my_issues == 'development-planning')
    $my_issues = 'Development Planning';
else if($my_issues == 'gender-youth')
    $my_issues = 'Gender & Youth';
else if($my_issues == 'local-economic-development')
    $my_issues = 'Local Economic Development';
else if($my_issues == 'tourism-conservation')
    $my_issues = 'Tourism & Conservation';
else if($my_issues == 'tourism-planning')
    $my_issues = 'Tourism Planning';
else if($my_issues == 'training-standard')
    $my_issues = 'Training & Standard';

?>
<div class="row content">
    <div class="grid-12 column">
        <h3 class="title-gray"><?php _e('Project By Issues - ' . $my_issues, 'indecon'); ?></h3>

        <?php
        // echo $paged;
        $temp = $wp_query;
        $wp_query = null;
        $wp_query = new WP_Query();
        $wp_query->query('showposts=24&post_type=project' . '&paged=' . $paged);
        // var_dump($wp_query);
        
         $i = 0;
        ?>

        <?php if ($wp_query->have_posts()) : ?> 

            <?php while ($wp_query->have_posts()) : $wp_query->the_post();

                $issues = get_post_meta(get_the_ID(), '_issues', true);
               
                if ($my_issues == $issues) { ?>
                    <article id="post-<?php the_ID(); ?>" <?php echo post_class(); ?>>

                    <div class="grid-4 column">
                        <div class="thumb-network">
                            <?php get_the_image(array('size' => 'network-thumbnail', 'link_to_post' => true)); ?>
                            <h5 class="title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
                            </h5>
                            <p class="address">
                                <?php echo wp_trim_words($post->post_content, 10); ?>
                                <br>
                                <a href="<?php the_permalink(); ?>" class="button button-small button-primary"><?php _e('View', 'indecon'); ?></a>
                            </p>
                        </div>
                    </div>

                </article>

            <?php 
                $i++;
                }
                endwhile; 
                
                if($i == 0) echo 'No Data';
                ?>
            <article>
                <div class="grid-12 column mix">
                    <?php get_template_part('pagination'); ?>
                </div>
            </article>
        <?php endif; ?>

        <?php
        $wp_query = null;
        $wp_query = $temp;  // Reset
        ?>

    </div>

</div>

<?php get_footer(); ?>