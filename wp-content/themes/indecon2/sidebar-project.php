<?php

/**
 * The Template for displaying sidebar primary
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php if ( is_active_sidebar( 'primary' ) ) { ?>

	<!-- sidebar -->
	<div class="grid-4 column sidebar">
		<div class="distance">
			<div class="box section-pastle text-center">
				test
			</div>
		</div>

		<?php //dynamic_sidebar( 'primary' ); ?>
	
	</div><!-- /sidebar -->

<?php } ?>