		<?php include "header.php" ?>
		<?php include "headnav.php" ?>
		
		<div class="row content">
				<div class="grid-8 column post-content">

						<h3 class="title-gray">Research</h3>
						<p><img src="assets/img/example/burung.jpg" alt="burung"></p>
						<p>Indecon is actively involved in the development of ecotourism by conducting research in the field of ecotourism. These research were focusing on topics related to development of ecotourism and sustainable tourism which comprise tourism assessment; market analysis; business, organization and tourism impact studies. Some of the examples include:</p>
						<ul id="myTab" class="nav nav-tabs" style="padding-top: 30px; margin-bottom:10px;">
							<li class="active"><a href="#proj-2010">2010</a></li>
							<li><a href="#proj-2009">2009</a></li>
							<li><a href="#proj-2008">2008</a></li>
							<li><a href="#proj-2007">2007</a></li>
							<li><a href="#proj-2006">2006</a></li>
							<li><a href="#proj-2005">2005</a></li>
							<li><a href="#proj-2004">2004</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="proj-2010">
								<ul class="list-line">
								  	<li>Tourism Development of Environmental Friendly and Pro-Community Tourism; carried out in collaboration with FFI Aceh.</li>
								  	<li>Feasibility Study on Tourism Development in Gapang for IboihMakmur Co-op, Sabang, carried out in collaboration with FFI Aceh.</li>
								  	<li>Preliminary Identification of Tourism Attraction Potencies in Wakatobi, carried out in collaboration with Joint Program TNC-WWF.</li>
								</ul>
							</div>
							<div class="tab-pane" id="proj-2009">
								<ul class="list-line">
								  	<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</li>
								  	<li>Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </li>
								  	<li>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </li>
								</ul>
							</div>
							<div class="tab-pane" id="proj-2008">
								<ul class="list-line">
								  	<li>Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </li>
								  	<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</li>
								  	<li>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </li>
								</ul>
							</div>
							<div class="tab-pane" id="proj-2007">
								<ul class="list-line">
									<li>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </li>
								  	<li>Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </li>
								  	<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</li>
								</ul>
							</div>
							<div class="tab-pane" id="proj-2006">
								<ul class="list-line">
									<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</li>
									<li>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </li>
								  	<li>Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </li>
								</ul>
							</div>
							<div class="tab-pane" id="proj-2005">
								<ul class="list-line">
									<li>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </li>
									<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</li>
								  	<li>Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </li>
								</ul>
							</div>
							<div class="tab-pane" id="proj-2004">
								<ul class="list-line">
									<li>Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </li>
									<li>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </li>
									<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</li>
								</ul>
							</div>
						 </div>
				</div>
				
				<!-- sidebar -->
				<div class="grid-4 column sidebar">
						<div class="distance">
							<div class="box section-pastle text-center">
								<h4>JOIN OUR COMMUNITY</h4>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
								<a class="button button-block button-primary">join now</a>
							</div>
						</div>
						<div class="distance text-center">
							<p>- OR -<br>You can get news for the latest update </p>
							<input style="margin-bottom: 0" type="email" placeholder="Email address ..." class="input-block">
							<button type="submit" class="button button-small button-green">Subscribe</button>
						</div>
						<div class="distance">						
							<h4 class="title-post title-gray">Archives</h4>
							<ul class="unstyled list-post">
								<li><a href="#">September 2013 <span class="muted">(10)</span></a></li>
								<li><a href="#">October 2013 <span class="muted">(6)</span></a></li>
								<li><a href="#">November 2013 <span class="muted">(8)</span></a></li>
								<li><a href="#">December 2013 <span class="muted">(5)</span></a></li>
							</ul>
						</div>
						<div class="distance">
							<a href="#"><img style="margin-top:10px;" src="assets/img/example/banner-1.jpg" alt="Learning Centre"></a>
						</div>
				</div><!-- /sidebar -->
		</div>
		
		
        
		<?php include "footer.php" ?>
		<script>
			    $('#myTab a').click(function (e) {
					e.preventDefault()
					$(this).tab('show')
  				})
		</script>
		
</body>
</html>