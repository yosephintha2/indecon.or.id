		<footer class="footer">
			<div class="row">
				<div class="grid-6 column">
					<div class="nav-bottom">
						<a href="#">About Us</a> 
						<a href="contact.php">Contact Us</a> 
						<a href="#">FAQ</a> 
					</div>
					<p>&copy; 2014 <a href="#">Indecon</a>. All rigths reserved.</p>
				</div>
				<div class="grid-6 column">
					<div class="socmed">
						<a href="#"><span class="icon-facebook"></span></a>
					    <a href="#"><span class="icon-twitter"></span></a>
					    <a href="#"><span class="icon-email"></span></a>
					    <a href="#"><span class="icon-rss"></span></a>
					</div>
					<form class="sub-foot">
						<input type="text" class="grid-5 clean" placeholder="Enter your email address">
						<button type="submit" class="button button-small button-primary">Subscribe</button>
					</form>
				</div>
			</div>
		</footer>
		
		<script src="<?php echo $urlpath; ?>/assets/js/jquery.1.8.1.min.js"></script>
		<script src="<?php echo $urlpath; ?>/assets/js/foundation.js"></script>
		<script src="<?php echo $urlpath; ?>/assets/js/foundation.forms.js"></script>
		<script src="<?php echo $urlpath; ?>/assets/js/bootstrap.js"></script>
		<script>$(document).foundation();</script>
		<script>
			$(function(){
			    $('.navtop > .dropdown').hover(function() {
			        $(this).addClass('open');
			    }, function() {
			        $(this).removeClass('open');
			    });

			    //$('#main-slider').liquidSlider();
				// Simple slider
				var slideCount = $('#slider ul li').length;
				var slideWidth = $('#slider ul li').width();
				var slideHeight = $('#slider ul li').height();
//				var sliderUlWidth = slideCount * slideWidth;
				var sliderUlWidth = slideCount * 100;
				var liWidth = slideWidth / slideCount;
				
				//$('#slider').css({ width: slideWidth, height: slideHeight });
				
				//$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
				$('#slider ul').css({ width: sliderUlWidth + "%"});
				$('#slider ul li').css({ width: 100 / slideCount + "%"});
				
			    $('#slider ul li:last-child').prependTo('#slider ul');
			
			    function moveLeft() {
			        $('#slider ul').animate({
			            left: + slideWidth
			        }, 500, function () {
			            $('#slider ul li:last-child').prependTo('#slider ul');
			            $('#slider ul').css('left', '');
			        });
			    };
			
			    function moveRight() {
			        $('#slider ul').animate({
			            left: - slideWidth
			        }, 500, function () {
			            $('#slider ul li:first-child').appendTo('#slider ul');
			            $('#slider ul').css('left', '');
			        });
			    };
			
			    $('a.control_prev').click(function () {
			        moveLeft();
			    });
			
			    $('a.control_next').click(function () {
			        moveRight();
			    });
			});
		</script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->