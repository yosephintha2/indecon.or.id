		<?php include "header.php" ?>
		<?php include "headnav.php" ?>
		
		<div class="row content">
				<div class="section-fixcenter text-center">
					<h3 class="title-gray">Login Member</h3>
					<label class="text-left">Username:</label>
					<input type="text" class="input-block">
					<label class="text-left">Password:</label>
					<input type="password" class="input-block">
					<div class="clearfix">
						<label class="checkbox pull-left">
							<input type="checkbox"> Remember me 
							</label>
						<div class="pull-right"><a class="link-green" href="#">Forgot password?</a></div>
					</div>
					<button class="button button-primary">Login Account</button>
				</div>
		</div>
		
		
        
		<?php include "footer.php" ?>
		
</body>
</html>