		<?php include "header.php" ?>
		<?php include "headnav.php" ?>
		
		<div class="row content">
				<div class="grid-8 column post-content">

						<h3 class="title-post">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor</h3>
						<p class="date">
							<time>January 11, 2014</time> - <span>Posted by <a href="#"><strong>Pildasi Batubara</strong></a></span>
						</p>
						<div class="wp-caption">
							<img src="assets/img/example/burung.jpg" alt="burung">
							<p class="caption">This class wp-caption images. Photo credit by <strong>Name Photographer</strong>.</p>
						</div>
						<p><img src="assets/img/example/burung.jpg" alt="burung"></p>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a <strong>paradisematic</strong> country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far <a href="#">World of Grammar</a>.</p>
						<p><strong>The Big Oxmox advised her not to do so</strong>, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
						<!--<h5>Shortcode Video</h5>
						<figure>
							<iframe src="//player.vimeo.com/video/71427664" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>
						</figure>
						<h5>Shortcode Audio</h5>
						<p>
							<iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/115865985&amp;color=ff6600&amp;auto_play=false&amp;show_artwork=false" height="100" width="100%" frameborder="no" scrolling="no"></iframe>
						</p>-->
						<h5>Shortcode Gallery</h5>
						<div class="row gallery">
							<div class="grid-3 column">
								<a href="#"><img src="assets/img/example/thumb-gallery.jpg" alt="burung"></a>
							</div>
							<div class="grid-3 column">
								<a href="#"><img src="assets/img/example/thumb-gallery.jpg" alt="burung"></a>
							</div>
							<div class="grid-3 column">
								<a href="#"><img src="assets/img/example/thumb-gallery.jpg" alt="burung"></a>
							</div>
							<div class="grid-3 column">
								<a href="#"><img src="assets/img/example/thumb-gallery.jpg" alt="burung"></a>
							</div>
							<div class="grid-3 column">
								<a href="#"><img src="assets/img/example/thumb-gallery.jpg" alt="burung"></a>
							</div>
							<div class="grid-3 column">
								<a href="#"><img src="assets/img/example/thumb-gallery.jpg" alt="burung"></a>
							</div>
							<div class="grid-3 column">
								<a href="#"><img src="assets/img/example/thumb-gallery.jpg" alt="burung"></a>
							</div>
							<div class="grid-3 column">
								<a href="#"><img src="assets/img/example/thumb-gallery.jpg" alt="burung"></a>
							</div>
						</div>
				</div>
				
				<!-- sidebar -->
				<div class="grid-4 column sidebar">
						<div class="distance">
							<div class="box section-pastle text-center">
								<h4>JOIN OUR COMMUNITY</h4>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
								<a class="button button-block button-primary">join now</a>
							</div>
						</div>
						<div class="distance text-center">
							<p>- OR -<br>You can get news for the latest update </p>
							<input style="margin-bottom: 0" type="email" placeholder="Email address ..." class="input-block">
							<button type="submit" class="button button-small button-green">Subscribe</button>
						</div>
						<div class="distance">
							<h4 class="title-post title-gray">News</h4>
							<ul class="unstyled list-post">
								<li><a href="#">When she reached the first hills of the Italic Mountains.</a></li>
								<li><a href="#">A small river named Duden flows by their place and supplies it with the necessary</a></li>
								<li><a href="#">It is a paradisematic country, in which roasted parts of sentences</a></li>
								<li><a href="#">One day however a small line of blind text by the name of Lorem Ipsum</a></li>
							</ul>
						</div>
						<div class="distance">						
							<h4 class="title-post title-gray">Archives</h4>
							<ul class="unstyled list-post">
								<li><a href="#">September 2013 <span class="muted">(10)</span></a></li>
								<li><a href="#">October 2013 <span class="muted">(6)</span></a></li>
								<li><a href="#">November 2013 <span class="muted">(8)</span></a></li>
								<li><a href="#">December 2013 <span class="muted">(5)</span></a></li>
							</ul>
						</div>
						<div class="distance">
							<a href="#"><img style="margin-top:10px;" src="assets/img/example/banner-1.jpg" alt="Learning Centre"></a>
						</div>
				</div><!-- /sidebar -->
		</div>
		
		
        
		<?php include "footer.php" ?>
		<script>
		$(function() {
				var $allVideos = $("iframe[src^='http://player.vimeo.com'], iframe[src^='http://www.youtube.com'], object, embed"),
				$fluidEl = $("figure");
				$allVideos.each(function() {
				  $(this)
				    // jQuery .data does not work on object/embed elements
				    .attr('data-aspectRatio', this.height / this.width)
				    .removeAttr('height')
				    .removeAttr('width');				
				});
				
				$(window).resize(function() {
				  var newWidth = $fluidEl.width();
				  $allVideos.each(function() {
				    var $el = $(this);
				    $el
				        .width(newWidth)
				        .height(newWidth * $el.attr('data-aspectRatio'));
				  });
				}).resize();
		});
		</script>
		
</body>
</html>