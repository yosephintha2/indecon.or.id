		<?php include "header.php" ?>
		<?php include "headnav-home.php" ?>
		
		<!-- Carousel ================================================== -->
		<div id="mySlide" class="carousel slide">
			<ol class="carousel-indicators">
                <li class="" data-slide-to="0" data-target="#mySlide"></li>
                <li data-slide-to="1" data-target="#mySlide" class="active"></li>
				<li data-slide-to="2" data-target="#mySlide" class=""></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active">
					<img alt="" src="assets/img/example/slide-1.jpg">
					<div class="row">
							<div class="carousel-caption">
								<h3><a href="detail.php">Far far away, behind the word mountains.</a></h3>
								<p><a href="#">A small river named Duden flows by their place and supplies it with the ...</a></p>
							</div>
					</div>
				</div>
				<div class="item">
					<img alt="" src="assets/img/example/slide-2.jpg">
					<div class="row">
							<div class="carousel-caption">
								<h3><a href="detail.php">Loreng baju abri merah warna seragamnya.</a></h3>
								<p><a href="detail.php">A small river named Duden flows by their place and supplies it with the ...</a></p>
							</div>
					</div>
				</div>
				<div class="item">
					<img alt="" src="assets/img/example/slide-3.jpg">
					<div class="row">
							<div class="carousel-caption">
								<h3><a href="detail.php">Far far away, behind the word mountains.</a></h3>
								<p><a href="detail.php">A small river named Duden flows by their place and supplies it with the ...</a></p>
							</div>
					</div>
				</div>
			</div>
			<a class="left carousel-control" href="#mySlide" data-slide="prev">&lsaquo;</a>
			<a class="right carousel-control" href="#mySlide" data-slide="next">&rsaquo;</a>
		</div><!-- /.carousel -->
		
		<div class="section section-pastle">
			<div class="row">
				<div class="grid-4 column">
					<div class="box box-green text-center">
						<h4>JOIN OUR COMMUNITY</h4>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						<a class="button button-block button-primary">join now</a>
					</div>
				</div>
				<div class="grid-8 column">
					<h3 class="title-gray" style="margin: 0;">OUR RESEARCH</h3>
					<a class="control_next">Next ></a>
					<a class="control_prev">< Prev</a>
					<div id="slider">
						<div class="wrap-slider" style="overflow: hidden; position: relative; float:left;">
							<ul>
								<li>
									<div class="thumb-slide"><a href="#"><img src="assets/img/example/img-thumb-1.png" alt="Thumbnail"></a></div>
									<div class="caption">
								    	<h4>When she reached the first hills</h4>
								    	<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. <a class="read-more" href="detail.php">Read More</a></p>
									</div>
								</li>
								<li>
									<div class="thumb-slide"><a href="#"><img src="assets/img/example/img-thumb-1.png" alt="Thumbnail"></a></div>
									<div class="caption">
								    	<h4>When she reached the first hills</h4>
								    	<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. <a class="read-more" href="detail.php">Read More</a></p>
									</div>
								</li>
								<li>
									<div class="thumb-slide"><a href="#"><img src="assets/img/example/img-thumb-1.png" alt="Thumbnail"></a></div>
									<div class="caption">
								    	<h4>When she reached the first hills</h4>
								    	<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. <a class="read-more" href="detail.php">Read More</a></p>
									</div>
								</li>
							</ul>
						</div>  
					</div>
				</div>
			</div>
		</div>
		
		<div class="section">
			<div class="row">
				<div class="grid-4 column">
					<h5 class="title-gray">News <a class="link-orange pull-right" href="#">+ view all</a></h5>
					<a href="detail.php"><img src="assets/img/example/img-thumb-1.png" alt="Thumbnail"></a>
					<h5 class="title"><a href="detail.php">Nullam dictum felis eu pede mollis pretium cascade.</a></h5>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. <a class="read-more" href="detail.php">Read More</a></p>
				</div>
				<div class="grid-4 column">
					<h5 class="title-gray">Activities <a class="link-orange pull-right" href="#">+ view all</a></h5>
					<a href="detail.php"><img src="assets/img/example/img-thumb-2.png" alt="Thumbnail"></a>
					<h5 class="title"><a href="detail.php">Donec sodales sagittis magna, sed consequat.</a></h5>
					<p>Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis.<a class="read-more" href="detail.php">Read More</a></p>
				</div>
				<div class="grid-4 column">
					<h5 class="title-gray">Media & Publication <a class="link-orange pull-right" href="#">+ view all</a></h5>
					<a href="detail.php"><img src="assets/img/example/img-thumb-3.png" alt="Thumbnail"></a>
					<h5 class="title"><a href="detail.php">GREEN CONSTRUCTION.</a></h5>
					<p>Donec et nulla in diam viverra vehicula at eu dolor. Pellentesque pellentesque accumsan massa sed sagittis. Sed placerat, sapien fringilla feugiat pharetra, justo quam auctor sapien, id consectetur. <a class="read-more" href="detail.php">Read More</a></p>
				</div>
			</div>
		</div>
		
		<div class="section section-pastle">
			<div class="row">
				<div class="grid-4 column">
					<h3>FOOTPRINT</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in bookmarksgrove right.</p>
					<a href="footprint.php" class="button button-green">VIEW ALL FOOTPRINT</a>
				</div>
				<div class="grid-4 column">
					<a href="#"><img style="margin-top:50px;" src="assets/img/img-footprint.png" alt="Footprint"></a>
				</div>
				<div class="grid-4 column">
					<a href="#"><img style="margin-top:10px;" src="assets/img/example/banner-1.jpg" alt="Learning Centre"></a>
				</div>
			</div>
		</div>
		
		<div class="row section-partner">
			<div class="grid-12 column text-center">
				<div class="title-center">
					<h4>OUR PARTNER</h4>
				</div>
				<a href="#"><img src="assets/img/partner-visitindo.png" alt="Visit Indonesia"></a>
				<a href="#"><img src="assets/img/partner-greentravel.png" alt="Green Travel Market"></a>
				<a href="#"><img src="assets/img/partner-ieta.png" alt="Indonesian Ecotour Alliance"></a>
				<a href="#"><img src="assets/img/partner-ties.png" alt="The International Ecotourism Society"></a>
				<a href="#"><img src="assets/img/partner-iet.png" alt="Indonesia Eco Travel"></a>
			</div>
		</div>
		
        
		<?php include "footer.php" ?>
		<script>
	      !function ($) {
	        $(function(){
	          // carousel function
	          $('#mySlide').carousel()
	        })
	      }(window.jQuery)
	      $.each( jQuery('.carousel .item'), function( i, val ) {
		    $(this).css('background-image','url('+$(this).find('img').attr('src')+')').css('background-size','cover').find('img').css('visibility','hidden');
		  });
		</script>
		
</body>
</html>