<?php

/**
 * The Template for displaying not found content
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

	<article <?php hybrid_post_attributes(); ?>>

		<header class="entry-header">
			<h1 class="entry-title"><?php _e( 'Nothing found', 'indecon' ); ?></h1>
		</header>

		<div class="entry-content">
			<p><?php _e( 'Apologies, but no entries were found.', 'indecon' ); ?></p>
		</div><!-- .entry-content -->

	</article><!-- .hentry .error -->