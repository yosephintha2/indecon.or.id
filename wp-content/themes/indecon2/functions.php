<?php
/**
 * Spx Core Functions
 *
 * @package    SPXframework
 * @subpackage Functions
 * @version    1.0
 * @since      1.0
 * @author     Alispx
 */

/* Load the core theme framework. */
require_once( trailingslashit( get_template_directory() ) . 'framework/library/hybrid.php' );
new Hybrid();

require_once( trailingslashit( get_template_directory() ) . 'inc/setup.php' );

/* Load the theme option framework. */
require_once( trailingslashit( get_template_directory() ) . 'framework/options/bootstrap.php' );


/**
 * Registers sidebars.
 * 
 * @since 1.0
 */
add_action( 'widgets_init', 'tokokoo_register_extra_sidebars', 15 );
function tokokoo_register_extra_sidebars() {
	
	register_sidebar(
		array(
			'id' 			=> 'primary',
			'name' 			=> __( 'Primary', 'indecon' ),
			'description' 	=> __( 'The widget area loaded on the blog page.', 'indecon' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s widget-%2$s distance">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="title-post title-gray">',
			'after_title'   => '</h4>'
		)
	);

}

add_action( 'widgets_init', 'tokokoo_register_extra_sidebars_project', 15 );
function tokokoo_register_extra_sidebars_project() {
	
	register_sidebar(
		array(
			'id' 			=> 'project',
			'name' 			=> __( 'Project', 'indecon' ),
			'description' 	=> __( 'The widget area loaded on the project page.', 'indecon' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s widget-%2$s distance">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="title-post title-gray">',
			'after_title'   => '</h4>'
		)
	);

}

/** 
 * Funcion Change teheme option setting name by filter
 * 
 * @author alispx
 **/
// options
$tmpl_opt  = get_template_directory() . '/inc/option.php';
$theme_options = new VP_Option(array(
	'is_dev_mode'           => false,                                  // dev mode, default to false
	'option_key'            => 'indecon_option',                           // options key in db, required
	'page_slug'             => 'indecon_option',                           // options page slug, required
	'template'              => $tmpl_opt,                              // template file path or array, required
	'menu_page'             => 'themes.php',                           // parent menu slug or supply `array` (can contains 'icon_url' & 'position') for top level menu
	'use_auto_group_naming' => true,                                   // default to true
	'use_util_menu'         => true,                                   // default to true, shows utility menu
	'minimum_role'          => 'edit_theme_options',                   // default to 'edit_theme_options'
	'layout'                => 'fixed',                                // fluid or fixed, default to fixed
	'page_title'            => __( 'Theme Options', 'indecon' ), // page title
	'menu_label'            => __( 'Theme Options', 'indecon' ), // menu label
));

add_action( 'after_setup_theme', 'indecon_image_size', 30 );
function indecon_image_size() {
	add_image_size( 'small', 50, 50, true );
	add_image_size( 'blog-tiny', 221, 124, true );
	add_image_size( 'blog-small', 303, 171, true );
	add_image_size( 'blog-thumbnail', 637, 358, true );
	add_image_size( 'network-thumbnail', 301, 169, true );
	add_image_size( 'thumbnail-thumbnail', 303, 175, true );
	add_image_size( 'team-thumbnail', 137, 137, true );
	add_image_size( 'learning-thumbnail', 637, 358, true );
	add_image_size( 'banner-widget', 273, 175, true );
}

add_action( 'admin_head', 'indecon_metabox_admin_script', 80 );
function indecon_metabox_admin_script() {
	?>
	
	<script>
		jQuery(document).ready(function($) {
			if($('#_publication_categories').val()=="photos"){$("#gallery-metabox").slideDown();}else{$("#gallery-metabox").slideUp();}
			if($('#_publication_categories').val()=="videos"){$("#publication_2").slideDown();}else{$("#publication_2").slideUp();}

			$('#_publication_categories').change(function(){
				if($(this).val()=="photos"){
					$("#gallery-metabox").slideDown();
				}else{
					$("#gallery-metabox").slideUp();
				}
			});

			$('#_publication_categories').change(function(){
				if($(this).val()=="videos"){
					$("#publication_2").slideDown();
				}else{
					$("#publication_2").slideUp();
				}
			});


		});
	</script>

<?php }

// Pagination


    function id_pagination() {
    global $wp_query;
    $big = 999999999;
    //$big = 10;
    $paged = paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
	'prev_next'          => true,
	'prev_text'          => __('Sebelumnya'),
	'next_text'          => __('Selanjutnya'),
	'type'               => 'list',
	'add_fragment'       => '',
	'before_page_number' => '',
	'after_page_number'  => '',
        'total' => $wp_query->max_num_pages
    ));
// Replace style bawaan, sesuaikan dengan class  pada CSS Anda.
    $arr = array(
    "<ul class='page-numbers'>" => '<ul class="halaman">',
    '<li>' => '<li class="list-halaman">',
    "'" => '"'
    );
    echo strtr($paged, $arr);
}



/**
 * Prevent plugin update
 *
 * @return void
 * @author alispx
 **/
add_filter('pre_site_transient_update_core','__return_null');
add_filter('pre_site_transient_update_plugins','__return_null');
add_filter('pre_site_transient_update_themes','__return_null');


function add_custom_query_var( $vars ){
  $vars[] = "issues";
  return $vars;
}
add_filter( 'query_vars', 'add_custom_query_var' );