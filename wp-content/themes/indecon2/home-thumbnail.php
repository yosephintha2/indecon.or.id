<?php

/**
 * The Template for displaying home news section
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="section">
	<!--
	<div class="row">
		<div class="grid-4 column">
			<a href="https://drive.google.com/drive/folders/1w6nUwRHNKza-DGEzzrO_JfNUPwX2al4i?usp=sharing" target="_blank" style="color: black;">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Aman-Covid-thumbnails.png" alt="Aman Covid">
				<h5 class="title">Adaptasi Pengelolaan Ekowisata Aman COVID-19</h5>
				</a>
				<p>
						<a href="https://drive.google.com/drive/folders/1w6nUwRHNKza-DGEzzrO_JfNUPwX2al4i?usp=sharing" target="_blank">
						    Panduan Aman Covid
						    </a>
				</p>
			 
		</div>
		<div class="grid-4 column">
		    <a href="https://drive.google.com/drive/folders/1jIoBf3m9iASBmboeiz3gS2XqKz0i7rXS?usp=sharing" target="_blank" style="color: black;">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/Survey-Online-thumbnails.png" alt="Survey Online">
				<h5 class="title">Hasil Survey Online #IndeconTourismSurvey</h5>
				</a>
				<p>
					Hasil update sementara survey online guna mengetahui dampak pandemi COVID-19 bagi pekerja pariwisata dan inovasi dalam bertahan.
					Jika ingin berpartisipasi bisa mengisi formulir pada link <a href="https://bit.ly/IndeconTourismSurvey"  target="_blank">bit.ly/IndeconTourismSurvey</a>
				</p>
			 
		</div>
		<div class="grid-4 column">
		    <a href="https://web.facebook.com/pg/indeconetwork/videos/?ref=page_internal" target="_blank" style="color: black;">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/Webinar-thumbnails.png" alt="Webinar">
				<h5 class="title">WEBINAR</h5>
				</a>
				<p>
					<a href="https://web.facebook.com/pg/indeconetwork/videos/?ref=page_internal" target="_blank">
					    Click for More Webinar.
					    </a>
				</p>
			 
		</div>
	</div>
	-->
	
	<div class="row">
	    <?php $thumbnail_args = array(
				'post_type' 		=> 'thumbnail',
				'posts_per_page'	=> 3,
				'orderby' 			=> 'date',
				'order' 			=> 'DESC'
			); 
		$thumbnails = new WP_Query( $thumbnail_args );
		?>

		<?php if ( $thumbnails->have_posts() ) : ?> 

			<?php while ( $thumbnails->have_posts() ) : $thumbnails->the_post(); ?>
				
				<?php //var_dump($thumbnails) ?>
				
				<?php $thumbnail_link = get_post_meta( get_the_ID(), '_thumbnail_link', false ); ?>

                <div class="grid-4 column">
				<?php if ( $thumbnail_link ) { ?>
					<a href="<?php echo esc_url( $thumbnail_link[0] ); ?>" target="_blank" style="color: black;">
						<?php get_the_image( array( 'size' => 'full', 'link_to_post' => false ) ); ?>
						<h5 class="title"><?php the_title(); ?></h5>
					</a>
					<p>
                        <?php echo $post->post_content ?>
				    </p>
				<?php } else { 
					get_the_image( array( 'size' => 'full', 'link_to_post' => false ) ); 
				} ?>
				</div>

			<?php endwhile; ?>

		<?php endif; ?>
	</div>
</div>