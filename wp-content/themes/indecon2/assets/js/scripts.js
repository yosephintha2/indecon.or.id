$ = jQuery.noConflict();

$(document).foundation();

$(function(){
	$('.navtop > .dropdown').hover(function() {
		$(this).addClass('open');
	}, function() {
		$(this).removeClass('open');
	});

	//$('#main-slider').liquidSlider();
	// Simple slider
	var slideCount = $('#slider ul li').length;
	var slideWidth = $('#slider ul li').width();
	var slideHeight = $('#slider ul li').height();
//				var sliderUlWidth = slideCount * slideWidth;
	var sliderUlWidth = slideCount * 100;
	var liWidth = slideWidth / slideCount;
	
	//$('#slider').css({ width: slideWidth, height: slideHeight });
	
	//$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	$('#slider ul').css({ width: sliderUlWidth + "%"});
	$('#slider ul li').css({ width: 100 / slideCount + "%"});
	
	$('#slider ul li:last-child').prependTo('#slider ul');

	function moveLeft() {
		$('#slider ul').animate({
			left: + slideWidth
		}, 500, function () {
			$('#slider ul li:last-child').prependTo('#slider ul');
			$('#slider ul').css('left', '');
		});
	};

	function moveRight() {
		$('#slider ul').animate({
			left: - slideWidth
		}, 500, function () {
			$('#slider ul li:first-child').appendTo('#slider ul');
			$('#slider ul').css('left', '');
		});
	};

	$('a.control_prev').click(function () {
		moveLeft();
	});

	$('a.control_next').click(function () {
		moveRight();
	});

	$('.sidebar .distance ul').addClass('unstyled list-post');

	var $radios = $('input:radio[name=member_type]');
	if($radios.is(':checked') === false) {
		$radios.filter('[value=individu]').prop('checked', true);
	}

	$(".penanggung-jawab").hide();
	$("input[name=member_type]").live( "change", function(){
		if ( $('[value=organisasi]').is(":checked") ) {
				$(".penanggung-jawab").fadeIn("slow");
			} else {
				$(".penanggung-jawab").fadeOut("slow");
			}
	});

	// Tabs
	$('#myTab a').click(function (e) {
		e.preventDefault()
		$(this).tab('show');
	});

	// SLider News 
	!function ($) {
		$(function(){
		  // carousel function
		  $('#mySlide').carousel()
		})
	  }(window.jQuery)
	  $.each( jQuery('.carousel .item'), function( i, val ) {
		$(this).css('background-image','url('+$(this).find('img').attr('src')+')').css('background-size','cover').find('img').css('visibility','hidden');
	  });
	
	$('#wp_signup_form').validate({
		rules: {
			pj_contact: {
				 required: {
					depends: function(element) {
					   return $("#organisasi:checked");
					}
				 }
			},
			pj_position: {
				 required: {
					depends: function(element) {
					   return $("#organisasi:checked");
					}
				 }
			},
			personal_address: {
				 required: {
					depends: function(element) {
					   return $("#organisasi:checked");
					}
				 }
			},
			pj_phone: {
				 required: {
					depends: function(element) {
					   return $("#organisasi:checked");
					}
				 }
			},
			pj_email: {
				 required: {
					depends: function(element) {
					   return $("#organisasi:checked");
					}
				 }
			}
			
	}
	});

	$('#wp_submit_activity').validate({
		rules: {
			fullname: "required",
			email: {
				required : true,
				email : true,
			},
			subject: "required",
			featured_image: "required",
			stories: "required",
		}
	});
});

$(function(){
    $('#filter-wrapper').mixItUp();  
    $('.publication-filter').mixItUp();  
});