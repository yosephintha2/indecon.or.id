$ = jQuery.noConflict();
$(document).ready(function() {
		$("#submitbtn").click(function() { 
			//get input field values
			
			var user_name 				= $('input[name=userName]').val(); 
			var user_password			= $('input[name=userPassword]').val();
			var confirm_password		= $('input[name=confirmPassword]').val();
			var user_website     		= $('input[name=userWebsite]').val();
			var user_email    			= $('input[name=userEmail]').val();
			var user_description 		= $('textarea[name=userDescription]').val();
			var user_fullname 			= $('input[name=userFullname]').val();
			var user_organization 		= $('input[name=userOrganization]').val();
			var user_profession 		= $('input[name=userProfession]').val();
			var user_division 			= $('input[name=userDivision]').val();
			var user_address 			= $('textarea[name=userAddress]').val();
			var user_phone 		 		= $('input[name=userPhone]').val();
			var user_motivation 		= $('textarea[name=userMotivation]').val();
			var member_type 			= $('input[name=memberType]').val();
			var pj_contact	  			= $('textarea[name=pjContact]').val();
			var pj_position	  			= $('input[name=pjPosition]').val();
			var personal_address 		= $('textarea[name=personalAddress]').val();
			var pj_phone 				= $('input[name=pjPhone]').val();
			var pj_email 				= $('input[name=pjEmail]').val();
			var user_avatar 			= $('textarea[name=userAvatar]').val();
			
			//simple validation at client's end
			//we simply change border color to red if empty field using .css()
			var proceed = true;
			if ( user_name == "" ) { 
				$( 'input[name=userName]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( user_password == "" ){ 
				$( 'input[name=userPassword]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( confirm_password == "" ) {    
				$( 'input[name=confirmPassword]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( user_website == "" ) {  
				$( 'input[name=userWebsite]' ).css( 'border-color','red' ); 
				proceed = false;
			} 
			if ( user_email == "" ) {  
				$( 'input[name=userEmail]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( user_description == "" ) {  
				$( 'textarea[name=userDescription]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( user_fullname == "" ) {  
				$( 'input[name=userFullname]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( user_organization == "" ) {  
				$( 'input[name=userOrganization]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( user_profession == "" ) {  
				$( 'input[name=userProfession]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( user_division == "" ) {  
				$( 'input[name=userDivision]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( user_address == "" ) {  
				$( 'textarea[name=userAddress]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( user_phone == "" ) {  
				$( 'input[name=userPhone]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( user_motivation == "" ) {  
				$( 'textarea[name=userMotivation]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( pj_contact == "" ) {  
				$( 'input[name=pjContact]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( pj_position == "" ) {  
				$( 'input[name=pjPosition]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( personal_address == "" ) {  
				$( 'textarea[name=personalAddress]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( pj_phone == "" ) {  
				$( 'input[name=pjPhone]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}
			if ( pj_email == "" ) {  
				$( 'input[name=pjEmail]' ).css( 'border-color', 'red' ); 
				proceed = false;
			}

			//everything looks good! proceed...
			if ( proceed ) {

				$(".loading-img").show(); //show loading image
				$(".submitbtn").hide(); //hide submit button
				
				//data to be sent to server			
				var post_data = new FormData();    
				post_data.append( 'userName', user_name );
				post_data.append( 'userPassword', user_password );
				post_data.append( 'confirmPassword', confirm_password );
				post_data.append( 'userWebsite', user_website );
				post_data.append( 'userEmail', user_email );
				post_data.append( 'userDescription', user_description );
				post_data.append( 'userFullname', user_fullname );
				post_data.append( 'userOrganization', user_organization );
				post_data.append( 'userProfession', user_profession );
				post_data.append( 'userDivision', user_division );
				post_data.append( 'userAddress', user_address );
				post_data.append( 'userPhone', user_phone );
				post_data.append( 'userMotivation', user_motivation );
				post_data.append( 'pjContact', pj_contact );
				post_data.append( 'pjPosition', pj_position );
				post_data.append( 'personalAddress', personal_address );
				post_data.append( 'pjPhone', pj_phone );
				post_data.append( 'pjEmail', pj_email );
				post_data.append( 'userAvatar', user_avatar );
				
				//instead of $.post() we are using $.ajax()
				//that's because $.ajax() has more options and can be used more flexibly.
				$.ajax({
				  url: 'registrationAjax.ajaxurl',
				  data: post_data,
				  processData: false,
				  contentType: false,
				  type: 'POST',
				  dataType:'json',
				  success: function(data){
						//load json data from server and output message     
						if ( data.type == 'error' ) {
							output = '<div class="error">'+data.text+'</div>';
						} else{
							output = '<div class="success">'+data.text+'</div>';
							
							//reset values in all input fields
							$('#wp_signup_form input').val(''); 
							$('#wp_signup_form textarea').val(''); 
						}
						
						$("#result").hide().html(output).slideDown(); //show results from server
						$(".loading-img").hide(); //hide loading image
						$(".submitbtn").show(); //show submit button
				  }
				});

			}
		});
		
		//reset previously set border colors and hide all message on .keyup()
		$("#wp_signup_form input, #wp_signup_form textarea").keyup(function() { 
			$("#wp_signup_form input, #wp_signup_form textarea").css('border-color',''); 
			$("#result").slideUp();
		});
		
	});