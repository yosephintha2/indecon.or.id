<?php

/**
 * The Template for displaying slider
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

	<!-- Carousel ================================================== -->
	<div id="mySlide" class="carousel slide">
		<?php
			$post_perpage 	= indecon_option( 'slider_per_page' );
			$per_page 		= ( ! empty( $post_perpage ) ) ? indecon_option( 'slider_per_page' ) : 5;
			$slider_args = array(
				'post_type' 		=> 'slider',
				'posts_per_page'	=> $per_page,
				'order' 			=> 'DESC',
				'orderby' 			=> 'date'
				);
			$sliders = new WP_Query( $slider_args );
			$counter  = 1;
		 ?>

		<?php if ( $sliders->have_posts() ) : ?> 
			
			<ol class="carousel-indicators">
				<?php for ( $i=0; $i < $sliders->post_count ; $i++ ) { ?>
					<?php if ( 0 == $i ) : ?>
						<li class="active" data-slide-to="<?php echo $i; ?>" data-target="#mySlide"></li>
					<?php else : ?>
						<li data-slide-to="<?php echo $i ?>" data-target="#mySlide" class=""></li>
					<?php endif ?>
				<?php } ?>
			</ol>

			<div class="carousel-inner">
		 	
		 	<?php while ( $sliders->have_posts() ) : $sliders->the_post(); ?>
				
				<?php if ( 1 == $counter ) {
					$slider_class = 'active';
				} else {
					$slider_class = '';
				} ?>

				<div class="item <?php echo $slider_class; ?>">
					<?php get_the_image( array( 'size' => 'slider', 'link_to_post' => false ) ); ?>
					<div class="row">
						<div class="carousel-caption">
							<?php $slider_link = get_post_meta( get_the_ID(), '_slider_link', true ); 
									$link = ! empty( $slider_link ) ? get_post_meta( get_the_ID(), '_slider_link', true ) : '';
								?>
							<h3><a href="<?php echo esc_url( $link ); ?>"><?php the_title(); ?></a></h3>
							<p><a href="<?php echo esc_url( $link ); ?>"><?php echo wp_trim_words( $post->post_excerpt, 20 ); ?></a></p>
						</div>
					</div>
				</div>
				

		<?php $counter++; endwhile; ?>
			
			</div>
		
		<a class="left carousel-control" href="#mySlide" data-slide="prev">&lsaquo;</a>
		<a class="right carousel-control" href="#mySlide" data-slide="next">&rsaquo;</a>
		
		<?php endif; wp_reset_postdata(); ?>

	</div><!-- /.carousel -->