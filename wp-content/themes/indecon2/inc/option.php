<?php

return array(
	'title' => __('Indecon Option Panel', 'indecon'),
	'logo' => get_template_directory_uri().'/assets/img/logo-indecon.png',
	'menus' => array(
		array(
			'title' => __('Basic Settings', 'indecon'),
			'name' => 'menu_1',
			'icon' => 'font-awesome:fa-magic',
			'menus' => array(
				
				array(
					'title' => __('General Settings', 'indecon'),
					'name' => 'submenu_1',
					'icon' => 'font-awesome:fa-th-large',
					'controls' => array(
						
						array(
							'type' => 'upload',
							'name' => 'custom_favicon',
							'label' => __('Favicon', 'indecon'),
							'description' => __('You can set custom favicon here', 'indecon'),
						),

						array(
							'type' => 'textarea',
							'name' => 'footer_credit',
							'label' => __('Footer Credit', 'indecon'),
							'description' => __('You can set custom footer credit here', 'indecon'),
							'default' => '<p>&copy; 2014 <a href="#">Indecon</a>. All rigths reserved.</p>',
						),

						array(
							'type' => 'codeeditor',
							'name' => 'google_analytic_code',
							'label' => __('Google Analytic Script', 'indecon'),
							'description' => __('Paste your google analytic script here', 'indecon'),
							'theme' => 'github',
							'mode' => 'javascript',
						),

						array(
							'type' => 'codeeditor',
							'name' => 'custom_header_script',
							'label' => __('Custom Header Script', 'indecon'),
							'description' => __('You can put custom script in the header', 'indecon'),
							'theme' => 'github',
							'mode' => 'javascript',
						),

						array(
							'type' => 'codeeditor',
							'name' => 'custom_footer_script',
							'label' => __('Custom Footer Script', 'indecon'),
							'description' => __('You can put custom script in the footer', 'indecon'),
							'theme' => 'github',
							'mode' => 'javascript',
						),

					),
				),


				// HOME PAGE
				array(
					'title' => __('Home Page', 'indecon'),
					'name' => 'submenu_2',
					'icon' => 'font-awesome:fa-home',
					'controls' => array(
						array(
							'type' => 'textbox',
							'name' => 'slider_per_page',
							'label' => __('Slider Display Number', 'indecon'),
							'description' => __('Set how many slider will be displayed', 'indecon'),
							'default' => '5',
							'validation' => 'numeric',
						),

						array(
							'type' => 'textbox',
							'name' => 'slider_news_per_page',
							'label' => __('Slider News Display Number', 'indecon'),
							'description' => __('Set how many slides will be displayed', 'indecon'),
							'default' => '5',
							'validation' => 'numeric',
						),

						// Section Join COmmunity Box
						array(
							'type' => 'section',
							'title' => __('Join Community Box', 'indecon'),
							'name' => 'join_community_box',
							'description' => __('Controlling Join Commonity Box', 'indecon'),
							'fields' => array(
								array(
									'type' => 'textbox',
									'name' => 'join_community_title',
									'label' => __('Title', 'indecon'),
									'description' => __('Set The Join Community Title', 'indecon'),
									'default' => __('JOIN OUR COMMUNITY', 'indecon'),
								),
								array(
									'type' => 'textarea',
									'name' => 'join_community_content',
									'label' => __('Content', 'indecon'),
									'description' => __('Join Community Content ', 'indecon'),
									'default' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
								),
								
								array(
									'type' => 'select',
									'name' => 'join_community_button_link',
									'label' => __('Link', 'indecon'),
									'description' => __('Link to Registration Page', 'indecon'),
									'items' => array(
										'data' => array(
											array(
												'source' => 'function',
												'value' => 'vp_get_pages',
											),
										),
									),
								),
							),
						),
						
						// Section Category COntroller
						array(
							'type' => 'section',
							'name' => 'category_controller',
							'title' => __('Category / Page Controller', 'indecon'),
							'description' => __('Controls The Category and Page.', 'indecon'),
							'fields' => array(

								array(
									'type' => 'select',
									'name' => 'news_category_target',
									'label' => __('Set the news category will be displayed', 'indecon'),
									'items' => array(
										'data' => array(
											array(
												'source' => 'function',
												'value' => 'vp_get_categories',
											),
										),
									),
									'default' => array(
										'{{first}}',
									),
								),

								array(
									'type' => 'select',
									'name' => 'activities_page_target',
									'label' => __('Set the activity page will be displayed', 'indecon'),
									'items' => array(
										'data' => array(
											array(
												'source' => 'function',
												'value' => 'vp_get_pages',
											),
										),
									),
									'default' => array(
										'{{first}}',
									),
								),

								array(
									'type' => 'select',
									'name' => 'media_page_target',
									'label' => __('Set the media and publication page will be displayed', 'indecon'),
									'items' => array(
										'data' => array(
											array(
												'source' => 'function',
												'value' => 'vp_get_pages',
											),
										),
									),
									'default' => array(
										'{{first}}',
									),
								),
							),
						), // End Category Controller
						

						// Section Footprint COntroller
						array(
							'type' => 'section',
							'name' => 'footprint_controller',
							'title' => __('Footprint Controller', 'indecon'),
							'description' => __('Controls The Footprint.', 'indecon'),
							'fields' => array(

								array(
									'type' => 'textarea',
									'name' => 'footprint_content',
									'label' => __('Footprint Content', 'indecon'),
									'description' => __('Set the footprint content', 'indecon'),
									'default' => 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in bookmarksgrove right.',
								),

								array(
									'type' => 'select',
									'name' => 'footprint_target_page',
									'label' => __('Set the footprint pages', 'indecon'),
									'items' => array(
										'data' => array(
											array(
												'source' => 'function',
												'value' => 'vp_get_pages',
											),
										),
									),
									'default' => array(
										'{{first}}',
									),
								),

								array(
									'type' => 'upload',
									'name' => 'footprint_map_image',
									'label' => __('Map Image', 'indecon'),
									'description' => __('Upload the map here', 'indecon'),
									'default' => get_template_directory_uri().'/assets/img/img-footprint.png',
								),

								array(
									'type' => 'textbox',
									'name' => 'footprint_map_link',
									'label' => __('Map Link', 'indecon'),
									'description' => __('Link Target, eg: http://indecon.co.id', 'indecon'),
									'default' => '',
									'validation' => 'url',
								),

								array(
									'type' => 'upload',
									'name' => 'footprint_banner_image',
									'label' => __('Banner Image', 'indecon'),
									'description' => __('Upload the banner here', 'indecon'),
									'default' => get_template_directory_uri().'/assets/img/example/banner-1.jpg',
								),

								array(
									'type' => 'textbox',
									'name' => 'footprint_banner_link',
									'label' => __('Banner Link', 'indecon'),
									'description' => __('Link Target, eg: http://indecon.co.id', 'indecon'),
									'default' => '',
									'validation' => 'url',
								),

							),
						), // End Footprint Controller
						
					),
				),
				
				// SOCIAL
				array(
					'title' => __('Social Settings', 'indecon'),
					'name' => 'socials',
					'icon' => 'font-awesome:fa-user',
					'controls' => array(
						
						// Section Social Account
						array(
							'type' => 'section',
							'title' => __('Social Account', 'indecon'),
							'name' => 'social_account',
							'description' => __('Controlling Social Account', 'indecon'),
							'fields' => array(
								array(
									'type' => 'textbox',
									'name' => 'social_facebook',
									'label' => __('Facebook', 'indecon'),
									'description' => __('Insert your facebook username', 'indecon'),
									'default' => 'indecon',
								),

								array(
									'type' => 'textbox',
									'name' => 'social_twitter',
									'label' => __('Twitter', 'indecon'),
									'description' => __('Insert your twitter username', 'indecon'),
									'default' => 'indecon',
								),

								array(
									'type' => 'textbox',
									'name' => 'social_email',
									'label' => __('Email', 'indecon'),
									'description' => __('Insert your email address', 'indecon'),
									'default' => 'indecon@indecon.co.id',
									'validation' => 'email',
								),

								array(
									'type' => 'textbox',
									'name' => 'social_rss',
									'label' => __('RSS', 'indecon'),
									'description' => __('Insert your RSS Link', 'indecon'),
									'default' => 'http://indecon.co.id/rss',
									'validation' => 'url',
								),
								
								
							),
						),
						
					),
				),

				// Page Setting
				array(
					'title' => __('Custom Settings', 'indecon'),
					'name' => 'pages',
					'icon' => 'font-awesome:fa-cogs',
					'controls' => array(
						
						// Pages COntrol
						array(
							'type' => 'section',
							'title' => __('Pages', 'indecon'),
							'name' => 'section_pages',
							'description' => __('Controlling Pages', 'indecon'),
							'fields' => array(
								array(
									'type' => 'select',
									'name' => 'user_after_login',
									'label' => __('Redirect user after log in', 'indecon'),
									'description' => __('Page after log in', 'indecon'),
									'items' => array(
										'data' => array(
											array(
												'source' => 'function',
												'value' => 'vp_get_pages',
											),
										),
									),
									'default' => array(
										'{{first}}',
									),
								),

							),
						),

						// Subscribe Control
						array(
							'type' => 'section',
							'title' => __('Subscribe', 'indecon'),
							'name' => 'section_pages',
							'description' => __('Controlling Subscribe Form', 'indecon'),
							'fields' => array(
								array(
									'type' => 'textbox',
									'name' => 'subscribe_action_link',
									'label' => __('Subscribe API URL', 'indecon'),
									'description' => __('Insert Mailchimp API URL', 'indecon'),
									'default' => ''
								),

							),
						),

						// Login Redirect
						array(
							'type' => 'section',
							'title' => __('Login Redirect', 'indecon'),
							'name' => 'section_login_redirect',
							'description' => __('Controlling Login Page', 'indecon'),
							'fields' => array(
								array(
									'type' => 'select',
									'name' => 'login_redirect_page',
									'label' => __('Login Page', 'indecon'),
									'description' => __('Choose login page', 'indecon'),
									'items' => array(
										'data' => array(
											array(
												'source' => 'function',
												'value' => 'vp_get_pages',
											),
										),
									),
									'default' => array(
										'{{first}}',
									),
								),

							),
						),
						
					),
				),
			),
		),
		
		
	)
);

/**
 *EOF
 */