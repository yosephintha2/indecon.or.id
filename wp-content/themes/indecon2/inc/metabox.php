<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'networks_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function networks_metaboxes( array $meta_boxes ) {
	// Start with an underscore to hide fields from custom fields list
	$prefix = '_network_';

	$meta_boxes['network_metabox'] = array(
		'id'         => 'network_metabox',
		'title'      => __( 'Network Details', 'indecon' ),
		'pages'      => array( 'network', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		// 'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
		'fields'     => array(

			array(
				'name' 		=> __( 'Tipe', 'indecon' ),
				'desc' 		=> __( 'type', 'indecon' ),
				'id'   		=> 'member-type',
				'type' 		=> 'select',
				//'std' 		=> $member_type,
				'options' => array(
								array('name' => 'Individu', 'value' => 'individu'),
								array('name' => 'Organisasi', 'value' => 'organisasi')
							)
			),
			
			array(
				'name' => __( 'Full Name', 'indecon' ),
				'desc' => __( 'Full Name', 'indecon' ),
				'id'   => 'full_name',
				'type' => 'text_medium',
			),

			array(
				'name' => __( 'Profesi', 'indecon' ),
				'desc' => __( 'Profesi', 'indecon' ),
				'id'   => 'profession',
				'type' => 'text_medium',
			),

			array(
				'name' => __( 'Bidang Gerak', 'indecon' ),
				'desc' => __( 'Bidang Gerak', 'indecon' ),
				'id'   => 'division',
				'type' => 'text_medium',
			),

			array(
				'name' => __( 'Organisasi', 'indecon' ),
				'desc' => __( 'Organisasi', 'indecon' ),
				'id'   => 'organization',
				'type' => 'text_medium',
			),

			array(
				'name' => __( 'Alamat', 'indecon' ),
				'desc' => __( 'Alamat Lengkap', 'indecon' ),
				'id'   => 'full_address',
				'type' => 'textarea_small',
			),

			array(
				'name' => __( 'Phone Number', 'indecon' ),
				'desc' => __( 'Network phone number', 'indecon' ),
				'id'   => 'phone_number',
				'type' => 'text_medium',
			),

			array(
				'name' => __( 'Network URL', 'indecon' ),
				'desc' => __( 'Network URL', 'indecon' ),
				'id'   => 'website_url',
				'type' => 'text_medium',
			),

			array(
				'name' => __( 'Motivasi', 'indecon' ),
				'desc' => __( 'Motivasi Bergabung', 'indecon' ),
				'id'   => 'motivation',
				'type' => 'wysiwyg',
				'options' => array(
							'textarea_rows' => get_option('default_post_edit_rows', 5), // rows="..."
						),
			),

			array(
				'name' => __( 'Deskripsi', 'indecon' ),
				'desc' => __( 'Deskripsi Singkat', 'indecon' ),
				'id'   => 'description',
				'type' => 'wysiwyg',
				'options' => array(
							'textarea_rows' => get_option('default_post_edit_rows', 5), // rows="..."
						),
			),
		)
	);

	$meta_boxes['pj_metabox'] = array(
		'id'         => 'pj_metabox',
		'title'      => __( 'Penanggung Jawab Details', 'indecon' ),
		'pages'      => array( 'network', ), // Post type
		'context'    => 'normal',
		'priority'   => 'low',
		'show_names' => true, // Show field names on the left
		// 'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
		'fields'     => array(

			array(
				'name' => __( 'Nama Kontak', 'indecon' ),
				'desc' => __( 'Nama Kontak', 'indecon' ),
				'id'   => 'pj_contact',
				'type' => 'text_medium',
			),

			array(
				'name' => __( 'Alamat Pribadi', 'indecon' ),
				'desc' => __( 'Alamat Pribadi', 'indecon' ),
				'id'   => 'personal_address',
				'type' => 'text_medium',
			),

			array(
				'name' => __( 'Jabatan di Lembaga', 'indecon' ),
				'desc' => __( 'Jabatan di Lembaga', 'indecon' ),
				'id'   => 'pj_posision',
				'type' => 'text_medium',
			),

			// array(
			// 	'name' => __( 'No tlp/fax/Handpon', 'indecon' ),
			// 	'desc' => __( 'No tlp/fax/Handpon', 'indecon' ),
			// 	'id'   => 'pj_phone',
			// 	'type' => 'text_medium',
			// ),

			array(
				'name' => __( 'Email', 'indecon' ),
				'desc' => __( 'Email penanggung jawab', 'indecon' ),
				'id'   => 'pj_email',
				'type' => 'text_medium',
			),

			
		)
	);


	/**
	 * Metabox for the Footprint
	 */
	$meta_boxes[] = array(
		'id'            => 'footprint',
		'title'         => __( 'Footprint Metabox', 'indecon' ),
		'pages'         => array( 'footprint' ), // Tells CMB to use user_meta vs post_meta
		'show_names'    => true,
		'fields'        => array(
			array(
				'name'    	=> __( 'Type', 'indecon' ),
				'desc'     	=> __( 'Footprint Type', 'indecon' ),
				'id'       	=> '_footprint_type',
				'type'     	=> 'select',
				'options' => array(
								array('name' => 'Training', 'value' => 'training'),
								array('name' => 'Project', 'value' => 'project')
							)
			),

			array(
				'name'     => __( 'Latitude', 'indecon' ),
				'desc'     => __( 'Latitude', 'indecon' ),
				'id'       => '_footprint_latitude',
				'type'     => 'text_small',
			),

			array(
				'name'     => __( 'Latitude', 'indecon' ),
				'desc'     => __( 'Longitude', 'indecon' ),
				'id'       => '_footprint_longitude',
				'type'     => 'text_small',
			),

			array(
				'name'     => __( 'Link', 'indecon' ),
				'desc'     => __( 'Footprint Link', 'indecon' ),
				'id'       => '_footprint_link',
				'type'     => 'text',
			),

			
			
		)
	);

	/**
	 * Metabox for the Partner
	 */
	$meta_boxes[] = array(
		'id'            => 'partner',
		'title'         => __( 'Partner Details', 'indecon' ),
		'pages'         => array( 'partner' ), // Tells CMB to use user_meta vs post_meta
		'show_names'    => true,
		'priority'  	=> 'low',
		'fields'        => array(
			array(
				'name'     => __( 'Partner Link', 'indecon' ),
				'desc'     => __( 'Insert the partner link, eg: http://indecon.co.id', 'indecon' ),
				'id'       => '_partner_link',
				'type'     => 'text',
			)
		)
	);

	/**
	 * Metabox for the Learning
	 */
	$meta_boxes[] = array(
		'id'            => 'learning',
		'title'         => __( 'Learning Details', 'indecon' ),
		'pages'         => array( 'learning' ), // Tells CMB to use user_meta vs post_meta
		'show_names'    => true,
		'priority'  	=> 'low',
		'fields'        => array(
			array(
				'name'     => __( 'Learning Type', 'indecon' ),
				'desc'     => __( 'Learning type', 'indecon' ),
				'id'       => '_learning_type',
				'type'     => 'select',
				'options' => array(
								array('name' => 'Public', 'value' => 'public'),
								array('name' => 'Private', 'value' => 'private')
							)
			)
		)
	);

	/**
	 * Metabox for the Team
	 */
	$meta_boxes[] = array(
		'id'            => 'team',
		'title'         => __( 'Team Details', 'indecon' ),
		'pages'         => array( 'team' ), // Tells CMB to use user_meta vs post_meta
		'show_names'    => true,
		'priority'  	=> 'low',
		'fields'        => array(
			array(
				'name'     => __( 'Position', 'indecon' ),
				'desc'     => __( 'Team Position', 'indecon' ),
				'id'       => '_team_position',
				'type'     => 'text',
			),

			array(
				'name'     => __( 'Email', 'indecon' ),
				'desc'     => __( 'Email Address', 'indecon' ),
				'id'       => '_team_email',
				'type'     => 'text',
			),

			array(
				'name'     => __( 'Facebook', 'indecon' ),
				'desc'     => __( 'Facebook username', 'indecon' ),
				'id'       => '_team_facebook',
				'type'     => 'text',
			),

			array(
				'name'     => __( 'Twitter', 'indecon' ),
				'desc'     => __( 'Twitter username', 'indecon' ),
				'id'       => '_team_twitter',
				'type'     => 'text',
			)
		)
	);

    /**
	 * Metabox for the Slider
	 */
	$meta_boxes['slider_metabox'] = array(
		'id'         => 'slider_metabox',
		'title'      => __( 'Slider Details', 'indecon' ),
		'pages'      => array( 'slider', ), // Post type
		'context'    => 'normal',
		'priority'   => 'low',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => __( 'Slider Link', 'indecon' ),
				'desc' => __( 'Insert the slider link', 'indecon' ),
				'id'   => '_slider_link',
				'type' => 'text',
			),
		),
	);

	/**
	 * Metabox for the Publication COntrol
	 */
	$meta_boxes[] = array(
		'id'            => 'publication_1',
		'title'         => __( 'Publications Category', 'indecon' ),
		'pages'         => array( 'publication' ), // Tells CMB to use user_meta vs post_meta
		'show_names'    => true,
		'priority'  	=> 'high',
		'fields'        => array(
			array(
				'name'     => __( 'Publication Category', 'indecon' ),
				'desc'     => __( 'Choose publication category', 'indecon' ),
				'id'       => '_publication_categories',
				'type'     => 'select',
				'options' => array(
								array('name' => 'Article', 'value' => 'article'),
								array('name' => 'Photos', 'value' => 'photos'),
								array('name' => 'Videos', 'value' => 'videos'),
							)
			),

		)
	);

	/**
	 * Metabox for the Publication COntrol
	 */
	$meta_boxes[] = array(
		'id'            => 'publication_2',
		'title'         => __( 'Video URL', 'indecon' ),
		'pages'         => array( 'publication' ), // Tells CMB to use user_meta vs post_meta
		'show_names'    => true,
		'priority'  	=> 'low',
		'fields'        => array(
			array(
				'name' => __( 'Video URL', 'indecon' ),
				'desc' => __( 'Insert youtube video URL here', 'indecon' ),
				'id'   => '_youtube_video_url',
				'type' => 'text',
			),
		)
	);

	// Add other metaboxes as needed
	
	 /**
     * Metabox for the Thumbnail
     */
    $meta_boxes[] = array(
        'id' => 'thumbnail',
        'title' => __('Thumbnail Metabox', 'indecon'),
        'pages' => array('thumbnail'), // Tells CMB to use user_meta vs post_meta
        'show_names' => true,
        'priority' => 'low',
        'fields' => array(
            array(
                'name' => __('Thumbnail Link', 'indecon'),
                'desc' => __('Insert the thumbnail link, eg: http://indecon.co.id', 'indecon'),
                'id' => '_thumbnail_link',
                'type' => 'text',
            ),
        )
    );
    
    /**
     * Metabox for the Project
     */
    $meta_boxes[] = array(
        'id' => 'thumbnail',
        'title' => __('Project Metabox', 'indecon'),
        'pages' => array('project'), // Tells CMB to use user_meta vs post_meta
        'show_names' => true,
        'priority' => 'low',
        'fields' => array(
             array(
                'name' => __('Duration', 'indecon'),
                'desc' => __('Duration', 'indecon'),
                'id' => '_duration',
                'type' => 'text_medium',
            ),
            // array(
            //     'name' => __('Issues', 'indecon'),
            //     'desc' => __('Issues', 'indecon'),
            //     'id' => '_issues',
            //     'type' => 'textarea_small',
            // ),
            
            array(
				'name' => __('Issues', 'indecon'),
				'desc' => __('Issues', 'indecon'),
				'id'   		=> '_issues',
				'type' 		=> 'select',
				//'std' 		=> $member_type,
				// 'options' => array(
				// 				array('name' => 'Climate Change', 'value' => 'climate-change'),
				// 				array('name' => 'Development Planning', 'value' => 'development-planning'),
				// 				array('name' => 'Gender & Youth', 'value' => 'gender-youth'),
				// 				array('name' => 'Local Economic Development', 'value' => 'local-economic-development'),
				// 				array('name' => 'Tourism & Conservation', 'value' => 'tourism-conservation'),
				// 				array('name' => 'Tourism Planning', 'value' => 'tourism-planning'),
				// 				array('name' => 'Training & Standard', 'value' => 'training-standard'),
				// 			)
				'options' => array(
								array('name' => 'Climate Change', 'value' => 'Climate Change'),
								array('name' => 'Development Planning', 'value' => 'Development Planning'),
								array('name' => 'Gender & Youth', 'value' => 'Gender & Youth'),
								array('name' => 'Local Economic Development', 'value' => 'Local Economic Development'),
								array('name' => 'Tourism & Conservation', 'value' => 'Tourism & Conservation'),
								array('name' => 'Tourism Planning', 'value' => 'Tourism Planning'),
								array('name' => 'Training & Standard', 'value' => 'Training & Standard'),
							)
			),
            
            array(
                'name' => __('Location', 'indecon'),
                'desc' => __('Location', 'indecon'),
                'id' => '_location',
                'type' => 'textarea_small',
            ),
            
            array(
				'name'     => __( 'Latitude', 'indecon' ),
				'desc'     => __( 'Latitude', 'indecon' ),
				'id'       => '_project_latitude',
				'type'     => 'text_small',
			),

			array(
				'name'     => __( 'Latitude', 'indecon' ),
				'desc'     => __( 'Longitude', 'indecon' ),
				'id'       => '_project_longitude',
				'type'     => 'text_small',
			),
			
			array(
                'name' => __('Donor', 'indecon'),
                'desc' => __('Donor', 'indecon'),
                'id' => '_donor',
                'type' => 'text',
            ),
        )
    );
    
    
	return $meta_boxes;
}

add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once( trailingslashit( get_template_directory() ) . 'framework/metabox/init.php' );

}
