<?php

/**
 * Additional core function 
 *
 * @return void
 * @author alispx
 **/
function indecon_site_logo() {

	echo '<div class="logo">'; ?>
		<a href="<?php echo home_url(); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-indecon.png" alt="Indecon Logo">
		</a>
		<p class="tagline"><?php echo bloginfo( 'description' ); ?></p>
	<?php		
	echo '</div>';
}

/**
 * Function insert_attachment
 *
 * @return $attachment
 * @author alispx
 **/
// function insert_attachment( $file_handler, $post_id, $setthumb = 'false' ) {
// 	// check to make sure its a successful upload
// 	if ( $_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

// 	require_once( ABSPATH . "wp-admin" . '/includes/image.php' );
// 	require_once( ABSPATH . "wp-admin" . '/includes/file.php' );
// 	require_once( ABSPATH . "wp-admin" . '/includes/media.php' );

// 	$attach_id = media_handle_upload( $file_handler, $post_id );

// 	if ( $setthumb ) {
// 		update_post_meta( $post_id, '_thumbnail_id', $attach_id);
// 	} 

// 	return $attach_id;
// }

//attachment helper function   
function insert_attachment( $file_handler, $post_id, $setthumb = 'false' ) {
    if ( $_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) { return __return_false(); 
    } 
    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attach_id = media_handle_upload( $file_handler, $post_id );
    //set post thumbnail if setthumb is 1
    if ( $setthumb == 1 ) update_post_meta( $post_id, '_thumbnail_id', $attach_id );
    return $attach_id;
}

/**
 * Redirect non-admin users to home page
 *
 * This function is attached to the 'admin_init' action hook.
 */
add_action( 'admin_init', 'redirect_non_admin_users' );
function redirect_non_admin_users() {
	if ( ! current_user_can( 'manage_options' ) && '/wp-admin/admin-ajax.php' != $_SERVER['PHP_SELF'] ) {
		wp_redirect( home_url() );
		exit;
	}
}

/**
 * Print custom favicon on header
 *
 * @return void
 * @author alispx
 **/
add_action( 'wp_head', 'custom_favicon', 20 );
function custom_favicon() {
	if ( indecon_option( 'custom_facivon' ) ) { ?>
		<link rel="shortcut icon" href="<?php echo esc_attr( indecon_option( 'custom_facivon' ) ); ?>" />
	<?php } else {?>
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
	<?php }
}

/**
 * Print custom header script
 *
 * @return void
 * @author alispx
 **/
add_action( 'wp_head', 'custom_header_script', 60 );
function custom_header_script() {
	if ( indecon_option( 'custom_header_script' ) ) {
		$header_script = "\n" . '<style type="text/javascript">' .indecon_option( 'custom_header_script' ). '</style>' . "\n";
		echo $header_script;
	}
}

/**
 * Print custom footer script
 *
 * @return void
 * @author alispx
 **/
add_action( 'wp_footer', 'custom_footer_script', 60 );
function custom_footer_script() {
	if ( indecon_option( 'custom_footer_script' ) ) {
		$footer_script = "\n" . '<style type="text/javascript">' .indecon_option( 'custom_footer_script' ). '</style>' . "\n";
		echo $footer_script;
	}
}

/**
 * Print custom footer script
 *
 * @return void
 * @author alispx
 **/
add_action( 'wp_head', 'google_analytic_script', 70 );
function google_analytic_script() {
	if ( indecon_option( 'google_analytic_code' ) ) {
		$google_script = "\n" . '<style type="text/javascript">' .indecon_option( 'google_analytic_code' ). '</style>' . "\n";
		echo $google_script;
	}
}

function indecon_select_language() {
	if ( !function_exists( "qtrans_getSortedLanguages" ) ) {
		// plugin qTranslate must be active
		echo '<!-- Please activate the qTranslate plugin -->';
		return;
	}
	global $q_config;
	if( is_404() ) {
		$url = get_option( 'home' );
	} else {
		$url = '';
	}
	$languages = qtrans_getSortedLanguages( true );
	foreach( $languages as $language ) {
		$classes = array( 'language', 'lang-'.$language );
		// add an extra style class to the active language
		if( $language == $q_config[ 'language' ] ) {
			$classes[] = 'active';
		}
		echo '<a href="'.qtrans_convertURL( $url, $language ).'"';
		echo ' hreflang="' . $language . '" title="' . $q_config[ 'language_name' ][ $language ] . '">';
		echo '<img src="' . get_stylesheet_directory_uri() . '/images/flags/' . $language . '.png" alt="' . $q_config[ 'language_name' ][ $language ] . '" /> '.$q_config['language_name'][$language].'</a>' . "\n";
	}
}

/**
 * Wrapping get_post_meta function 
 *
 * @return void
 * @author matamerah
 **/
function indecon_post_meta( $key = '' ) {
	$the_meta = get_post_meta( get_the_ID(), $key, true );
	
	if ( ! empty( $the_meta ) ) {
		echo $the_meta;
	} else {
		echo '';
	}

}