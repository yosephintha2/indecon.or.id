<?php

/**
 * Theme setup function.  This function adds support for theme features and defines the default theme
 * actions and filters.
 *
 * @since  1.0
 * @access public
 */

/**
 * Define Constant
 *
 * @return void
 * @author alispx
 **/
define( 'THEME_PATH', get_template_directory_uri() );
define( 'THEME_SCRIPT', get_template_directory_uri(). '/assets/js/' );
define( 'THEME_STYLE', get_template_directory_uri(). '/assets/css/' );
define( 'THEME_FONT', get_template_directory_uri(). '/assets/css/' );
/**
 * Flush rewrite rules.
 * 
 * @since 1.0
 */
add_action( 'after_switch_theme', 'spx_flush_rewrite_rules', 5 );
function spx_flush_rewrite_rules() {
	flush_rewrite_rules();
}   

/**
 * Function Setup Theme
 * 
 * @since 1.0
 */
add_action( 'after_setup_theme', 'spx_theme_setup', 10 );
function spx_theme_setup() {

	/* Get action/filter hook prefix. */
	$prefix = hybrid_get_prefix();

	/* Define theme support */
	add_theme_support( 'hybrid-core-menus', array( 'primary', 'secondary' ) );
	add_theme_support( 'hybrid-core-sidebars', array( 'primary','project' ) );
	add_theme_support( 'hybrid-core-scripts', array( 'comment-reply' ) );
	add_theme_support( 'hybrid-core-styles', array( 'gallery', 'style' ) );
	add_theme_support( 'hybrid-core-widgets' );
	add_theme_support( 'hybrid-core-shortcodes' );
	add_theme_support( 'hybrid-core-template-hierarchy' );
	add_theme_support( 'loop-pagination' );
	add_theme_support( 'get-the-image' );
	add_theme_support( 'cleaner-gallery' );
	add_theme_support( 'cleaner-caption' );
	add_theme_support( 'automatic-feed-links' );

	/* Handle content width for embeds and images. */
	hybrid_set_content_width( 1280 );

	// add post-formats to post_type 'publication'
	add_post_type_support( 'publication', 'post-formats' );

}

/**
 * Function unclude file that required by theme
 * 
 * @since 1.0
 */
add_action( 'after_setup_theme', 'spx_include_file', 15 );

function spx_include_file() {

	// Load Metabox Framework
	require_once( trailingslashit( get_template_directory() ) . 'inc/metabox.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/functions.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/bootstrap-navwalker.php' );

	// Load Widget Framework
	require_once( trailingslashit( get_template_directory() ) . 'framework/widget/widget-image-field.php' );


	// Load Custom Post Type
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/slider.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/network.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/footprint.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/thumbnail.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/project.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/project.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/research.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/training.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/learning.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/team.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/partner.php' );
    // require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/member.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/activity.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/post-type/publication.php' );
	
	// Load Custom Widget
	require_once( trailingslashit( get_template_directory() ) . 'inc/widget/banner-widget.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/widget/archive-widget.php' );

	require_once( trailingslashit( get_template_directory() ) . 'inc/extension/if-menu/if-menu.php' );

	
}

/**
 * Function load style and script
 *
 * @author alispx
 **/
add_action( 'wp_enqueue_scripts', 'spx_load_script', 10 );
function spx_load_script() {
	// Load script
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'foundation', THEME_SCRIPT. 'foundation.js' , '', '1.0', false );
	wp_enqueue_script( 'foundation-forms', THEME_SCRIPT. 'foundation.forms.js' , '', '1.0', false );
	wp_enqueue_script( 'bootstrap', THEME_SCRIPT. 'bootstrap.js' , '', '1.0', false );
	wp_enqueue_script( 'modernizer', THEME_SCRIPT. 'modernizr-2.6.2.min.js' , '', '2.6.2', false );
	wp_enqueue_script( 'validator', THEME_SCRIPT. 'jquery.validate.min.js' , '', '2.6.2', false );
	wp_enqueue_script( 'mixitup', THEME_SCRIPT. 'jquery.mixitup.min.js' , '', '', false );
	wp_enqueue_script( 'spx-script', THEME_SCRIPT. 'scripts.js' , array( 'jquery' ), '1.0', false );

	wp_enqueue_style( 'fancybpx-style', THEME_SCRIPT . '/fancybox/jquery.fancybox.css' );
	wp_enqueue_script( 'fancybox', THEME_SCRIPT. '/fancybox/jquery.fancybox.js' , '', '', false );
	wp_enqueue_script( 'fancybox-method', THEME_SCRIPT. '/fancybox/fancybox-method.js' , '', '', false );
	
	wp_enqueue_style( 'gfont', 'https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' );
	wp_enqueue_style( 'font', THEME_STYLE . 'fonts.css' );
	wp_enqueue_style( 'normalize', THEME_STYLE . 'normalize.css' );
	wp_enqueue_style( 'stylesheet', THEME_STYLE . 'stylesheet.css' );
	wp_enqueue_style( 'custom', THEME_STYLE . 'custom-css.css' );
	

}

/**
 * Function gmaps script
 *
 * @author alispx
 **/
add_action( 'wp_enqueue_scripts', 'spx_load_gmaps_script', 20 );
function spx_load_gmaps_script() {
	// Load script
	if ( is_page_template( 'page-templates/footprint.php' ) ) {
		wp_enqueue_script( 'gmaps-api', 'https://maps.google.com/maps/api/js?sensor=true' , '', '3.0', false );
		wp_enqueue_script( 'gmaps', THEME_SCRIPT. 'gmaps.js' , '', '3.0', false );
	}
	
	if ( is_page_template( 'page-templates/project.php' ) ) {
		wp_enqueue_script( 'gmaps-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDDkyiOeYrYiEWZoGEPCKSzcS7klheX6xY&sensor=false' , '', '3.0', false );
		wp_enqueue_script( 'gmaps', THEME_SCRIPT. 'gmaps.js' , '', '3.0', false );
	}
}



/* 
 * Funcion To Call Option
 * 
 * @since  1.0
 */
function indecon_option( $name ) {
    return vp_option( "indecon_option." . $name );
}