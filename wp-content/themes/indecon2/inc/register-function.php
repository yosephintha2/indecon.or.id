<?php 

add_action("wp_ajax_indecon_user_registration", "indecon_user_registration");
add_action("wp_ajax_nopriv_indecon_user_registration", "indecon_user_registration");
function indecon_user_registration() {

if ( $_POST ) {

	//check if its an ajax request, exit if not
    if ( ! isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) AND strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) != 'xmlhttprequest' ) {
	
		//exit script outputting json data
		$output = json_encode(
		array(
			'type'	=> 'error', 
			'text'	=> 'Request must come from Ajax'
		));
		
		die( $output );
    } 

    //check $_POST vars are set, exit if any missing
	if ( ! isset( $_POST["userName"] ) || ! isset( $_POST["userEmail"] ) || ! isset( $_POST["userPhone"] ) || ! isset( $_POST["userMessage"] ) )
	{
		$output = json_encode( array( 'type' => 'error', 'text' => 'Input fields are empty!' ) );
		die( $output );
	}


	$password 			= filter_var( $_POST["userPassword"], FILTER_SANITIZE_STRING );
	$confirm_password 	= filter_var( $_POST["confirmPassword"], FILTER_SANITIZE_STRING );
	$website 			= filter_var( $_POST["userWebsite"], FILTER_SANITIZE_URL );
	$email 				= filter_var( $_POST["userEmail"], FILTER_SANITIZE_EMAIL );
	$username 			= filter_var( $_POST["userName"], FILTER_SANITIZE_STRING );
	$description 		= filter_var( $_POST["userDescription"], FILTER_SANITIZE_STRING );
	$fullname 			= filter_var( $_POST["userFullname"], FILTER_SANITIZE_STRING );
	$organization 		= filter_var( $_POST["userOrganization"], FILTER_SANITIZE_STRING );
	$profession 		= filter_var( $_POST["userProfession"], FILTER_SANITIZE_STRING );
	$division 			= filter_var( $_POST["userDivision"], FILTER_SANITIZE_STRING );
	$address 			= filter_var( $_POST["userAddress"], FILTER_SANITIZE_STRING );
	$phone_number		= filter_var( $_POST["userPhone"], FILTER_SANITIZE_STRING );
	$motivasi			= filter_var( $_POST["userMotivation"], FILTER_SANITIZE_STRING );
	$member_type		= filter_var( $_POST["memberType"], FILTER_SANITIZE_STRING );
	$contact_of_pj		= filter_var( $_POST["pjContact"], FILTER_SANITIZE_STRING );
	$position_of_pj		= filter_var( $_POST["pjPosition"], FILTER_SANITIZE_STRING );
	$personal_address	= filter_var( $_POST["personaAddress"], FILTER_SANITIZE_STRING );
	$phone_of_pj		= filter_var( $_POST["pjPhone"], FILTER_SANITIZE_STRING );
	$email_of_pj		= filter_var( $_POST["pjEmail"], FILTER_SANITIZE_STRING );
	$gambar_profile		= filter_var( $_POST["userAvatar"], FILTER_SANITIZE_STRING );

	//additional php validation
	if ( strlen( $username ) < 4 ) // If length is less than 4 it will throw an HTTP error.
	{
		$output = json_encode( array( 'type' => 'error', 'text' => 'Name is too short or empty!' ) );
		die( $output );
	}

	if ( ! filter_var( $email , FILTER_VALIDATE_EMAIL ) ) //email validation
	{
		$output = json_encode( array( 'type' => 'error', 'text' => 'Please enter a valid email!' ) );
		die( $output );
	}

	if ( ! is_numeric( $phone_number ) ) //check entered data is numbers
	{
		$output = json_encode( array( 'type' => 'error', 'text' => 'Only numbers allowed in phone field' ) );
		die( $output );
	}

	if ( strlen( $description ) < 5 ) //check emtpy message
	{
		$output = json_encode( array( 'type' => 'error', 'text' => 'Too short message! Please enter something.' ) );
		die( $output );
	}

	$user_id = wp_insert_user( array(
			'user_email' 	=> $email,
			'user_login' 	=> $username,
			'user_pass' 	=> $password,
			'display_name' 	=> $username,
			'role' 			=> 'contributor',
		)
	);

	if ( $user_id ) {

		$post = array( //our wp_insert_post args
					'post_title'	=> wp_strip_all_tags( $organization ),
					'post_status'	=> 'draft',
					'post_type' 	=> 'network',

				);
			 
		$my_post_id = wp_insert_post( $post ); //send our post, save the resulting ID

		if ( $my_post_id ) {
			update_post_meta( $my_post_id, 'memberType', $member_type );
			update_post_meta( $my_post_id, 'userFullname', esc_attr( $fullname ) );
			update_post_meta( $my_post_id, 'userOrganization', esc_attr( $organization ) );
			update_post_meta( $my_post_id, 'userProfession', esc_attr( $profession ) );
			update_post_meta( $my_post_id, 'userDivision', esc_attr( $division ) );
			update_post_meta( $my_post_id, 'userAddress', esc_attr( $address ) );
			update_post_meta( $my_post_id, 'userPhone', esc_attr( $phone_number ) );
			update_post_meta( $my_post_id, 'userMotivation', esc_attr( $motivasi ) );
			update_post_meta( $my_post_id, 'userDescription', esc_attr( $description ) );
			update_post_meta( $my_post_id, 'userWebsite', esc_attr( $website ) );

			update_post_meta( $my_post_id, 'pjPhone', esc_attr( $contact_of_pj ) );
			update_post_meta( $my_post_id, 'personaAddress', esc_attr( $personal_address ) );
			update_post_meta( $my_post_id, 'pjPosition', esc_attr( $position_of_pj ) );
			update_post_meta( $my_post_id, 'pjPhone', esc_attr( $phone_of_pj ) );
			update_post_meta( $my_post_id, 'pjEmail', esc_attr( $email_of_pj ) );

			
			 if (!function_exists('wp_generate_attachment_metadata')){
				require_once( ABSPATH . "wp-admin" . '/includes/image.php');
				require_once( ABSPATH . "wp-admin" . '/includes/file.php');
				require_once( ABSPATH . "wp-admin" . '/includes/media.php');
			}
			 if ( $_FILES ) {
				foreach ( $_FILES as $file => $array ) {
					if ( $_FILES[$file]['error'] !== UPLOAD_ERR_OK ) {
						return "upload error : " . $_FILES[$file]['error'];
					}
					$attach_id = media_handle_upload( $file, $my_post_id );
				}   
			}
			if ( $attach_id > 0 ){
				//and if you want to set that image as Post  then use:
				update_post_meta( $my_post_id,'_thumbnail_id', $attach_id );
			}
		}

		$from 		= get_option( 'admin_email' );
		$headers 	= 'From: '.$from . "\r\n";
		$subject 	= "Registration successful";
		$msg 		= "Registration successful.\nYour login details\nUsername: $username\nPassword: $password";
		wp_mail( $email, $subject, $msg, $headers );

	} else {
		$output = json_encode( array( 'type' => 'error', 'text' => 'Failed submit new network' ) );
		die( $output );
	}

}

}

if ( ! is_admin() ) {
	add_action( 'init', 'indecon_script_enqueuer' );
}
function indecon_script_enqueuer() {
   wp_register_script( "indecon_registration", THEME_SCRIPT .'/registration.js', array('jquery') );
   wp_localize_script( 'indecon_registration', 'registrationAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        

   wp_enqueue_script( 'indecon_registration' );

}