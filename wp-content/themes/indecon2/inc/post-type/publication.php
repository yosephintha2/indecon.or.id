<?php 

/**
 * Register Post Type Publication
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_cpt_publication' );
function register_cpt_publication() {

    $labels = array( 
        'name' 					=> _x( 'Publications', 'publication' ),
        'singular_name' 		=> _x( 'Publication', 'publication' ),
        'add_new' 				=> _x( 'Add New', 'publication' ),
        'add_new_item' 			=> _x( 'Add New Publication', 'publication' ),
        'edit_item' 			=> _x( 'Edit Publication', 'publication' ),
        'new_item' 				=> _x( 'New Publication', 'publication' ),
        'view_item' 			=> _x( 'View Publication', 'publication' ),
        'search_items' 			=> _x( 'Search Publications', 'publication' ),
        'not_found' 			=> _x( 'No publications found', 'publication' ),
        'not_found_in_trash' 	=> _x( 'No publications found in Trash', 'publication' ),
        'parent_item_colon' 	=> _x( 'Parent Publication:', 'publication' ),
        'menu_name' 			=> _x( 'Publication', 'publication' ),
    );

    $args = array( 
        'labels' 				=> $labels,
        'hierarchical' 			=> true,
        'description' 			=> 'Publication Post Type',
        'supports' 				=> array( 'title', 'editor', 'thumbnail' ),
        
        'public' 				=> true,
        'show_ui' 				=> true,
        'show_in_menu' 			=> true,
        'menu_position' 		=> 5,
        'menu_icon' 			=> 'dashicons-admin-site',
        'show_in_nav_menus' 	=> true,
        'publicly_queryable' 	=> true,
        'exclude_from_search'	=> false,
        'has_archive' 			=> false,
        'query_var' 			=> true,
        'can_export' 			=> true,
        'rewrite' 				=> true,
        'capability_type' 		=> 'post'
    );

    register_post_type( 'publication', $args );
}

