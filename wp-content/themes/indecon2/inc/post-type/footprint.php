<?php 

/**
 * Register Post Type Footprint
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_post_type_footprint' );
function register_post_type_footprint() {

    $labels = array( 
        'name' 					=> _x( 'Footprints', 'footprint', 'indecon' ),
        'singular_name' 		=> _x( 'Footprint', 'footprint', 'indecon' ),
        'add_new' 				=> _x( 'Add New', 'footprint', 'indecon' ),
        'add_new_item' 			=> _x( 'Add New Footprint', 'footprint', 'indecon' ),
        'edit_item' 			=> _x( 'Edit Footprint', 'footprint', 'indecon' ),
        'new_item' 				=> _x( 'New Footprint', 'footprint', 'indecon' ),
        'view_item' 			=> _x( 'View Footprint', 'footprint', 'indecon' ),
        'search_items' 			=> _x( 'Search Footprints', 'footprint', 'indecon' ),
        'not_found' 			=> _x( 'No footprints found', 'footprint', 'indecon' ),
        'not_found_in_trash' 	=> _x( 'No footprints found in Trash', 'footprint', 'indecon' ),
        'parent_item_colon' 	=> _x( 'Parent Footprint:', 'footprint', 'indecon' ),
        'menu_name' 			=> _x( 'Footprint', 'footprint', 'indecon' ),
    );

    $args = array( 
        'labels' 				=> $labels,
        'hierarchical' 			=> true,
        'description' 			=> 'Footprint Post type',
        'supports' 				=> array( 'title', 'excerpt' ),
        
        'public' 				=> true,
        'show_ui' 				=> true,
        'show_in_menu' 			=> true,
        'menu_position' 		=> 31,
        'menu_icon' 			=> 'dashicons-share',
        'show_in_nav_menus' 	=> true,
        'publicly_queryable' 	=> true,
        'exclude_from_search' 	=> false,
        'has_archive' 			=> false,
        'query_var' 			=> true,
        'can_export' 			=> true,
        'rewrite' 				=> true,
        'capability_type' 		=> 'post'
    );

    register_post_type( 'footprint', $args );
}