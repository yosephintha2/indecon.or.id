<?php 

/**
 * Register Research Post Type
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_post_type_research' );
function register_post_type_research() {

    $labels = array( 
        'name' 					=> _x( 'Researchs', 'research', 'indecon' ),
        'singular_name' 		=> _x( 'Research', 'research', 'indecon' ),
        'add_new' 				=> _x( 'Add New', 'research', 'indecon' ),
        'add_new_item' 			=> _x( 'Add New Research', 'research', 'indecon' ),
        'edit_item' 			=> _x( 'Edit Research', 'research', 'indecon' ),
        'new_item' 				=> _x( 'New Research', 'research', 'indecon' ),
        'view_item' 			=> _x( 'View Research', 'research', 'indecon' ),
        'search_items' 			=> _x( 'Search Researches', 'research', 'indecon' ),
        'not_found' 			=> _x( 'No researchs found', 'research', 'indecon' ),
        'not_found_in_trash' 	=> _x( 'No researchs found in Trash', 'research', 'indecon' ),
        'parent_item_colon' 	=> _x( 'Parent Research:', 'research', 'indecon' ),
        'menu_name' 			=> _x( 'Research', 'research', 'indecon' ),
    );

    $args = array( 
        'labels' 				=> $labels,
        'hierarchical' 			=> true,
        'description' 			=> 'Research Post Type',
        'supports' 				=> array( 'title', 'editor', 'thumbnail' ),
        
        'public' 				=> true,
        'show_ui' 				=> true,
        'show_in_menu' 			=> true,
        'menu_position' 		=> 29,
        'menu_icon' 			=> 'dashicons-lightbulb',
        'show_in_nav_menus' 	=> true,
        'publicly_queryable' 	=> true,
        'exclude_from_search' 	=> false,
        'has_archive' 			=> false,
        'query_var' 			=> true,
        'can_export' 			=> true,
        'rewrite' 				=> true,
        'capability_type' 		=> 'post'
    );

    register_post_type( 'research', $args );
}

/**
 * Register Custom Taxonomy
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_taxonomy_tahun_research', 0 );
function register_taxonomy_tahun_research() {

    $labels = array(
        'name'                       => _x( 'Tahun Research', 'Taxonomy General Name', 'indecon' ),
        'singular_name'              => _x( 'Tahun Research', 'Taxonomy Singular Name', 'indecon' ),
        'menu_name'                  => __( 'Tahun Research', 'indecon' ),
        'all_items'                  => __( 'All Items', 'indecon' ),
        'parent_item'                => __( 'Parent Item', 'indecon' ),
        'parent_item_colon'          => __( 'Parent Item:', 'indecon' ),
        'new_item_name'              => __( 'New Item Name', 'indecon' ),
        'add_new_item'               => __( 'Add New Item', 'indecon' ),
        'edit_item'                  => __( 'Edit Item', 'indecon' ),
        'update_item'                => __( 'Update Item', 'indecon' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'indecon' ),
        'search_items'               => __( 'Search Items', 'indecon' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'indecon' ),
        'choose_from_most_used'      => __( 'Choose from the most used items', 'indecon' ),
        'not_found'                  => __( 'Not Found', 'indecon' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_in_menu'               => true,
        'menu_position'              => 28,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'query_var'                  => 'tahun_research',
    );
    register_taxonomy( 'tahun_research', array( 'research' ), $args );

}


/**
 * Define custom columns
 *
 * @return void
 * @author alispx
 **/
add_filter( 'manage_edit-research_columns', 'indecon_edit_research_columns' ) ;
function indecon_edit_research_columns( $columns ) {

    $columns = array(
        'cb'                => '<input type="checkbox" />',
        'title'             => __( 'Judul Riset', 'indecon' ),
        'research_img'      => __( 'Gambar Riset', 'indecon' ),
        'tahun_research'    => __( 'Tahun Riset', 'indecon' ),
        'date'              => __( 'Tanggal', 'indecon' )
    );

    return $columns;
}

/**
 * Custom columns content
 *
 * @return void
 * @author alispx
 **/
add_action( 'manage_research_posts_custom_column', 'indecon_manage_research_columns', 10, 2 );
function indecon_manage_research_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        case 'research_img' :

            $img = indecon_research_column_image( $post_id );

            if ( $img ) {
                echo '<img src="' . esc_url( $img ) . '" />';
            } else {
                echo __( 'Not Available', 'indecon' );
            } 

            break;

        case 'tahun_research' :

            $terms = get_the_terms( $post_id, 'tahun_research' );

            if ( ! empty( $terms ) ) {

                $out = array();

                foreach ( $terms as $term ) {
                    $out[] = sprintf( '<a href="%s">%s</a>',
                        esc_url( add_query_arg( array( 'post_type' => 'research', 'tahun_research' => $term->slug ), 'edit.php' ) ),
                        esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'tahun_research', 'display' ) )
                    );
                }

                echo join( ', ', $out );
            }

            else {
                _e( 'N/A', 'indecon' );
            }

            break;

        default :
            break;
    }
}

/**
 * Get the featured image.
 * 
 * @since 1.0
 */
function indecon_research_column_image( $post_id ) {

    $post_thumbnail_id = get_post_thumbnail_id( $post_id );

    if ( $post_thumbnail_id ) {
        $post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'small' );
        return $post_thumbnail_img[0];
    }

}