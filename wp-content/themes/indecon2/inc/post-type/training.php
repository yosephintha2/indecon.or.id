<?php 

/**
 * Register Post Type Training
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_post_type_training' );
function register_post_type_training() {

	$labels = array( 
		'name' 					=> _x( 'Trainings', 'training', 'indecon' ),
		'singular_name' 		=> _x( 'Training', 'training', 'indecon' ),
		'add_new' 				=> _x( 'Add New', 'training', 'indecon' ),
		'add_new_item' 			=> _x( 'Add New Training', 'training', 'indecon' ),
		'edit_item' 			=> _x( 'Edit Training', 'training', 'indecon' ),
		'new_item' 				=> _x( 'New Training', 'training', 'indecon' ),
		'view_item' 			=> _x( 'View Training', 'training', 'indecon' ),
		'search_items' 			=> _x( 'Search Trainings', 'training', 'indecon' ),
		'not_found' 			=> _x( 'No trainings found', 'training', 'indecon' ),
		'not_found_in_trash' 	=> _x( 'No trainings found in Trash', 'training', 'indecon' ),
		'parent_item_colon' 	=> _x( 'Parent Training:', 'training', 'indecon' ),
		'menu_name' 			=> _x( 'Training', 'training', 'indecon' ),
	);

	$args = array( 
		'labels' 				=> $labels,
		'hierarchical' 			=> true,
		'description' 			=> 'Training Post Type',
		'supports' 				=> array( 'title', 'editor', 'thumbnail' ),
		
		'public' 				=> true,
		'show_ui' 				=> true,
		'show_in_menu' 			=> true,
		'menu_position' 		=> 30,
		'menu_icon' 			=> 'dashicons-list-view',
		'show_in_nav_menus' 	=> true,
		'publicly_queryable' 	=> true,
		'exclude_from_search' 	=> false,
		'has_archive' 			=> false,
		'query_var' 			=> true,
		'can_export' 			=> true,
		'rewrite' 				=> true,
		'capability_type' 		=> 'post'
	);

	register_post_type( 'training', $args );
}

/**
 * Register Custom Taxonomy
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_taxonomy_tahun_training', 0 );
function register_taxonomy_tahun_training() {

	$labels = array(
		'name'                       => _x( 'Tahun Training', 'Taxonomy General Name', 'indecon' ),
		'singular_name'              => _x( 'Tahun Training', 'Taxonomy Singular Name', 'indecon' ),
		'menu_name'                  => __( 'Tahun Training', 'indecon' ),
		'all_items'                  => __( 'All Items', 'indecon' ),
		'parent_item'                => __( 'Parent Item', 'indecon' ),
		'parent_item_colon'          => __( 'Parent Item:', 'indecon' ),
		'new_item_name'              => __( 'New Item Name', 'indecon' ),
		'add_new_item'               => __( 'Add New Item', 'indecon' ),
		'edit_item'                  => __( 'Edit Item', 'indecon' ),
		'update_item'                => __( 'Update Item', 'indecon' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'indecon' ),
		'search_items'               => __( 'Search Items', 'indecon' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'indecon' ),
		'choose_from_most_used'      => __( 'Choose from the most used items', 'indecon' ),
		'not_found'                  => __( 'Not Found', 'indecon' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_in_menu'          	 => true,
        'menu_position'         	 => 29,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'query_var'                  => 'tahun_training',
	);
	register_taxonomy( 'tahun_training', array( 'training' ), $args );

}

/**
 * Define custom columns
 *
 * @return void
 * @author alispx
 **/
add_filter( 'manage_edit-training_columns', 'indecon_edit_training_columns' ) ;
function indecon_edit_training_columns( $columns ) {

	$columns = array(
		'cb'                => '<input type="checkbox" />',
		'title'             => __( 'Judul Training', 'indecon' ),
		'training_img'      => __( 'Gambar Training', 'indecon' ),
		'tahun_training'    => __( 'Tahun Training', 'indecon' ),
		'date'              => __( 'Tanggal', 'indecon' )
	);

	return $columns;
}

/**
 * Custom columns content
 *
 * @return void
 * @author alispx
 **/
add_action( 'manage_training_posts_custom_column', 'indecon_manage_training_columns', 10, 2 );
function indecon_manage_training_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		case 'training_img' :

			$img = indecon_training_column_image( $post_id );

			if ( $img ) {
				echo '<img src="' . esc_url( $img ) . '" />';
			} else {
				echo __( 'Not Available', 'indecon' );
			} 

			break;

		case 'tahun_training' :

			$terms = get_the_terms( $post_id, 'tahun_training' );

			if ( ! empty( $terms ) ) {

				$out = array();

				foreach ( $terms as $term ) {
					$out[] = sprintf( '<a href="%s">%s</a>',
						esc_url( add_query_arg( array( 'post_type' => 'training', 'tahun_training' => $term->slug ), 'edit.php' ) ),
						esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'tahun_training', 'display' ) )
					);
				}

				echo join( ', ', $out );
			}

			else {
				_e( 'N/A', 'indecon' );
			}

			break;

		default :
			break;
	}
}

/**
 * Get the featured image.
 * 
 * @since 1.0
 */
function indecon_training_column_image( $post_id ) {

	$post_thumbnail_id = get_post_thumbnail_id( $post_id );

	if ( $post_thumbnail_id ) {
		$post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'small' );
		return $post_thumbnail_img[0];
	}

}