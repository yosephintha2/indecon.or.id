<?php 

/**
 * Register post type slider
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_post_type_slider' );
function register_post_type_slider() {

    $labels = array( 
        'name'                  => _x( 'Sliders', 'slider', 'indecon' ),
        'singular_name'         => _x( 'Slider', 'slider', 'indecon' ),
        'add_new'               => _x( 'Add New', 'slider', 'indecon' ),
        'add_new_item'          => _x( 'Add New Slider', 'slider', 'indecon' ),
        'edit_item'             => _x( 'Edit Slider', 'slider', 'indecon' ),
        'new_item'              => _x( 'New Slider', 'slider', 'indecon' ),
        'view_item'             => _x( 'View Slider', 'slider', 'indecon' ),
        'search_items'          => _x( 'Search Sliders', 'slider', 'indecon' ),
        'not_found'             => _x( 'No sliders found', 'slider', 'indecon' ),
        'not_found_in_trash'    => _x( 'No sliders found in Trash', 'slider', 'indecon' ),
        'parent_item_colon'     => _x( 'Parent Slider:', 'slider', 'indecon' ),
        'menu_name'             => _x( 'Slider', 'slider', 'indecon' ),
    );

    $args = array( 
        'labels'                => $labels,
        'hierarchical'          => false,
        'description'           => 'Slider post type to handle slider',
        'supports'              => array( 'title', 'excerpt', 'thumbnail' ),
        
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 61,
        'menu_icon'             => 'dashicons-slides',
        'show_in_nav_menus'     => true,
        'publicly_queryable'    => true,
        'exclude_from_search'   => false,
        'has_archive'           => false,
        'query_var'             => true,
        'can_export'            => true,
        'rewrite'               => true,
        'capability_type'       => 'post'
    );

    register_post_type( 'slider', $args );
}

/**
 * Define custom columns
 *
 * @return void
 * @author alispx
 **/
add_filter( 'manage_edit-slider_columns', 'indecon_edit_slider_columns' ) ;
function indecon_edit_slider_columns( $columns ) {

    $columns = array(
        'cb'                => '<input type="checkbox" />',
        'title'             => __( 'Slider Title', 'indecon' ),
        'slider_img'        => __( 'Slider Image', 'indecon' ),
        'date'              => __( 'Date', 'indecon' )
    );

    return $columns;
}

/**
 * Custom columns content
 *
 * @return void
 * @author alispx
 **/
add_action( 'manage_slider_posts_custom_column', 'indecon_manage_slider_columns', 10, 2 );
function indecon_manage_slider_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        case 'slider_img' :

            $img = indecon_slider_column_image( $post_id );

            if ( $img ) {
                echo '<img src="' . esc_url( $img ) . '" />';
            } else {
                echo __( 'Not Available', 'indecon' );
            } 

            break;

        default :
            break;
    }
}

/**
 * Get the featured image.
 * 
 * @since 1.0
 */
function indecon_slider_column_image( $post_id ) {

    $post_thumbnail_id = get_post_thumbnail_id( $post_id );

    if ( $post_thumbnail_id ) {
        $post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'small' );
        return $post_thumbnail_img[0];
    }

}