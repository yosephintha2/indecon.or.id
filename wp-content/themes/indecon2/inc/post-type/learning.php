<?php 

/**
 * Register Post Type Learning
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_cpt_learning' );
function register_cpt_learning() {

	$labels = array( 
		'name' 					=> _x( 'Learnings', 'learning', 'indecon' ),
		'singular_name' 		=> _x( 'Learning', 'learning', 'indecon' ),
		'add_new' 				=> _x( 'Add New', 'learning', 'indecon' ),
		'add_new_item' 			=> _x( 'Add New Learning', 'learning', 'indecon' ),
		'edit_item' 			=> _x( 'Edit Learning', 'learning', 'indecon' ),
		'new_item' 				=> _x( 'New Learning', 'learning', 'indecon' ),
		'view_item' 			=> _x( 'View Learning', 'learning', 'indecon' ),
		'search_items' 			=> _x( 'Search Learnings', 'learning', 'indecon' ),
		'not_found' 			=> _x( 'No learnings found', 'learning', 'indecon' ),
		'not_found_in_trash' 	=> _x( 'No learnings found in Trash', 'learning', 'indecon' ),
		'parent_item_colon' 	=> _x( 'Parent Learning:', 'learning', 'indecon' ),
		'menu_name' 			=> _x( 'Learning', 'learning', 'indecon' ),
	);

	$args = array( 
		'labels' 				=> $labels,
		'hierarchical' 			=> true,
		'description' 			=> 'Learning Center Post Type',
		'supports' 				=> array( 'title', 'editor', 'thumbnail' ),
		
		'public' 				=> true,
		'show_ui' 				=> true,
		'show_in_menu' 			=> true,
		'menu_position' 		=> 30,
		'menu_icon' 			=> 'dashicons-welcome-learn-more',
		'show_in_nav_menus' 	=> true,
		'publicly_queryable' 	=> true,
		'exclude_from_search' 	=> false,
		'has_archive' 			=> false,
		'query_var' 			=> true,
		'can_export' 			=> true,
		'rewrite' 				=> true,
		'capability_type' 		=> 'post'
	);

	register_post_type( 'learning', $args );
}

/**
 * Define custom columns
 *
 * @return void
 * @author alispx
 **/
add_filter( 'manage_edit-learning_columns', 'indecon_edit_learning_columns' ) ;
function indecon_edit_learning_columns( $columns ) {

	$columns = array(
		'cb'                => '<input type="checkbox" />',
		'title'             => __( 'Learning Title', 'indecon' ),
		'learning_img'      => __( 'Learning Image', 'indecon' ),
		'learning_type'     => __( 'Learning Type', 'indecon' ),
		'date'              => __( 'Date', 'indecon' )
	);

	return $columns;
}

/**
 * Custom columns content
 *
 * @return void
 * @author alispx
 **/
add_action( 'manage_learning_posts_custom_column', 'indecon_manage_learning_columns', 10, 2 );
function indecon_manage_learning_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		case 'learning_img' :

			$img = indecon_learning_column_image( $post_id );

			if ( $img ) {
				echo '<img src="' . esc_url( $img ) . '" />';
			} else {
				echo __( 'Not Available', 'indecon' );
			} 

			break;

		case 'learning_type' :

			$learning_type = get_post_meta( $post_id, '_learning_type', false );

			if ( ! empty( $learning_type ) ) {
				echo $learning_type[0];	
			}

			break;

		default :
			break;
	}
}

/**
 * Get the featured image.
 * 
 * @since 1.0
 */
function indecon_learning_column_image( $post_id ) {

	$post_thumbnail_id = get_post_thumbnail_id( $post_id );

	if ( $post_thumbnail_id ) {
		$post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'small' );
		return $post_thumbnail_img[0];
	}

}