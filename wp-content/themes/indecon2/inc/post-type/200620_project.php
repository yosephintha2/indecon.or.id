<?php 

/**
 * Register post type project
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_post_type_project' );
function register_post_type_project() {

	$labels = array( 
		'name' 					=> _x( 'Projects', 'project', 'indecon' ),
		'singular_name' 		=> _x( 'Project', 'project', 'indecon' ),
		'add_new' 				=> _x( 'Add New', 'project', 'indecon' ),
		'add_new_item' 			=> _x( 'Add New Project', 'project', 'indecon' ),
		'edit_item' 			=> _x( 'Edit Project', 'project', 'indecon' ),
		'new_item' 				=> _x( 'New Project', 'project', 'indecon' ),
		'view_item' 			=> _x( 'View Project', 'project', 'indecon' ),
		'search_items' 			=> _x( 'Search Projects', 'project', 'indecon' ),
		'not_found' 			=> _x( 'No projects found', 'project', 'indecon' ),
		'not_found_in_trash' 	=> _x( 'No projects found in Trash', 'project', 'indecon' ),
		'parent_item_colon' 	=> _x( 'Parent Project:', 'project', 'indecon' ),
		'menu_name' 			=> _x( 'Project', 'project', 'indecon' ),
	);

	$args = array( 
		'labels' 				=> $labels,
		'hierarchical' 			=> true,
		'description' 			=> 'Project Post Type',
		'supports' 				=> array( 'title', 'editor', 'thumbnail' ),
		'taxonomies' 			=> array( 'tahun' ),
		'public' 				=> true,
		'show_ui' 				=> true,
		'show_in_menu' 			=> true,
		'menu_position' 		=> 28,
		'menu_icon' 			=> 'dashicons-feedback',
		'show_in_nav_menus' 	=> true,
		'publicly_queryable' 	=> true,
		'exclude_from_search' 	=> false,
		'has_archive' 			=> false,
		'query_var' 			=> true,
		'can_export' 			=> true,
		'rewrite' 				=> true,
		'capability_type' 		=> 'post'
	);

	register_post_type( 'project', $args );
}

/**
 * Register Custom Taxonomy
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_taxonomy_tahun', 0 );
function register_taxonomy_tahun() {

	$labels = array(
		'name'                       => _x( 'Tahun Project', 'Taxonomy General Name', 'indecon' ),
		'singular_name'              => _x( 'Tahun Project', 'Taxonomy Singular Name', 'indecon' ),
		'menu_name'                  => __( 'Tahun Project', 'indecon' ),
		'all_items'                  => __( 'All Items', 'indecon' ),
		'parent_item'                => __( 'Parent Item', 'indecon' ),
		'parent_item_colon'          => __( 'Parent Item:', 'indecon' ),
		'new_item_name'              => __( 'New Item Name', 'indecon' ),
		'add_new_item'               => __( 'Add New Item', 'indecon' ),
		'edit_item'                  => __( 'Edit Item', 'indecon' ),
		'update_item'                => __( 'Update Item', 'indecon' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'indecon' ),
		'search_items'               => __( 'Search Items', 'indecon' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'indecon' ),
		'choose_from_most_used'      => __( 'Choose from the most used items', 'indecon' ),
		'not_found'                  => __( 'Not Found', 'indecon' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_in_menu'          	 => true,
        'menu_position'         	 => 27,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'query_var'                  => 'tahun_project',
	);
	register_taxonomy( 'tahun', array( 'project' ), $args );

}


/**
 * Define custom columns
 *
 * @return void
 * @author alispx
 **/
add_filter( 'manage_edit-project_columns', 'indecon_edit_project_columns' ) ;
function indecon_edit_project_columns( $columns ) {

	$columns = array(
		'cb'            => '<input type="checkbox" />',
		'title'         => __( 'Project Name' ),
		'project_img'   => __( 'Project Image' ),
		'tahun'         => __( 'Tahun' ),
		'date'          => __( 'Date' )
	);

	return $columns;
}

/**
 * Custom columns content
 *
 * @return void
 * @author alispx
 **/
add_action( 'manage_project_posts_custom_column', 'indecon_manage_project_columns', 10, 2 );
function indecon_manage_project_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		case 'project_img' :

			$img = indecon_project_column_image( $post_id );

			if ( $img ) {
				echo '<img src="' . esc_url( $img ) . '" />';
			} else {
				echo __( 'Not Available', 'indecon' );
			} 

			break;

		case 'tahun' :

			$terms = get_the_terms( $post_id, 'tahun' );

			if ( ! empty( $terms ) ) {

				$out = array();

				foreach ( $terms as $term ) {
					$out[] = sprintf( '<a href="%s">%s</a>',
						esc_url( add_query_arg( array( 'post_type' => 'project', 'tahun' => $term->slug ), 'edit.php' ) ),
						esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'tahun', 'display' ) )
					);
				}

				echo join( ', ', $out );
			}

			else {
				_e( 'N/A', 'indecon' );
			}

			break;

		default :
			break;
	}
}

/**
 * Get the featured image.
 * 
 * @since 1.0
 */
function indecon_project_column_image( $post_id ) {

	$post_thumbnail_id = get_post_thumbnail_id( $post_id );

	if ( $post_thumbnail_id ) {
		$post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'small' );
		return $post_thumbnail_img[0];
	}

}