<?php 

/**
 * Register Post Type Team
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_post_type_team' );
function register_post_type_team() {

    $labels = array( 
        'name' 					=> _x( 'Teams', 'team', 'indecon' ),
        'singular_name' 		=> _x( 'Team', 'team', 'indecon' ),
        'add_new' 				=> _x( 'Add New', 'team', 'indecon' ),
        'add_new_item' 			=> _x( 'Add New Team', 'team', 'indecon' ),
        'edit_item' 			=> _x( 'Edit Team', 'team', 'indecon' ),
        'new_item' 				=> _x( 'New Team', 'team', 'indecon' ),
        'view_item' 			=> _x( 'View Team', 'team', 'indecon' ),
        'search_items' 			=> _x( 'Search Teams', 'team', 'indecon' ),
        'not_found' 			=> _x( 'No teams found', 'team', 'indecon' ),
        'not_found_in_trash' 	=> _x( 'No teams found in Trash', 'team', 'indecon' ),
        'parent_item_colon' 	=> _x( 'Parent Team:', 'team', 'indecon' ),
        'menu_name' 			=> _x( 'Team', 'team', 'indecon' ),
    );

    $args = array( 
        'labels' 				=> $labels,
        'hierarchical' 			=> true,
        'description' 			=> 'Team Post Type',
        'supports' 				=> array( 'title', 'thumbnail' ),
        
        'public' 				=> true,
        'show_ui' 				=> true,
        'show_in_menu' 			=> true,
        'menu_position' 		=> 33,
        'menu_icon' 			=> 'dashicons-groups',
        'show_in_nav_menus' 	=> true,
        'publicly_queryable' 	=> true,
        'exclude_from_search' 	=> false,
        'has_archive' 			=> false,
        'query_var' 			=> true,
        'can_export' 			=> true,
        'rewrite' 				=> true,
        'capability_type' 		=> 'post'
    );

    register_post_type( 'team', $args );
}

/**
 * Define custom columns
 *
 * @return void
 * @author alispx
 **/
add_filter( 'manage_edit-team_columns', 'indecon_edit_team_columns' ) ;
function indecon_edit_team_columns( $columns ) {

    $columns = array(
        'cb'                => '<input type="checkbox" />',
        'title'             => __( 'Team Name', 'indecon' ),
        'team_img'          => __( 'Team Image', 'indecon' ),
        'date'              => __( 'Date', 'indecon' )
    );

    return $columns;
}

/**
 * Custom columns content
 *
 * @return void
 * @author alispx
 **/
add_action( 'manage_team_posts_custom_column', 'indecon_manage_team_columns', 10, 2 );
function indecon_manage_team_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        case 'team_img' :

            $img = indecon_team_column_image( $post_id );

            if ( $img ) {
                echo '<img src="' . esc_url( $img ) . '" />';
            } else {
                echo __( 'Not Available', 'indecon' );
            } 

            break;

        default :
            break;
    }
}

/**
 * Get the featured image.
 * 
 * @since 1.0
 */
function indecon_team_column_image( $post_id ) {

    $post_thumbnail_id = get_post_thumbnail_id( $post_id );

    if ( $post_thumbnail_id ) {
        $post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'small' );
        return $post_thumbnail_img[0];
    }

}