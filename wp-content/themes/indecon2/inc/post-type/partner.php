<?php 

/**
 * Register Post Type Partner
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_post_type_partner' );
function register_post_type_partner() {

    $labels = array( 
        'name' 					=> _x( 'Partners', 'partner', 'indecon' ),
        'singular_name' 		=> _x( 'Partner', 'partner', 'indecon' ),
        'add_new' 				=> _x( 'Add New', 'partner', 'indecon' ),
        'add_new_item' 			=> _x( 'Add New Partner', 'partner', 'indecon' ),
        'edit_item' 			=> _x( 'Edit Partner', 'partner', 'indecon' ),
        'new_item' 				=> _x( 'New Partner', 'partner', 'indecon' ),
        'view_item' 			=> _x( 'View Partner', 'partner', 'indecon' ),
        'search_items' 			=> _x( 'Search Partners', 'partner', 'indecon' ),
        'not_found' 			=> _x( 'No partners found', 'partner', 'indecon' ),
        'not_found_in_trash' 	=> _x( 'No partners found in Trash', 'partner', 'indecon' ),
        'parent_item_colon' 	=> _x( 'Parent Partner:', 'partner', 'indecon' ),
        'menu_name' 			=> _x( 'Partners', 'partner', 'indecon' ),
    );

    $args = array( 
        'labels' 				=> $labels,
        'hierarchical' 			=> true,
        'description' 			=> 'Partner post type',
        'supports' 				=> array( 'title', 'excerpt', 'thumbnail' ),
        
        'public' 				=> true,
        'show_ui' 				=> true,
        'show_in_menu' 			=> true,
        'menu_position' 		=> 32,
        'menu_icon' 			=> 'dashicons-admin-links',
        'show_in_nav_menus' 	=> true,
        'publicly_queryable' 	=> true,
        'exclude_from_search' 	=> false,
        'has_archive' 			=> false,
        'query_var' 			=> true,
        'can_export' 			=> true,
        'rewrite' 				=> true,
        'capability_type' 		=> 'post'
    );

    register_post_type( 'partner', $args );
}