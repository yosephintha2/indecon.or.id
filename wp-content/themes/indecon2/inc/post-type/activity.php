<?php 

/**
 * Register Post Type Activity
 *
 * @return void
 * @author alispx
 **/

add_action( 'init', 'register_cpt_activity' );
function register_cpt_activity() {

    $labels = array( 
        'name'                  => _x( 'Activities', 'activity' ),
        'singular_name'         => _x( 'Activity', 'activity' ),
        'add_new'               => _x( 'Add New', 'activity' ),
        'add_new_item'          => _x( 'Add New Activity', 'activity' ),
        'edit_item'             => _x( 'Edit Activity', 'activity' ),
        'new_item'              => _x( 'New Activity', 'activity' ),
        'view_item'             => _x( 'View Activity', 'activity' ),
        'search_items'          => _x( 'Search Activities', 'activity' ),
        'not_found'             => _x( 'No activities found', 'activity' ),
        'not_found_in_trash'    => _x( 'No activities found in Trash', 'activity' ),
        'parent_item_colon'     => _x( 'Parent Activity:', 'activity' ),
        'menu_name'             => _x( 'Activities', 'activity' ),
    );

    $args = array( 
        'labels'                => $labels,
        'hierarchical'          => true,
        'description'           => 'Activity Post Type',
        'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments' ),
        
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-status',
        
        'show_in_nav_menus'     => true,
        'publicly_queryable'    => true,
        'exclude_from_search'   => false,
        'has_archive'           => false,
        'query_var'             => true,
        'can_export'            => true,
        'rewrite'               => true,
        'capability_type'       => 'post'
    );

    register_post_type( 'activity', $args );
}