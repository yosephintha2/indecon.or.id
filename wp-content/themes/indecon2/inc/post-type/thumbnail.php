<?php 

/**
 * Register Post Type Thumbnail
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_post_type_thumbnail' );
function register_post_type_thumbnail() {

/*
    $labels = array( 
        'name' 					=> _x( 'Thumbnails', 'thumbnail', 'indecon' ),
        'singular_name' 		=> _x( 'Thumbnail', 'thumbnail', 'indecon' ),
        'add_new' 				=> _x( 'Add New', 'thumbnail', 'indecon' ),
        'add_new_item' 			=> _x( 'Add New Thumbnail', 'thumbnail', 'indecon' ),
        'edit_item' 			=> _x( 'Edit Thumbnail', 'thumbnail', 'indecon' ),
        'new_item' 				=> _x( 'New Thumbnail', 'thumbnail', 'indecon' ),
        'view_item' 			=> _x( 'View Thumbnail', 'thumbnail', 'indecon' ),
        'search_items' 			=> _x( 'Search Thumbnails', 'thumbnail', 'indecon' ),
        'not_found' 			=> _x( 'No thumbnails found', 'thumbnail', 'indecon' ),
        'not_found_in_trash' 	=> _x( 'No thumbnails found in Trash', 'thumbnail', 'indecon' ),
        'parent_item_colon' 	=> _x( 'Parent Thumbnail:', 'thumbnail', 'indecon' ),
        'menu_name' 			=> _x( 'Thumbnail', 'thumbnail', 'indecon' ),
    );

    $args = array( 
        'labels' 				=> $labels,
        'hierarchical' 			=> true,
        'description' 			=> 'Thumbnail Post type',
        'supports' 				=> array( 'title', 'excerpt' ),
        
        'public' 				=> true,
        'show_ui' 				=> true,
        'show_in_menu' 			=> true,
        'menu_position' 		=> 31,
        'menu_icon' 			=> 'dashicons-share',
        'show_in_nav_menus' 	=> true,
        'publicly_queryable' 	=> true,
        'exclude_from_search' 	=> false,
        'has_archive' 			=> false,
        'query_var' 			=> true,
        'can_export' 			=> true,
        'rewrite' 				=> true,
        'capability_type' 		=> 'post'
    );

*/

$labels = array( 
        'name' 					=> _x( 'Thumbnails', 'thumbnail', 'indecon' ),
        'singular_name' 		=> _x( 'Thumbnail', 'thumbnail', 'indecon' ),
        'add_new' 				=> _x( 'Add New', 'thumbnail', 'indecon' ),
        'add_new_item' 			=> _x( 'Add New Thumbnail', 'thumbnail', 'indecon' ),
        'edit_item' 			=> _x( 'Edit Thumbnail', 'thumbnail', 'indecon' ),
        'new_item' 				=> _x( 'New Thumbnail', 'thumbnail', 'indecon' ),
        'view_item' 			=> _x( 'View Thumbnail', 'thumbnail', 'indecon' ),
        'search_items' 			=> _x( 'Search Thumbnails', 'thumbnail', 'indecon' ),
        'not_found' 			=> _x( 'No thumbnails found', 'thumbnail', 'indecon' ),
        'not_found_in_trash' 	=> _x( 'No thumbnails found in Trash', 'thumbnail', 'indecon' ),
        'parent_item_colon' 	=> _x( 'Parent Thumbnail:', 'thumbnail', 'indecon' ),
        'menu_name' 			=> _x( 'Thumbnails', 'thumbnail', 'indecon' ),
    );

    $args = array( 
        'labels' 				=> $labels,
        'hierarchical' 			=> true,
        'description' 			=> 'Thumbnail post type',
        'supports' 				=> array( 'title', 'editor', 'thumbnail' ),
        
        'public' 				=> true,
        'show_ui' 				=> true,
        'show_in_menu' 			=> true,
        'menu_position' 		=> 32,
        'menu_icon' 			=> 'dashicons-admin-links',
        'show_in_nav_menus' 	=> true,
        'publicly_queryable' 	=> true,
        'exclude_from_search' 	=> false,
        'has_archive' 			=> false,
        'query_var' 			=> true,
        'can_export' 			=> true,
        'rewrite' 				=> true,
        'capability_type' 		=> 'post'
    );
    register_post_type( 'thumbnail', $args );
}