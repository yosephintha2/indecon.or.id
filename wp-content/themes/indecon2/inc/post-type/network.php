<?php 

/**
 * Register post type network
 *
 * @return void
 * @author alispx
 **/
add_action( 'init', 'register_post_type_network' );
function register_post_type_network() {

    $labels = array( 
        'name' 					=> _x( 'Networks', 'network', 'indecon' ),
        'singular_name' 		=> _x( 'Network', 'network', 'indecon' ),
        'add_new' 				=> _x( 'Add New', 'network', 'indecon' ),
        'add_new_item' 			=> _x( 'Add New Network', 'network', 'indecon' ),
        'edit_item' 			=> _x( 'Edit Network', 'network', 'indecon' ),
        'new_item' 				=> _x( 'New Network', 'network', 'indecon' ),
        'view_item' 			=> _x( 'View Network', 'network', 'indecon' ),
        'search_items' 			=> _x( 'Search Networks', 'network', 'indecon' ),
        'not_found' 			=> _x( 'No networks found', 'network', 'indecon' ),
        'not_found_in_trash' 	=> _x( 'No networks found in Trash', 'network', 'indecon' ),
        'parent_item_colon' 	=> _x( 'Parent Network:', 'network', 'indecon' ),
        'menu_name' 			=> _x( 'Network', 'network', 'indecon' ),
    );

    $args = array( 
        'labels' 				=> $labels,
        'hierarchical' 			=> true,
        'description' 			=> 'Network Post Type',
        'supports' 				=> array( 'title', 'thumbnail' ),
        
        'public' 				=> true,
        'show_ui' 				=> true,
        'show_in_menu' 			=> true,
        'menu_position'         => 60,
        'menu_icon' 			=> 'dashicons-networking',
        'show_in_nav_menus' 	=> true,
        'publicly_queryable' 	=> true,
        'exclude_from_search' 	=> false,
        'has_archive' 			=> false,
        'query_var' 			=> true,
        'can_export' 			=> true,
        'rewrite' 				=> true,
        'capability_type' 		=> 'post'
    );

    register_post_type( 'network', $args );
}