<?php

/**
 * The Template for displaying content of post
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<article <?php hybrid_post_attributes(); ?>>

	<h3 class="title-post"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	<p class="date">
		<?php echo do_shortcode( '[entry-published]' ); ?> - 
		<?php echo do_shortcode( '[entry-author before="Posted by "]' ); ?>
	</p>

	<div class="framebox">
		<?php get_the_image( array( 'size' => 'blog-thumbnail', 'link_to_post' => false ) ); ?>
	</div>
	
	<p><?php echo wp_trim_words( $post->post_content, 55 ); ?></p>

</article>