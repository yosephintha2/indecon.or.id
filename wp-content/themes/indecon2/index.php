<?php

/**
 * The Template for blog index 
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header(); ?>
	
	<div class="row content">


		<div class="grid-8 column post-content">
		
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
				<?php get_template_part( 'content' ); ?>
				
			<?php endwhile; ?>
				<?php get_template_part( 'pagination' ); ?>
			<?php else : ?>
				<?php get_template_part( 'not', 'found' ); ?>
			<?php endif; ?>

		</div>

		<?php get_sidebar( 'primary' ); ?>

	</div>



<?php get_footer(); ?>

<!--<div class="pagination"><?php id_pagination(); ?></div>-->