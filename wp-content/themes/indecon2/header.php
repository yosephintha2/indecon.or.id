<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
<title><?php hybrid_document_title(); ?></title>
<meta name="description" content="indecon">
<meta name="author" content="">
<meta name="keywords" content="Indecon">
<meta name="viewport" content="width=device-width, initial-scale=1" />

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="favicon.png">

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); // wp_head ?>

</head>

<body <?php hybrid_body_attributes(); ?>>

	<!--[if lt IE 7]>
	<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
	<![endif]-->
	<header class="header">

		<div class="row">

			<div class="grid-12 column">
				<div class="head-top">
					<div class="elang">
						<?php do_action('icl_language_selector'); ?><?php indecon_select_language(); ?>
					</div>
					<?php indecon_site_logo(); // Site Logo ?>
					<?php get_template_part( 'social', 'media' ); ?>
				</div>
				<?php get_template_part( 'menu', 'primary' ); // Load menu primary ?>
			</div>
			
		</div>

	</header>    

