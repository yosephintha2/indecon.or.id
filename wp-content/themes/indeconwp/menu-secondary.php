<?php

/**
 * The Template for displaying menu secondary
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="nav-bottom">

	<?php if ( has_nav_menu( 'secondary' ) ) {

		wp_nav_menu(
			array(
				'theme_location'  => 'secondary',
				'menu_class'      => 'menu-bottom',
			)
		);

	} ?>

</div>