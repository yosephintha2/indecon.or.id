<?php

/**
 * The Template for displaying footer of page
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

		<footer class="footer">

			<div class="row">
				<div class="grid-6 column">
					<?php get_template_part( 'menu', 'secondary' ); ?>
					<?php if ( indecon_option( 'footer_credit' ) ) {
						echo indecon_option( 'footer_credit' );
					} else { ?>
						<p>
						<?php sprintf( '&copy; 2014 <a href="%s">%d</a>. %d.</p>', home_url(), bloginfo( 'name' ), __( 'All rigths reserved', 'indecon' ) ); ?>
					<?php } ?>
				</div>
				<div class="grid-6 column">
					<?php get_template_part( 'social', 'media' ); ?>

					<form class="sub-foot" action="<?php echo esc_url( indecon_option( 'subscribe_action_link' ) ); ?>">
						<input type="text" class="grid-5 clean" placeholder="<?php _e( 'Enter your email address', 'indecon' ); ?>">
						<button type="submit" class="button button-small button-primary"><?php _e( 'Subscribe', 'indecon' ); ?></button>
					</form>
				</div>
			</div>

		</footer>

	<?php wp_footer(); // wp_footer ?>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67843004-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>
