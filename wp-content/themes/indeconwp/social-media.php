<?php

/**
 * The Template for displaying social media
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php 
	$facebook         	= indecon_option( 'social_facebook' ); 
	$twitter         	= indecon_option( 'social_twitter' ); 
	$email       	 	= indecon_option( 'social_email' ); 
	$rss      			= indecon_option( 'social_rss' );
?>

<div class="socmed">
	<?php if ( $facebook ) { ?>
		<a href="http://facebook.com/<?php echo esc_attr( $facebook ); ?>"><span class="icon-facebook"></span></a>
	<?php } if ( $twitter ) { ?> 
		<a href="http://twitter.com/<?php echo esc_attr( $twitter ); ?>"><span class="icon-twitter"></span></a>
	<?php } if ( $email ) { ?>
		<a href="mailto:<?php echo esc_attr( $email ); ?>"><span class="icon-email"></span></a>
	<?php } if ( $rss ) { ?>  	
		<a href="<?php echo esc_url( $rss ); ?>"><span class="icon-rss"></span></a>
	<?php } ?>
</div>