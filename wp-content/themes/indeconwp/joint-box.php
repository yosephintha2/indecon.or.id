<?php
/**
 * The Template for displaying Join Box
 *
 * @author 		alispx
 * @version     1.0
 */ 
	global $box_class;
 ?>
<div class="box <?php echo $box_class; ?> text-center">
	<h4>
		<?php if ( indecon_option( 'join_community_title' ) ) {
			echo indecon_option( 'join_community_title' );
		} else {
			_e( 'JOIN OUR COMMUNITY', 'indecon' );
			} ?>
	</h4>

	<p>
		<?php if ( indecon_option( 'join_community_content' ) ) {
			echo indecon_option( 'join_community_content' );
		}else {
			echo 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.';
		} ?>
	</p>

	<a href="<?php echo get_permalink( indecon_option( 'join_community_button_link' ) ) ?>" class="button button-block button-primary"><?php _e( 'join now', 'indecon' ); ?></a>

</div>