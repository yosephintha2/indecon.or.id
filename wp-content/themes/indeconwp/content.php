<?php

/**
 * The Template for displaying content of post
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<h3 class="title-post"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	<p class="date">
		<?php indecon_posted_on(); ?>
	</p>

	<div class="framebox">
		<?php if ( has_post_thumbnail() ) : ?>
			<?php the_post_thumbnail( 'blog-thumbnail' ); ?>
		<?php endif; ?>
	</div>
	
	<p><?php echo wp_trim_words( $post->post_content, 55 ); ?></p>

</article>