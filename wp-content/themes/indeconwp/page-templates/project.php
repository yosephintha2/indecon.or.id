<?php

/*
Template Name: Projects
*/

get_header(); ?>

	<div class="row content">
		<div class="grid-8 column post-content">

				<h3 class="title-gray"><?php _e( 'Project', 'indecon' ); ?></h3>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<p>
						<?php if ( has_post_thumbnail() ) : ?>
							<?php the_post_thumbnail( 'full' ); ?>
						<?php endif; ?>
					</p>
					<?php the_content(); ?>

				<?php endwhile; ?>
				<?php endif; ?>

				<?php $terms = get_terms( 'tahun', 'hide_empty=1&order=DESC' ); ?>
				<ul id="myTab" class="nav nav-tabs" style="padding-top: 30px; margin-bottom:10px;">
					<?php
						$count_term = 1;
						foreach ( $terms as $term ) :
							if ( $count_term == 1 ) { ?>
					  			<li class="active"><a href="#proj-<?php echo esc_attr( $term->name ); ?>"><?php echo esc_attr( $term->name ); ?></a></li>
							<?php } else { ?>
					  			<li><a href="#proj-<?php echo esc_attr( $term->name ); ?>"><?php echo esc_attr( $term->name ); ?></a></li>
							<?php } ?>

					<?php $count_term++; endforeach; ?>
				</ul>
				<div class="tab-content">

					<?php foreach ( $terms as $i => $term ) : ?>
						<?php
						$args = array(
							'post_type' 		=> 'project',
							'posts_per_page'	=> 20,
							'tahun'		=> $term->name,
							'order' 			=> 'DESC'
						);

						?>
						<div class="tab-pane<?php echo  ($i == 0) ? ' active':'';?>" id="proj-<?php echo esc_attr( $term->name ); ?>">
							<ul class="list-line">
							<?php
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post();?>
								<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
							<?php endwhile; ?>
							</ul>
						</div>
						<?php $count_post++;?>
					<?php endforeach;?>
				 </div>
		</div>

		<!-- sidebar -->
		<?php get_sidebar( 'primary' ); ?>
	</div>

<?php get_footer(); ?>
