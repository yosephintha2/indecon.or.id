<?php 
/*
Template Name: Activities
*/
get_header(); ?>
	
	<div class="row content">
		<div class="grid-12 column">
			
			<h3 class="title-gray"><?php _e( 'Activities', 'indecon' ); ?></h3>
			
			
			<?php
				$temp 		= $wp_query; 
				$wp_query 	= null; 
				$wp_query 	= new WP_Query(); 
				$wp_query->query( 'showposts=6&post_type=activity'.'&paged='.$paged ); 
			?>

			 <?php if ( $wp_query->have_posts() ) : ?> 

			 	<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php echo post_class(); ?>>
						
						<div class="grid-4 column">
							<div class="thumb-network">
								<a href="<?php the_permalink(); ?>">
									<?php if ( has_post_thumbnail() ) : ?>
										<?php the_post_thumbnail( 'network-thumbnail' ); ?>
									<?php endif; ?>
								</a>
								<h5 class="title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
								</h5>
								<p class="address">
									<?php echo wp_trim_words( $post->post_content, 10 ); ?>
									<br>
									<a href="<?php the_permalink(); ?>" class="button button-small button-primary"><?php _e( 'View', 'indecon' ); ?></a>
								</p>
							</div>
						</div>
							
					</article>
					
				<?php endwhile; ?>
					
				<?php get_template_part( 'pagination' ); ?>

			<?php endif; ?>
			
			<?php 
			  $wp_query = null; 
			  $wp_query = $temp;  // Reset
			?>

		</div>
		
	</div>

<?php get_footer(); ?>