<?php

/*
Template Name: User Profile
*/

get_header(); 

if ( is_user_logged_in() ) {

	$current_user = wp_get_current_user(); 
	$edit_template = indecon_option( 'user_edit_profile' );
	
	?>

	<div class="row content">
			<?php 
				$network_args = array(
					'post_type' 		=> 'network',
					'posts_per_page'	=> 1,
					'author' 			=> $current_user->ID
					);

				$networks = new WP_Query( $network_args );

			if ( $networks->have_posts() ) : while ( $networks->have_posts() ) : $networks->the_post(); ?>
					
		<div class="grid-8 column post-content">
			
			<?php 
				$edit_network = add_query_arg( 'post', $networks->post->ID, get_permalink( $edit_template ) ); ?>

			<h3 class="title-gray"><?php _e( 'Company Profile', 'indecon' ); ?></h3>
			<span class="edit-profile-button"><a href="<?php echo $edit_network; ?>"><?php _e( 'edit', 'indecon' ); ?></a></span>
			<hr>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<?php 
							$type 			= get_post_meta( get_the_ID(), 'member-type', false );
							$full_name 		= get_post_meta( get_the_ID(), 'full_name', false );
							$profession 	= get_post_meta( get_the_ID(), 'profession', false );
							$division 		= get_post_meta( get_the_ID(), 'division', false );
							$organization 	= get_post_meta( get_the_ID(), 'organization', false );
							$address 		= get_post_meta( get_the_ID(), 'full_address', false );
							$phone_number	= get_post_meta( get_the_ID(), 'phone_number', false );
							$website_url	= get_post_meta( get_the_ID(), 'website_url', false );
							$motivation		= get_post_meta( get_the_ID(), 'motivation', false );
							$description	= get_post_meta( get_the_ID(), 'description', false );
							$contact_of_pj	= get_post_meta( get_the_ID(), 'contact-of-pj', false );
							$address_of_pj	= get_post_meta( get_the_ID(), 'personal_address', false );
							$phone_of_pj	= get_post_meta( get_the_ID(), 'phone-of-pj', false );
							$email_of_pj	= get_post_meta( get_the_ID(), 'pj_email', false );
							$position_of_pj	= get_post_meta( get_the_ID(), 'pj_posision', false );
						 ?>
						 <div class="row">
						 	<div class="grid-12 column">
						 		<table id="table-network">
						 			<tr>
						 				<td valign="top" colspan="2" rowspan="7" width="35%">
						 					<?php if ( has_post_thumbnail() ) : ?>
												<?php the_post_thumbnail( 'blog-thumbnail' ); ?>
											<?php endif; ?></td>
						 				<td width="31%"><strong><?php _e( 'Nama Lengkap : ', 'indecon' ); ?></strong></td>
						 				<td><?php if ( ! empty( $full_name ) ) echo $full_name[0]; ?></td>
						 			</tr>
						 			<tr>
						 				<td><strong><?php _e( 'Profesi : ', 'indecon' ); ?></strong></td>
						 				<td><?php if ( ! empty( $profession ) ) echo $profession[0]; ?></td>
						 			</tr>
						 			<tr>
						 				<td><strong><?php _e( 'Bidang Gerak : ', 'indecon' ); ?></strong></td>
						 				<td><?php if ( ! empty( $division ) ) echo $division[0]; ?></td>
						 			</tr>
						 			<tr>
						 				<td><strong><?php _e( 'Organisasi Tempat Bekerja : ', 'indecon' ); ?></strong></td>
						 				<td><?php if ( ! empty( $organization ) ) echo $organization[0]; ?></td>
						 			</tr>
						 			<tr>
						 				<td><strong><?php _e( 'Alamat : ', 'indecon' ); ?></strong></td>
						 				<td><?php if ( ! empty( $address ) ) echo $address[0]; ?></td>
						 			</tr>
						 			<tr>
						 				<td><strong><?php _e( 'No tlp/fax/Handpone : ', 'indecon' ); ?></strong></td>
						 				<td><?php if ( ! empty( $phone_number ) ) echo $phone_number[0]; ?></td>
						 			</tr>
						 			<tr>
						 				<td><strong><?php _e( 'Website/blog (jika ada) : ', 'indecon' ); ?></strong></td>
						 				<td><?php if ( ! empty( $website_url ) ) echo $website_url[0]; ?></td>
						 			</tr>
						 		</table>
						 		<table>
						 			<tr>
						 				<td width="30%"><strong><?php _e( 'Motivasi bergabung: ', 'indecon' ); ?></strong></td>
						 				<td><?php if ( ! empty( $motivation ) ) echo $motivation[0]; ?></td>
						 			</tr>
						 			<tr>
						 				<td><strong><?php _e( 'Deskripsi singkat tentang anda : ', 'indecon' ); ?></strong></td>
						 				<td><?php if ( ! empty( $description ) ) echo $description[0]; ?></td>
						 			</tr>
								</table>

								<h3><?php _e( 'Oganisasi', 'indecon' ); ?></h3>
								<hr>
								<table>
									<tr>
										<td>
						 					<strong><?php _e( 'Nama Kontak : ', 'indecon' ); ?></strong>
						 				</td>
										<td><?php if ( ! empty( $contact_of_pj ) ) echo $contact_of_pj[0]; ?></td>
									</tr>
									<tr>
										<td><strong><?php _e( 'Jabatan di Lembaga : ', 'indecon' ); ?></strong></td>
										<td><?php if ( ! empty( $position_of_pj ) ) echo $position_of_pj[0]; ?></td>
									</tr>
									<tr>
										<td><strong><?php _e( 'Alamat Pribadi : ', 'indecon' ); ?></strong></td>
										<td><?php if ( ! empty( $address_of_pj ) ) echo $address_of_pj[0]; ?></td>
									</tr>
									<tr>
										<td><strong><?php _e( 'No tlp/fax/Handpone : ', 'indecon' ); ?></strong></td>
										<td><?php if ( ! empty( $phone_of_pj ) ) echo $phone_of_pj[0]; ?></td>
									</tr>
									<tr>
										<td><strong><?php _e( 'Email : ', 'indecon' ); ?></strong></td>
										<td colspan="3"><?php if ( ! empty( $email_of_pj ) ) echo $email_of_pj[0]; ?></td>
									</tr>
						 		</table>
							</div>
						 </div>
					</article>
					
				<?php endwhile; ?>

		
		</div>
		<div class="grid-4 column sidebar">
			<h3><?php _e( 'Active User', 'indecon' ); ?></h3>
			<hr>
				<strong><?php _e( 'welcome, ', 'indecon' ); ?> <?php echo $current_user->user_login; ?></strong>
				<figure>
					<?php echo get_avatar( $current_user->ID, 160 ); ?>
				</figure>
		</div>
		<?php else : ?>
			<div class="grid-10 offset-grid-1 column">
				<h3><?php _e( 'Your account is being reviewed by Administrator. Patiently wait', 'indecon' ); ?></h3>
			</div>
		<?php endif; ?>
	</div>


<?php } else { ?>

	<div class="row content">
		<div class="grid-10 offset-grid-1 column">
			<h3><?php _e( 'You must logged in first to access this page.', 'indecon' ); ?></h3>
		</div>
	</div>

<?php } get_footer(); ?>