<?php

/*
Template Name: Register
*/

get_header(); 

if ( is_user_logged_in() && ! is_super_admin() ) :
	?>
	<div class="row content">
		<div class="grid-10 offset-grid-1 column">
			<h3><?php _e( 'You\'re already registered', 'indecon' ); ?></h3>
		</div>
	</div>
	<?php 

else :

$error = '';
$success = '';
global $wpdb, $PasswordHash, $current_user, $user_ID;

	
$password 			= ( isset( $_POST['password'] ) ) ? $_POST['password'] : '';
$confirm_password 	= ( isset( $_POST['confirm-password'] ) ) ? $_POST['confirm-password'] : '';
$website 			= ( isset( $_POST['website'] ) ) ? $_POST['website'] : '';
$email 				= ( isset( $_POST['email'] ) ) ? $_POST['email'] : '';
$username 			= ( isset( $_POST['username'] ) ) ? $_POST['username'] : '';
$description 		= ( isset( $_POST['description'] ) ) ? $_POST['description'] : '';
$fullname 			= ( isset( $_POST['fullname'] ) ) ? $_POST['fullname'] : '';
$organization 		= ( isset( $_POST['organization'] ) ) ? $_POST['organization'] : '';
$profession 		= ( isset( $_POST['profession'] ) ) ? $_POST['profession'] : '';
$division 			= ( isset( $_POST['division'] ) ) ? $_POST['division'] : '';
$address 			= ( isset( $_POST['address'] ) ) ? $_POST['address'] : '';
$phone_number		= ( isset( $_POST['phone_number'] ) ) ? $_POST['phone_number'] : '';
$motivasi			= ( isset( $_POST['motivation'] ) ) ? $_POST['motivation'] : '';
$member_type		= ( isset( $_POST['member_type'] ) ) ? $_POST['member_type'] : '';
$contact_of_pj		= ( isset( $_POST['pj_contact'] ) ) ? $_POST['pj_contact'] : '';
$position_of_pj		= ( isset( $_POST['pj_position'] ) ) ? $_POST['pj_position'] : '';
$personal_address	= ( isset( $_POST['personal_address'] ) ) ? $_POST['personal_address'] : '';
$phone_of_pj		= ( isset( $_POST['pj_phone'] ) ) ? $_POST['pj_phone'] : '';
$email_of_pj		= ( isset( $_POST['pj_email'] ) ) ? $_POST['pj_email'] : '';
$gambar_profile		= ( isset( $_POST['thumbnail'] ) ) ? $_POST['thumbnail'] : '';

if ( isset( $_POST['task'] ) && $_POST['task'] == 'register' ) {
	
	if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
		$error = 'Invalid email address.';
	} else if( email_exists( $email ) ) {
		$error = 'Email already exist.';
	} else if( $password <> $confirm_password ){
		$error = 'Password do not match.';		
	} else {

		$user_id = wp_insert_user( array(
				'user_email' 	=> $email,
				'user_login' 	=> $username,
				'user_pass' 	=> $password,
				'display_name' 	=> $username,
				'role' 			=> 'contributor',
			)
		);

		if ( $user_id ) {

			$post = array( //our wp_insert_post args
						'post_title'	=> wp_strip_all_tags( $organization ),
						'post_status'	=> 'draft',
						'post_type' 	=> 'network',
						'post_author'	=> $user_id

					);
				 
			$my_post_id = wp_insert_post( $post ); //send our post, save the resulting ID

			if ( $my_post_id ) {
				update_post_meta( $my_post_id, 'member_type', $member_type );
				update_post_meta( $my_post_id, 'full_name', esc_attr( $fullname ) );
				update_post_meta( $my_post_id, 'organization', esc_attr( $organization ) );
				update_post_meta( $my_post_id, 'profession', esc_attr( $profession ) );
				update_post_meta( $my_post_id, 'division', esc_attr( $division ) );
				update_post_meta( $my_post_id, 'full_address', esc_attr( $address ) );
				update_post_meta( $my_post_id, 'phone_number', esc_attr( $phone_number ) );
				update_post_meta( $my_post_id, 'motivation', esc_attr( $motivasi ) );
				update_post_meta( $my_post_id, 'description', esc_attr( $description ) );
				update_post_meta( $my_post_id, 'website_url', esc_attr( $website ) );

				update_post_meta( $my_post_id, 'pj_contact', esc_attr( $contact_of_pj ) );
				update_post_meta( $my_post_id, 'personal_address', esc_attr( $personal_address ) );
				update_post_meta( $my_post_id, 'pj_position', esc_attr( $position_of_pj ) );
				update_post_meta( $my_post_id, 'pj_phone', esc_attr( $phone_of_pj ) );
				update_post_meta( $my_post_id, 'pj_email', esc_attr( $email_of_pj ) );

				
				 if (!function_exists('wp_generate_attachment_metadata')){
					    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
					    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
					    require_once(ABSPATH . "wp-admin" . '/includes/media.php');
					}
					if ( isset ($_FILES['thumbnail']) && $_FILES['thumbnail']['error'] == UPLOAD_ERR_OK ) { 
						$attach_id = media_handle_upload( 'thumbnail', $my_post_id );
						update_post_meta( $my_post_id,'_thumbnail_id', $attach_id ); 
					}
			}

			$from 		= get_option( 'admin_email' );
			$mail_to 	= array( $email, $from );
			$headers 	= 'From: '.$from . "\r\n";
			$subject 	= "Registration successful";
			$msg 		= "Registration successful.\nYour login details\nUsername: $username\nPassword: $password";
			wp_mail( $mail_to, $subject, $msg, $headers );
		}

		if ( is_wp_error( $user_id ) ) {
			$error = 'Error on user creation.';
		} else {
			do_action( 'user_register', $user_id );
			$success = 'You\'re successfully registered';
		}
		
	}
	
} ?>

		<div class="row content">
			<div class="grid-10 offset-grid-1 column">
				
				<!--display error/success message-->
				<div id="message">
					<?php 
						if ( ! empty( $error ) ) :
							echo '<p class="error">'.$error.'</p>';
						endif;
					?>
					
					<?php 
						if ( ! empty( $success ) ) :
							echo '<p class="error">'.$success.'</p>';
						endif;
					?>
				</div>
				
				<div id="result"></div> <!-- To hold validation results -->
				
				<form method="post" id="wp_signup_form" enctype="multipart/form-data">
						
						<h3 class="title-gray"><?php _e( 'Join Community', 'indecon' ); ?></h3>
						<hr>
						<h4 class="title-gray"><?php _e( 'Tipe Keanggotaan', 'indecon' ); ?></h4>
						<div class="row">
							<div class="grid-6 column">
								<?php $type = ( ! empty( $type ) ) ? get_post_meta( get_the_ID(), 'member_type', false ) : ''; ?>

								<label><?php _e( 'Pilih tipe keanggotaan :', 'indecon' ); ?> <span class="text-error">*</span></label>
								<input type="radio" name="member_type" value="individu" <?php checked( $type, 'individu' ); ?>>
								<?php _e( 'Profesional/Individu', 'indecon' ); ?>
								<input id="oraganisasi" type="radio" name="member_type" value="organisasi" <?php checked( $type, 'organisasi' ); ?>>
								<?php _e( 'Organisasi/Lembaga', 'indecon' ); ?>
							</div>
						</div>

						<hr>
						<h4 class="title-gray"><?php _e( 'Personal Information', 'indecon' ); ?></h4>
						<div class="row">
							<div class="grid-6 column">
								<label><?php _e( 'Nama Lengkap :', 'indecon' ); ?> <span class="text-error"> *</span></label>
								<input name="fullname" type="text" class="input-block" required value="<?php echo $fullname; ?>">
							</div>
							<div class="grid-6 column">
								<label><?php _e( 'Profesi :', 'indecon' ); ?><span class="text-error"> *</span></label>
								<input name="profession" type="text" class="input-block" required value="<?php echo $profession; ?>">
							</div>	
							<div class="grid-6 column">
								<label><?php _e( 'Bidang Gerak :', 'indecon' ); ?><span class="text-error"> *</span></label>
								<input name="division" type="text" class="input-block" required value="<?php echo $division; ?>">
							</div>	
							<div class="grid-6 column">
								<label><?php _e( 'Organisasi Tempat Bekerja :', 'indecon' ); ?><span class="text-error"> *</span></label>
								<input name="organization" type="text" class="input-block" required value="<?php echo $organization; ?>">
							</div>	
							<div class="grid-6 column">
								<label><?php _e( 'Alamat :', 'indecon' ); ?><span class="text-error"> *</span></label>
								<textarea name="address" class="input-block" required rows="4"><?php echo $address; ?></textarea>
							</div>	
							<div class="grid-6 column">
								<label><?php _e( 'No tlp/fax/Handphone :', 'indecon' ); ?><span class="text-error"> *</span></label>
								<input name="phone_number" type="text" class="input-block" required value="<?php echo $phone_number; ?>">
							</div>	
							<div class="grid-6 column">
								<label><?php _e( 'Website/blog (jika ada) :', 'indecon' ); ?><span class="text-error"> *</span></label>
								<input name="website" type="text" class="input-block" required value="<?php echo $website; ?>">
							</div>
							<div class="grid-6 column">
								<label><?php _e( 'Motivasi bergabung:', 'indecon' ); ?><span class="text-error"> *</span></label>
								<textarea name="motivation" class="input-block" required rows="4"><?php echo $motivasi; ?></textarea>
							</div>
							<div class="grid-6 column">
								<label><?php _e( 'Deskripsi singkat tentang anda :', 'indecon' ); ?><span class="text-error"> *</span></label>
								<textarea name="description" class="input-block" required rows="4"><?php echo $description; ?></textarea>
							</div>
						</div>
						
						<div class="penanggung-jawab">
							<hr>
							<h4 class="title-gray"><?php _e( 'Detil kontak penanggung jawab', 'indecon' ); ?><span class="text-error">*</span></h4>
							<div class="row">
								<div class="grid-6 column">
									<label><?php _e( 'Nama Kontak ', 'indecon' ); ?><span class="text-error"> *</span>:</label>
									<input name ="pj_contact" type="text" class="input-block" required value="<?php echo $contact_of_pj; ?>">
									<label><?php _e( 'Jabatan di Lembaga ', 'indecon' ); ?><span class="text-error"> *</span>:</label>
									<input name="pj_position" type="text" class="input-block" required value="<?php echo $position_of_pj; ?>">
								</div>	
								<div class="grid-6 column">
									<label><?php _e( 'Alamat Pribadi ', 'indecon' ); ?><span class="text-error"> *</span>:</label>
									<input name="personal_address" type="text" class="input-block" required value="<?php echo $personal_address; ?>">
									<label><?php _e( 'No tlp/fax/Handpone :', 'indecon' ); ?><span class="text-error"> *</span></label>
									<input name="pj_phone" type="text" class="input-block" required value="<?php echo $phone_of_pj; ?>">
								</div>
								<div class="grid-6 column">
									<label><?php _e( 'Email :', 'indecon' ); ?><span class="text-error"> *</span></label>
									<input name="pj_email" type="text" class="input-block" required value="<?php echo $email_of_pj; ?>">
								</div>
							</div>
						</div>

						<hr>
						<h4 class="title-gray"><?php _e( 'Keanggotaan', 'indecon' ); ?></h4>
						<div class="row">
							<div class="grid-6 column">
								<label><?php _e( 'Email address :', 'indecon' ); ?><span class="text-error"> *</span></label>
								<input name ="email" type="text" class="input-block" required value="<?php echo $email; ?>">
								<label><?php _e( 'Username :', 'indecon' ); ?><span class="text-error"> *</span></label>
								<input name="username" type="text" class="input-block" required value="<?php echo $username; ?>">
							</div>
							<div class="grid-6 column">
								<label><?php _e( 'Create password :', 'indecon' ); ?><span class="text-error"> *</span></label>
								<input name="password" type="password" required class="input-block">
								<label><?php _e( 'Confirm password :', 'indecon' ); ?> <span class="text-error"> *</span></label>
								<input name="confirm-password" type="password" required class="input-block">
							</div>
						</div>

						<div class="row">
							<div class="grid-4 column">
								<fieldset class="images">
									<label for="images"><?php _e( 'Gambar Profil', 'indecon' ) ?></label>
									<input type="file" name="thumbnail" size="50">
								</fieldset>
							</div>
						</div>


						<hr>
						<button id="submitbtn" name="adduser" class="button button-primary"><?php _e( 'Create Account', 'indecon' ) ?></button>
						<input type="hidden" name="task" value="register" />
				</form>

				</div>	

			</div>
			
<?php endif; ?>			
<?php get_footer();  ?>
	

