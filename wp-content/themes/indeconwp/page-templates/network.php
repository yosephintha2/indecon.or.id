<?php 
/*
Template Name: Network
*/
get_header(); ?>
	
	<div class="row content">
		<div class="grid-8 column post-content">
			<h3 class="title-gray"><?php _e( 'Networks', 'indecon' ); ?></h3>
			
			<button class="button button-primary filter" data-filter="all"><?php _e( 'All', 'indecon' ); ?></button>
			<button class="button button-primary filter" data-filter=".individu"><?php _e( 'Individu', 'indecon' ); ?></button>
			<button class="button button-primary filter" data-filter=".organisasi"><?php _e( 'Organisasi', 'indecon' ); ?></button>

			<?php
				$temp 		= $wp_query; 
				$wp_query 	= null; 
				$wp_query 	= new WP_Query(); 
				$wp_query->query( array( 'post_type' => 'network', 'posts_per_page' => 6, 'orderby' => 'title', 'order' => 'ASC', 'paged' => $paged ) ); 
			?>

			 <?php if ( $wp_query->have_posts() ) : ?> 
				
				<div class="row" id="filter-wrapper">

				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					<?php 
						$get_member_type = get_post_meta( get_the_ID(), 'member-type', true );
						if ( 'individu' == $get_member_type ) {
							$member_type = 'individu';
						} else if ( 'organisasi' == $get_member_type ) {
							$member_type = 'organisasi';
						} else {
							$member_type = 'none';
						}
					?>
			 
					<div class="grid-6 column mix <?php echo $member_type; ?>">
						<div class="thumb-network">
							<h5 class="title">
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
								<span class="small"><?php echo __( 'Since ', 'indecon' ) . date( __( 'Y', 'indecon' ) ); ?></span>
							</h5>

							<a href="<?php the_permalink(); ?>">
								<?php if ( has_post_thumbnail() ) : ?>
									<?php the_post_thumbnail( 'network-thumbnail' ); ?>
								<?php endif; ?>
							</a>
							
							<p class="address text-center">
								<?php 
									$address = get_post_meta( get_the_ID(), 'full_address', false );

									if ( ! empty( $address ) ) {
										echo $address[0];
									} ?>
								<br>
								<a href="<?php the_permalink(); ?>" class="button button-small button-primary"><?php _e( 'View', 'indecon' ); ?></a>
							</p>
						</div>
					</div>

				<?php endwhile; ?>
					
				</div>	
					
				<?php get_template_part( 'pagination' ); ?>

			<?php endif; ?>
			
			<?php 
			  $wp_query = null; 
			  $wp_query = $temp;  // Reset
			?>
		</div>
		
		<!-- sidebar -->
		<?php get_sidebar( 'primary' ); ?>
</div>
<?php get_footer(); ?>