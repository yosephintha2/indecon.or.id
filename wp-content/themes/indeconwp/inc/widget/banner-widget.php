<?php

// we can only use this Widget if the plugin is active
if( class_exists( 'WidgetImageField' ) )
   add_action( 'widgets_init', create_function( '', "register_widget( 'Indecon_Widget_Image' );" ) );


class Indecon_Widget_Image extends WP_Widget
{
   var $image_field = 'image';      // Defines the field name for your image field

   function __construct()
   {
	   $widget_ops = array(
			   'classname'     => 'iti_image',
			   'description'   => __( "Banner Image", 'indecon' )
		   );
	   parent::__construct( 'banner_image', __('Banner Image', 'indecon' ), $widget_ops );
   }

   function widget( $args, $instance )
   {
	   extract($args);

		$title 		= apply_filters('widget_title', $instance['title'] );
		$link 		= $instance['link'];
		$image_id   = $instance[$this->image_field];
		$image      = new WidgetImageField( $this, $image_id );

	   echo $before_widget;

		   // here you can customize the output of the field
		   ?>
			<a href="<?php echo esc_url( $link ); ?>">
			   <img src="<?php echo $image->get_image_src( 'banner-widget' ); ?>" width="<?php echo $image->get_image_width( 'banner-widget' ); ?>" height="<?php echo $image->get_image_height( 'banner-widget' ); ?>" alt="<?php echo esc_attr( $title ); ?>"/>
			</a>
		   <?php

	   echo $after_widget;
   }

   function form( $instance ) {

		$image_id   = esc_attr( isset( $instance[$this->image_field] ) ? $instance[$this->image_field] : 0 );
		$image      = new WidgetImageField( $this, $image_id );
		$title 		= $instance['title'];
		$link 		= $instance['link'];
   ?>
		<p>
			<label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:', 'indecon' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_name( 'link' ); ?>"><?php _e( 'Link:', 'indecon' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" type="text" value="<?php echo esc_attr( $link ); ?>" />
		</p>

	<?php 
	   // output the field
	   echo $image->get_widget_field( $this->image_field );  // parameter is optional, default is $this->image_field
   }

   function update( $new_instance, $old_instance ) {

		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['link'] = strip_tags( $new_instance['link'] );
		$instance[$this->image_field] = intval( strip_tags( $new_instance[$this->image_field] ) );

	   return $instance;
   }
}