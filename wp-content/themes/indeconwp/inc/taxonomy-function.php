<?php
/**
  * Register custom taxonomy
  *
  * @return void
  * @author matamerah
  **/
add_action( 'init', 'matamerah_register_custom_taxonomy' );
function matamerah_register_custom_taxonomy() {

	register_extended_taxonomy( 'tahun', 'project', array(), 
		array( 
			'singular' 	=> 'Project Year',
			'plural' 	=> 'Projects Year',
			'slug'		=> 'tahun', 
			)
	);

	register_extended_taxonomy( 'tahun_research', 'research', array(), 
		array( 
			'singular' 	=> 'Research Year',
			'plural' 	=> 'Research Year',
			'slug'		=> 'tahun_research', 
			)
	);

	register_extended_taxonomy( 'tahun_training', 'training', array(), 
		array( 
			'singular' 	=> 'Training Year',
			'plural' 	=> 'Training Year',
			'slug'		=> 'tahun_training', 
			)
	);

	
 }

// add_filter( 'kecomang_taxonomy_images', 'matamerah_add_image_support_to_taxonomy' );
// function matamerah_add_image_support_to_taxonomy( $defined_taxo ) {
//   $defined_taxo = array( 'product_cat' );
  
//   return $defined_taxo;
// }