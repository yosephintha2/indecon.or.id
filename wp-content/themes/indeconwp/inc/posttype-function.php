<?php 

/**
 * Register custtom post type
 *
 * @return void
 * @author matamerah
 **/
add_action( 'after_setup_theme', 'matamerah_register_custom_posttype', 5 );	
function matamerah_register_custom_posttype() {
	
	register_extended_post_type( 'activity', 
		array(
			# Add some custom columns to the admin screen:
			'admin_cols' => array(
				'title' => array(
						'title' => 'Activity Name'
					),
				'featured_image' => array(
					'title'          	=> 'Activity Image',
					'featured_image' 	=> 'thumbnail',
					'height' 			=> 50,
					'width' 			=> 50
				
				),
				'date'
			),
			'supports'      => array( 'title', 'editor', 'thumbnail', 'author', 'comments' ),
			'menu_icon' 	=> 'dashicons-format-status',
			'menu_position' => 28,
		), 
		array(
			# Override the base names used for labels:
			'singular' => 'Activity',
			'plural'   => 'Activities',
			'slug'     => 'activity',
		)
	);

	register_extended_post_type( 'publication', 
		array(
			# Add some custom columns to the admin screen:
			'admin_cols' => array(
				'title' => array(
						'title' => 'Publication Name'
					),
				'featured_image' => array(
					'title'          	=> 'Publication Image',
					'featured_image' 	=> 'thumbnail',
					'height' 			=> 50,
					'width' 			=> 50
				
				),
				'date'
			),

			'supports'      => array( 'title', 'editor', 'thumbnail' ),
			'menu_icon' 	=> 'dashicons-admin-site',
			'menu_position' => 28,

		), 
		array(
			# Override the base names used for labels:
			'singular' => 'Publication',
			'plural'   => 'Publications',
			'slug'     => 'publication',
		)
	);

	register_extended_post_type( 'project', 
		array(
			# Add some custom columns to the admin screen:
			'admin_cols' => array(
				'title' => array(
						'title' => 'Project Name'
					),
				'featured_image' => array(
					'title'          	=> 'Project Image',
					'featured_image' 	=> 'thumbnail',
					'height' 			=> 50,
					'width' 			=> 50
				
				),
				'tahun' => array(
						'taxonomy' => 'tahun'
					),
				'date'
				
			),

			'supports' 		=> array( 'title', 'editor', 'thumbnail' ),
			'menu_icon' 	=> 'dashicons-feedback',
			'menu_position' => 28,

		), 
		array(
			# Override the base names used for labels:
			'singular' => 'Project',
			'plural'   => 'Projects',
			'slug'     => 'project',
		)
	);

	register_extended_post_type( 'research', 
		array(
			# Add some custom columns to the admin screen:
			'admin_cols' => array(
				'title' => array(
						'title' => 'Research Title'
					),
				'featured_image' => array(
					'title'          	=> 'Research Image',
					'featured_image' 	=> 'thumbnail',
					'height' 			=> 50,
					'width' 			=> 50
				
				),
				'tahun_research' => array(
						'taxonomy' => 'tahun_research'
					),
				'date'
				
			),

			'supports' 		=> array( 'title', 'editor', 'thumbnail' ),
			'menu_icon' 	=> 'dashicons-lightbulb',
			'menu_position' => 29,
                        'has_archive'   => false,


		), 
		array(
			# Override the base names used for labels:
			'singular' => 'Research',
			'plural'   => 'Research',
			'slug'     => 'research',
		)
	);

	register_extended_post_type( 'training', 
		array(
			# Add some custom columns to the admin screen:
			'admin_cols' => array(
				'title' => array(
						'title' => 'Training Title'
					),
				'featured_image' => array(
					'title'          	=> 'Training Image',
					'featured_image' 	=> 'thumbnail',
					'height' 			=> 50,
					'width' 			=> 50
				
				),
				'tahun_training' => array(
						'taxonomy' => 'tahun_training'
					),
				'date'
				
			),

			'supports' 		=> array( 'title', 'editor', 'thumbnail' ),
			'menu_icon' 	=> 'dashicons-list-view',
			'menu_position' => 30,

		), 
		array(
			# Override the base names used for labels:
			'singular' => 'Training',
			'plural'   => 'Training',
			'slug'     => 'training',
		)
	);

	register_extended_post_type( 'footprint', 
		array(

			'supports' 		=> array( 'title', 'editor' ),
			'menu_icon' 	=> 'dashicons-share',
			'menu_position' => 31,

		), 
		array(
			# Override the base names used for labels:
			'singular' => 'Footprint',
			'plural'   => 'Footprint',
			'slug'     => 'footprint',
		)
	);

	register_extended_post_type( 'learning', 
		array(

			'admin_cols' => array(
				'title' => array(
						'title' => 'Learning Title'
					),
				'featured_image' => array(
					'title'          	=> 'Learning Image',
					'featured_image' 	=> 'thumbnail',
					'height' 			=> 50,
					'width' 			=> 50
				
				),
				'learning_type' => array(
						'title'       => 'Learning Type',
						'meta_key'    => 'learning_type',
					),
				'date'
				
			),

			'supports' 		=> array( 'title', 'editor', 'thumbnail' ),
			'menu_icon' 	=> 'dashicons-welcome-learn-more',
			'menu_position' => 32,

		), 
		array(
			# Override the base names used for labels:
			'singular' => 'Learning',
			'plural'   => 'Learning',
			'slug'     => 'learning',
		)
	);

	register_extended_post_type( 'team', 
		array(

			'admin_cols' => array(
				'title' => array(
						'title' => 'Team Name'
					),
				'featured_image' => array(
					'title'          	=> 'Team Picture',
					'featured_image' 	=> 'thumbnail',
					'height' 			=> 50,
					'width' 			=> 50
				
				),
				'date'
				
			),

			'supports' 		=> array( 'title', 'thumbnail' ),
			'menu_icon' 	=> 'dashicons-groups',
			'menu_position' => 33,

		), 
		array(
			# Override the base names used for labels:
			'singular' => 'Team',
			'plural'   => 'Team',
			'slug'     => 'team',
		)
	);

	register_extended_post_type( 'partner', 
		array(

			'supports' 		=> array( 'title', 'excerpt', 'thumbnail' ),
			'menu_icon' 	=> 'dashicons-admin-links',
			'menu_position' => 34,

		), 
		array(
			# Override the base names used for labels:
			'singular' => 'Partner',
			'plural'   => 'Partner',
			'slug'     => 'partner',
		)
	);

	register_extended_post_type( 'slider', 
		array(

			'admin_cols' => array(
				'title' => array(
						'title' => 'Slider Title'
					),
				'featured_image' => array(
					'title'          	=> 'Slider Image',
					'featured_image' 	=> 'thumbnail',
					'height' 			=> 50,
					'width' 			=> 50
				
				),
				'date'
				
			),

			'supports' 		=> array( 'title', 'excerpt', 'thumbnail' ),
			'menu_icon' 	=> 'dashicons-slides',
			'menu_position' => 35,

		), 
		array(
			# Override the base names used for labels:
			'singular' => 'Slider',
			'plural'   => 'Slider',
			'slug'     => 'slider',
		)
	);

	register_extended_post_type( 'network', 
		array(

			'supports' 		=> array( 'title', 'thumbnail', 'author' ),
			'menu_icon' 	=> 'dashicons-networking',
			'menu_position' => 36,

		), 
		array(
			# Override the base names used for labels:
			'singular' => 'Network',
			'plural'   => 'Network',
			'slug'     => 'network',
		)
	);

	add_post_type_support( 'activity', 'thumbnail' );

	
}	

