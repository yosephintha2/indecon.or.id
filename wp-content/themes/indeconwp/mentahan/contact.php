		<?php include "header.php" ?>
		<?php include "headnav.php" ?>
		
		<div class="row content">
				<div class="grid-8 column post-content">
					<h3 class="title-gray">Contact</h3>
					<h5 class="title-gray">Our Team</h5>
					<div class="row">
						<div class="grid-3-small thumb-team">
							<img src="assets/img/example/img-team.jpg" alt="Thumbnail">
						</div>
						<div class="grid-3-small thumb-team">
							<img src="assets/img/example/img-team.jpg" alt="Thumbnail">
						</div>
						<div class="grid-3-small thumb-team">
							<img src="assets/img/example/img-team.jpg" alt="Thumbnail">
						</div>
						<div class="grid-3-small thumb-team">
							<img src="assets/img/example/img-team.jpg" alt="Thumbnail">
						</div>
						<div class="grid-3-small thumb-team">
							<img src="assets/img/example/img-team.jpg" alt="Thumbnail">
						</div>
						<div class="grid-3-small thumb-team">
							<img src="assets/img/example/img-team.jpg" alt="Thumbnail">
						</div>
						<div class="grid-3-small thumb-team">
							<img src="assets/img/example/img-team.jpg" alt="Thumbnail">
						</div>
						<div class="grid-3-small thumb-team">
							<img src="assets/img/example/img-team.jpg" alt="Thumbnail">
						</div>
					</div>
					<hr>
					<h5 class="title-gray">Send us an email</h5>
					<div class="row">
						<div class="grid-6 column">
							<label>Full name <span class="text-error">*</span>:</label>
							<input type="text" class="input-block">
						</div>
						<div class="grid-6 column">
							<label>Email address <span class="text-error">*</span>:</label>
							<input type="text" class="input-block">
						</div>
						<div class="grid-12 column">
							<label>Subject <span class="text-error">*</span>:</label>
							<input type="text" class="input-block">
						</div>
						<div class="grid-12 column">
							<label>Messages <span class="text-error">*</span>:</label>
							<textarea class="input-block" rows="6"></textarea>
						</div>
					</div>
					<button class="button button-primary">Create Account</button>
				</div>
				
				<!-- sidebar -->
				<div class="grid-4 column sidebar">
						<div class="distance">
							<div class="box section-pastle text-center">
								<h4>JOIN OUR COMMUNITY</h4>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
								<a class="button button-block button-primary">join now</a>
							</div>
						</div>
						<div class="distance text-center">
							<p>- OR -<br>You can get news for the latest update </p>
							<input style="margin-bottom: 0" type="email" placeholder="Email address ..." class="input-block">
							<button type="submit" class="button button-small button-green">Subscribe</button>
						</div>
						<div class="distance">						
							<h4 class="title-post title-gray">Archives</h4>
							<ul class="unstyled list-post">
								<li><a href="#">September 2013 <span class="muted">(10)</span></a></li>
								<li><a href="#">October 2013 <span class="muted">(6)</span></a></li>
								<li><a href="#">November 2013 <span class="muted">(8)</span></a></li>
								<li><a href="#">December 2013 <span class="muted">(5)</span></a></li>
							</ul>
						</div>
						<div class="distance">
							<a href="#"><img style="margin-top:10px;" src="assets/img/example/banner-1.jpg" alt="Learning Centre"></a>
						</div>
				</div><!-- /sidebar -->
		</div>
		
		
        
		<?php include "footer.php" ?>
		<script>
			    $('#myTab a').click(function (e) {
					e.preventDefault()
					$(this).tab('show')
  				})
		</script>
		
</body>
</html>