<?php

/**
 * The Template for displaying 404 page 
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header(); ?>

	<div class="row content">

		<div class="grid-8 column post-content">
		
			<article id="post-0" <?php post_class(); ?>>
		
				<div class="entry-container cl">

					<div class="post-content">

						<header class="entry-header">
							<h1 class="entry-title"><?php _e( '404 Not Found', 'indecon' ); ?></h1>
						</header>

						<div class="entry-content">
							<p><?php _e( 'Try another page', 'indecon' ); ?></p>
						</div>

					</div>
					
				</div>

			</article>

		</div>

		<?php get_sidebar( 'primary' ); ?>

	</div>

<?php get_footer(); ?>