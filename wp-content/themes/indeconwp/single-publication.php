<?php

/**
 * The Template for displaying single post
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header(); ?>
	
	<div class="row content">

		<div class="grid-8 column post-content">
	
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php 
					$publication_categories = get_post_meta( get_the_ID(), '_publication_categories', true );
							
						if ( 'videos' == $publication_categories ) { ?>

							<div class="grid-6 column">
								<h5 class="title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
								</h5>

								<?php 
									$youtube_video_url = get_post_meta( get_the_ID(), '_youtube_video_url', true );
									$video_url = esc_url( $youtube_video_url );
									echo do_shortcode( "[video width='637' height='358' src='{$video_url}']" );
								?>

								<?php the_content(); ?>
							</div>

						<?php } else if ( 'photos' == $publication_categories ) { ?>
							
							<div class="photo_wrapper">
								<h5 class="title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
								</h5>
								
								<ul class="publication_gallery_wrapper">
								<?php if ( has_post_thumbnail() ) : ?>
									<li class="column-2">
										<?php 
										$post_small = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'blog-small', false );
										$post_large = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full', false ); ?>
										<a class="my_fancybox" rel="gallery1" href="<?php echo $post_large[0]; ?>">
											<img src="<?php echo $post_small[0]; ?>" alt="Gallery">
										</a>
									</li>
								<?php endif; ?>
									<?php 
										$images = get_post_meta( get_the_ID(), 'vdw_gallery_id', true );
										foreach ( $images as $image ) {
											$small_image = wp_get_attachment_image_src( $image, 'blog-small', false );
											$large_image = wp_get_attachment_image_src( $image, 'full', false ); ?>
											<li class="column-2">
												<a class="my_fancybox" rel="gallery1" href="<?php echo $large_image[0]; ?>">
													<img src="<?php echo $small_image[0]; ?>" alt="Gallery">
												</a>
											</li>
										<?php }
									 ?>
								</ul>
								
								<?php the_content(); ?>
							</div>
							
						<?php } else { ?>

							<div class="grid-6 column">
								<h5 class="title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
								</h5>

								<?php the_content(); ?>
							</div>
							
						<?php }

						 ?>

				</article>
				
			<?php endwhile; ?>
			<?php endif; ?>
		
		</div>
		
		<?php get_sidebar( 'primary' ); ?>

	</div>

<?php get_footer(); ?>