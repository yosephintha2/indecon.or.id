<?php
/**
 * Spx Core Functions
 *
 * @package    SPXframework
 * @subpackage Functions
 * @version    1.0
 * @since      1.0
 * @author     Alispx
 */

/**
 * Define Constant
 *
 * @return void
 * @author alispx
 **/
define( 'THEME_PATH', get_template_directory_uri() );
define( 'THEME_SCRIPT', get_template_directory_uri(). '/assets/js/' );
define( 'THEME_STYLE', get_template_directory_uri(). '/assets/css/' );
define( 'THEME_FONT', get_template_directory_uri(). '/assets/css/' );

/* Load the theme option framework. */
require_once( trailingslashit( get_template_directory() ) . 'framework/options/bootstrap.php' );
require_once( trailingslashit( get_template_directory() ) . 'inc/setup.php' );


/**
 * Registers sidebars.
 * 
 * @since 1.0
 */
add_action( 'widgets_init', 'indecon_register_extra_sidebars', 15 );
function indecon_register_extra_sidebars() {
	
	register_sidebar(
		array(
			'id' 			=> 'primary',
			'name' 			=> __( 'Primary', 'indecon' ),
			'description' 	=> __( 'The widget area loaded on the blog page.', 'indecon' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s widget-%2$s distance">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="title-post title-gray">',
			'after_title'   => '</h4>'
		)
	);

}

function posts_for_current_author( $query ) {
	global $user_level;

	if ( $query->is_admin && $user_level < 5 ) {
		global $user_ID;
		$query->set( 'author',  $user_ID );
		unset( $user_ID );
	}
	unset( $user_level );

	return $query;
}
add_filter( 'pre_get_posts', 'posts_for_current_author' );