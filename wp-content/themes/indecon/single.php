<?php

/**
 * The Template for displaying single post
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header(); ?>
	
	<div class="row content">

		<div class="grid-8 column post-content">
	
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<h3 class="title-post"><?php the_title() ?></h3>
					<p class="date">
						<?php indecon_posted_on(); ?>
					</p>

					<?php the_content(); ?>

				</article>
				
			<?php endwhile; ?>
			<?php endif; ?>
		
		</div>
		
		<?php get_sidebar( 'primary' ); ?>

	</div>

<?php get_footer(); ?>