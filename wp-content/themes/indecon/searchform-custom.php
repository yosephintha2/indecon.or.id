<?php

/**
 * The Template for displaying dropdown search
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="dropdown search">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-search"></span></a>
	<form class="dropdown-menu pull-right" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input name="s" class="clean" type="search" placeholder="<?php esc_attr_e( 'Search &hellip;', 'indecon' ); ?>">
	</form>
</div>