<?php
/**
 * The Template for displaying research section of homepage
 *
 * @author 		alispx
 * @version     1.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
?>

<div class="section section-pastle">
    <div class="row">
        <div class="grid-4 column">
            <?php
            global $box_class;
            $box_class = 'box-green';
            ?>
            <?php get_template_part('joint', 'box'); ?>
        </div>
        <div class="grid-8 column">
            <a class="control_next">Next ></a>
            <a class="control_prev">&larr; Prev</a>
            <div id="slider">
                <div class="wrap-slider" style="overflow: hidden; position: relative; float:left;">

                    <?php
                    $latest_args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 5,
                        'orderby' => 'date',
                        'order' => 'DESC',
                    );

                    $latest = new WP_Query($latest_args);
                    ?>
                    <?php if ($latest->have_posts()) : ?>

                        <ul>

    <?php while ($latest->have_posts()) : $latest->the_post(); ?>

                                <li>
                                    <h3 class="title-gray" style="margin: 0;"><?php the_category(); ?></h3>
                                    <div class="thumb-slide">
                                        <a href="<?php the_permalink(); ?>">
        <?php if (has_post_thumbnail()) : ?>
                                                <?php the_post_thumbnail('blog-tiny'); ?>
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                    <div class="caption">
                                        <h4><?php the_title(); ?></h4>
                                        <p>
        <?php echo wp_trim_words($post->post_content, 20); ?>
                                            <a class="read-more" href="<?php the_permalink(); ?>"><?php _e('Read More', 'indecon'); ?></a>
                                        </p>
                                    </div>
                                </li>

    <?php endwhile; ?>
                        <?php else: ?>
                            <p><?php _e('No Post Available', 'indecon'); ?></p>
                        </ul>

<?php endif;
wp_reset_postdata(); ?>	

                </div>  
            </div>
        </div>
    </div>
</div>