<?php

/**
 * The Template for comments
 *
 * @author 		tokokoo
 * @version     2.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! function_exists( 'indecon_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function indecon_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	if ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<div class="comment-body">
			<?php _e( 'Pingback:', 'tokokoo' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( 'Edit', 'tokokoo' ), '<span class="edit-link">', '</span>' ); ?>
		</div>

	<?php else : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?>>
	
		<div class="comment-wrap">
		
			<?php if ( 0 != $args['avatar_size'] ) { echo get_avatar( $comment, $args['avatar_size'] ); } ?>
				
			<div class="comment-meta">
				<span class="comment-author vcard">
					<?php printf( '<cite class="fn">%s</cite>', get_comment_author_link() ); ?>
				</span>
				<span class="published">
					<time datetime="<?php comment_time( 'c' ); ?>">
						<?php printf( _x( '%1$s at %2$s', '1: date, 2: time', 'tokokoo' ), get_comment_date(), get_comment_time() ); ?>
					</time>
				</span>// 
				<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"><?php _e( 'Permalink', 'tokokoo' ); ?></a>
				<?php edit_comment_link( '<span class="edit">' . __( 'Edit', 'tokokoo' ) . '</span>', ' // ' ); ?>
			</div>

			<div class="comment-content comment-text comment-body">
				<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'tokokoo' ); ?></p>
				<?php endif; ?>
				
				<?php comment_text(); ?>
			</div><!-- .comment-content -->

			<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'tokokoo' ), 'after' => ' <span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>

		</div>

	<?php
	endif;
}
endif; // ends check for aio_comment()

/* If a post password is required or no comments are given and comments/pings are closed, return. */
if ( post_password_required() || ( !have_comments() && !comments_open() && !pings_open() ) 
	/* if on WooCommerce pages, return. */
	|| ( class_exists( 'woocommerce' ) && ( indecon_is_woocommerce_pages() ) ) )
	return;
?>

	<section id="comments">

		<?php if ( have_comments() ) : ?>

			<h2 id="comments-number" class="comments-header comment-title"><?php comments_number( __( 'No Responses', 'tokokoo' ), __( 'One Response', 'tokokoo' ), __( '% Responses', 'tokokoo' ) ); ?></h3>

			<?php if ( get_option( 'page_comments' ) ) : ?>
				<div class="comments-nav">
					<span class="page-numbers"><?php printf( __( 'Page %1$s of %2$s', 'tokokoo' ), ( get_query_var( 'cpage' ) ? absint( get_query_var( 'cpage' ) ) : 1 ), get_comment_pages_count() ); ?></span>
					<?php previous_comments_link(); ?>
					<?php next_comments_link(); ?>
				</div><!-- .comments-nav -->
			<?php endif; ?>

			<?php do_action( 'indecon_comment_list_before' ); ?>

				<ol class="comment-list">
					<?php wp_list_comments( array( 'callback' => 'indecon_comment', 'avatar_size' => 80 ) ); ?>
				</ol><!-- .comment-list -->

			<?php do_action( 'indecon_comment_list_after' ); ?>

		<?php endif; ?>

		<?php if ( pings_open() && !comments_open() ) : ?>

			<p class="comments-closed pings-open">
				<?php printf( __( 'Comments are closed, but <a href="%s" title="Trackback URL for this post">trackbacks</a> and pingbacks are open.', 'tokokoo' ), esc_url( get_trackback_url() ) ); ?>
			</p><!-- .comments-closed .pings-open -->

		<?php elseif ( !comments_open() && ( indecon_option( 'indecon_comment_form' ) == 0 ) ) : ?>

			<p class="comments-closed">
				<?php _e( 'Comments are closed.', 'tokokoo' ); ?>
			</p><!-- .comments-closed -->

		<?php endif; ?>

	</section><!-- #comments -->

	<?php
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	$req_class = ( $req ? ' req' : '');

	$fields =  array(
		'author' => '<div class="col-md-6"><p class="form-author' . $req_class . '"><label for="author">' . __( 'Name', 'tokokoo' ) . ( $req ? '<span class="required">*</span>' : '' ) . '</label> ' . '<input id="author" class="text-input" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="40"' . $aria_req . ' placeholder="' . __( 'Name', 'tokokoo' ) . '" /></p>',
		'email' => '<p class="form-email' . $req_class . '"><label for="email">' . __( 'Email', 'tokokoo' ) . ( $req ? '<span class="required">*</span>' : '' ) . '</label> ' . '<input id="email" class="text-input" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="40"' . $aria_req . 'placeholder="' . __( 'Email', 'tokokoo' ) . '" /></p>',
		'url' => '<p class="form-url"><label for="url">' . __( 'Website', 'tokokoo' ) . '</label><input id="url" class="text-input" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="40" placeholder="' . __( 'Website', 'tokokoo' ) . '" /></p></div>'
	);

	$wrapper_start = ( is_user_logged_in() ) ? '<div class="col-md-12">' : '<div class="col-md-6">';
	$args = array(
		'label_submit' => __( 'Post Comment', 'tokokoo' ),
		// close $wrapper_start in indecon_closing_comment_form() | inc/theme-layouts.php
		'comment_field' => $wrapper_start . '<p class="form-textarea req"><label for="comment">' . _x( 'Comment', 'noun', 'tokokoo' ) . '</label><textarea class="input-text" id="comment" name="comment" cols="60" rows="8" aria-required="true" placeholder="' . _x( 'Comments', 'noun', 'tokokoo' ) . '"></textarea></p>',
		'fields' => apply_filters( 'indecon_comment_form_default_fields', $fields ),
		'comment_notes_before' => '',
		'comment_notes_after' => ''
	);

	comment_form( $args ); // Loads the comment form. 