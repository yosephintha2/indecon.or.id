<?php

/**
 * The Template for displaying menu primary
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<nav class="navbar">
	
	<button class="navtop-toggle button button-green pull-left" data-target=".navtop-collapse" data-toggle="collapse" type="button">
		<span class="icon-menu"></span>
		<?php _e( 'Menu', 'indecon' ); ?>
	</button>
	
	<div class="navtop-collapse collapse">

		<?php if ( has_nav_menu( 'primary' ) ) {

			wp_nav_menu(
				array(
					'theme_location'  => 'primary',
					'menu_class'      => 'navtop',
					'walker'            => new wp_bootstrap_navwalker()
				)
			);
		
		}  else { ?>

			<div class="navtop-collapse collapse">
				<ul class="navtop">
					<?php wp_list_pages( array( 'depth' => 1,'sort_column' => 'menu_order','title_li' => '', 'include' => 2 ) ); ?>
				</ul>
			</div>

		<?php } ?>
	
	</div>
	
	<!-- Load Searchform Custom -->
	<?php get_template_part( 'searchform', 'custom' ); ?>

</nav>