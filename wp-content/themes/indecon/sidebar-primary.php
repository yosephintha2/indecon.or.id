<?php

/**
 * The Template for displaying sidebar primary
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php if ( is_active_sidebar( 'primary' ) ) { ?>

	<!-- sidebar -->
	<div class="grid-4 column sidebar">
		<div class="distance">
			<div class="box section-pastle text-center">
				<?php 
				global $box_class;
				$box_class = 'box-pastle';
				get_template_part( 'joint', 'box' ); ?>
			</div>
		</div>
		<div class="distance text-center">
			<?php _e( '<p>- OR -<br>You can get news for the latest update </p>', 'indecon' ); ?>
			
			<form action="<?php echo esc_url( indecon_option( 'subscribe_action_link' ) ); ?>">
				<input style="margin-bottom: 0" type="email" placeholder="<?php _e( 'Email address ...', 'indecon' ); ?>" class="input-block">
				<button type="submit" class="button button-small button-green"><?php _e( 'Subscribe', 'indecon' ); ?></button>
			</form>
		</div>

		<?php dynamic_sidebar( 'primary' ); ?>
	
	</div><!-- /sidebar -->

<?php } ?>