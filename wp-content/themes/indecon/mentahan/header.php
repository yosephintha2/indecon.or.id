<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
        <meta charset="utf-8">
        <title>INDECON</title>
        <meta name="description" content="indecon">
        <meta name="author" content="Pildasi Batubara">
        <meta name="keywords" content="Indecon">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?php $path = pathinfo($_SERVER['PHP_SELF']);$urlpath = substr($path['dirname'], 0, strlen($path['dirname']) - strlen(basename($path['dirname'])) - 1 ); $urlpath = empty($urlpath) ? "." : $urlpath;?>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-57-precomposed.png">
		<link rel="shortcut icon" href="favicon.png">
		
		<!-- Stylesheet 
		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>-->
		<link rel="stylesheet" href="<?php echo $urlpath; ?>/assets/css/fonts.css">
        <link rel="stylesheet" href="<?php echo $urlpath; ?>/assets/css/normalize.css">
        <link rel="stylesheet" href="<?php echo $urlpath; ?>/assets/css/stylesheet.css">
        <!-- For addtional custom style you can use addon.css -->
        <link rel="stylesheet" href="<?php echo $urlpath; ?>/assets/css/style.css">
        
        <script src="<?php echo $urlpath; ?>/assets/js/modernizr-2.6.2.min.js"></script>
    </head>