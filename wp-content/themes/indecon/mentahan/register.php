		<?php include "header.php" ?>
		<?php include "headnav.php" ?>
		
		<div class="row content">
				<div class="grid-10 offset-grid-1 column">
					<h3 class="title-gray">Join Community</h3>
					<div class="row">
						<div class="grid-6 column">
							<label>Email address <span class="text-error">*</span>:</label>
							<input type="text" class="input-block">
							<label>Username <span class="text-error">*</span>:</label>
							<input type="text" class="input-block">
						</div>
						<div class="grid-6 column">
							<label>Create password <span class="text-error">*</span>:</label>
							<input type="text" class="input-block">
							<label>Confirm password <span class="text-error">*</span>:</label>
							<input type="text" class="input-block">
						</div>
					</div>
					<hr>
					<h4 class="title-gray">Personal Information</h4>
					<div class="row">
						<div class="grid-6 column">
							<label>Full name <span class="text-error">*</span>:</label>
							<input type="text" class="input-block">
							<label>Organisation your work:</label>
							<input type="text" class="input-block">
						</div>
						<div class="grid-6 column">
							<label>Profession:</label>
							<input type="text" class="input-block">
							<label>Division of:</label>
							<input type="text" class="input-block">
						</div>
						<div class="grid-6 column">
							<label>Address:</label>
							<textarea class="input-block" rows="4"></textarea>
						</div>
						<div class="grid-6 column">
							<label>Phone / Fax:</label>
							<input type="text" class="input-block">
							<label>Website:</label>
							<input type="text" class="input-block">
						</div>
						<div class="grid-6 column">
							<label>Motivasi bergabung:</label>
							<textarea class="input-block" rows="4"></textarea>
						</div>
						<div class="grid-6 column">
							<label>Something about you:</label>
							<textarea class="input-block" rows="4"></textarea>
						</div>
					</div>
					<hr>
					<button class="button button-primary">Create Account</button>
				</div>
		</div>
		
		
        
		<?php include "footer.php" ?>
				
</body>
</html>