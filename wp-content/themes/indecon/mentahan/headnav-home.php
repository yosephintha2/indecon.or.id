    <body class="home">
    <!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
    <header class="header">
	    <div class="row">
		    <div class="grid-12 column">
		    	<div class="head-top">
				    <div class="elang">
				    	<a class="active" href="#"><span class="iflag-eng"></span> English</a>
				    	<a href="#"><span class="iflag-ind"></span> Indonesia</a>
				    </div>
				    <div class="logo">
					    <a href="./"><img src="assets/img/logo-indecon.png" alt="Indecon"></a>
					    <p class="tagline">Conservation and Community Empowerment Through Ecotourism</p>
				    </div>
				    <div class="socmed">
					    <a href="#"><span class="icon-facebook"></span></a>
					    <a href="#"><span class="icon-twitter"></span></a>
					    <a href="#"><span class="icon-email"></span></a>
					    <a href="#"><span class="icon-rss"></span></a>
				    </div>
		    	</div>
		    	<nav class="navbar">
		    		<button class="navtop-toggle button button-green pull-left" data-target=".navtop-collapse" data-toggle="collapse" type="button"><span class="icon-menu"></span> Menu</button>
		    		<div class="navtop-collapse collapse">		    		
			    		<ul class="navtop">
			    			<li class="active" ><a href="./">home</a></li>
			    			<li><a href="project.php">project</a></li>
			    			<li><a href="research.php">research</a></li>
			    			<li class="dropdown">
			    				<a href="#" class="dropdown-toggle" data-toggle="dropdown">learning centre <span class="caret"></span></a>
				        		<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
					        		<li><a href="#">Sub Menu Satu</a></li>
					        		<li><a href="#">Sub Menu Dua</a></li>
					        		<li><a href="#">Sub Menu Tiga</a></li>
					        		<li><a href="#">Sub Menu Empat</a></li>
				        		</ul>
			    			</li>
			    			<li><a href="training.php">training</a></li>
			    			<li class="dropdown">
				    			<a href="#" class="dropdown-toggle" data-toggle="dropdown">member <span class="caret"></span></a>
				        		<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
					        		<li><a href="#">Benefit</a></li>
					        		<li><a href="network.php">Networks</a></li>
					        		<li><a href="register.php">Register</a></li>
				        		</ul>
			    			</li>
			    			<li><a href="login.php">login</a></li>
			    		</ul>
		    		</div>
		    		<div class="dropdown search">
			    		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-search"></span></a>
			    		<form class="dropdown-menu pull-right">
				    		<input class="clean" type="search" placeholder="Search ...">
			    		</form>
		    		</div>
		    	</nav>
		    </div>
	    </div>
    </header>    