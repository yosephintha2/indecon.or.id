<?php

/**
 * Theme setup function.  This function adds support for theme features and defines the default theme
 * actions and filters.
 *
 * @since  1.0
 * @access public
 */


/**
 * Flush rewrite rules.
 * 
 * @since 1.0
 */
add_action( 'after_switch_theme', 'spx_flush_rewrite_rules', 5 );
function spx_flush_rewrite_rules() {
	flush_rewrite_rules();
}   

/**
 * Function unclude file that required by theme
 * 
 * @since 1.0
 */
add_action( 'after_setup_theme', 'spx_theme_setup' );
function spx_theme_setup() {

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	register_nav_menus( array(
		'primary' 	=> __( 'primary', 'indecon' ),
		'secondary' 	=> __( 'secondary', 'indecon' ),
	) );

	// Load Metabox Framework
	require_once( trailingslashit( get_template_directory() ) . 'inc/metabox.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/functions.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/bootstrap-navwalker.php' );

	// Load Widget Framework
	require_once( trailingslashit( get_template_directory() ) . 'framework/widget/widget-image-field.php' );
	
	// Load Extension
	require_once( trailingslashit( get_template_directory() ) . 'framework/extension/extended-cpts.php' );
	require_once( trailingslashit( get_template_directory() ) . 'framework/extension/extended-taxos.php' );


	// Load Custom Post Type
	require_once( trailingslashit( get_template_directory() ) . 'inc/posttype-function.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/taxonomy-function.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/template-tags.php' );
	
	// Load Custom Widget
	require_once( trailingslashit( get_template_directory() ) . 'inc/widget/banner-widget.php' );
	require_once( trailingslashit( get_template_directory() ) . 'inc/widget/archive-widget.php' );

	require_once( trailingslashit( get_template_directory() ) . 'inc/extension/if-menu/if-menu.php' );

	
}

/**
 * Function load style and script
 *
 * @author alispx
 **/
add_action( 'wp_enqueue_scripts', 'spx_load_script', 10 );
function spx_load_script() {
	// Load script
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'foundation', THEME_SCRIPT. 'foundation.js' , '', '1.0', false );
	wp_enqueue_script( 'foundation-forms', THEME_SCRIPT. 'foundation.forms.js' , '', '1.0', false );
	wp_enqueue_script( 'bootstrap', THEME_SCRIPT. 'bootstrap.js' , '', '1.0', false );
	wp_enqueue_script( 'modernizer', THEME_SCRIPT. 'modernizr-2.6.2.min.js' , '', '2.6.2', false );
	wp_enqueue_script( 'validator', THEME_SCRIPT. 'jquery.validate.min.js' , '', '2.6.2', false );
	wp_enqueue_script( 'mixitup', THEME_SCRIPT. 'jquery.mixitup.min.js' , '', '', false );
	wp_enqueue_script( 'spx-script', THEME_SCRIPT. 'scripts.js' , array( 'jquery' ), '1.0', false );

	wp_enqueue_style( 'fancybpx-style', THEME_SCRIPT . '/fancybox/jquery.fancybox.css' );
	wp_enqueue_script( 'fancybox', THEME_SCRIPT. '/fancybox/jquery.fancybox.js' , '', '', false );
	wp_enqueue_script( 'fancybox-method', THEME_SCRIPT. '/fancybox/fancybox-method.js' , '', '', false );
	
	wp_enqueue_style( 'gfont', 'http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' );
	wp_enqueue_style( 'font', THEME_STYLE . 'fonts.css' );
	wp_enqueue_style( 'normalize', THEME_STYLE . 'normalize.css' );
	wp_enqueue_style( 'stylesheet', THEME_STYLE . 'stylesheet.css' );
	wp_enqueue_style( 'custom', THEME_STYLE . 'custom-css.css' );
	

}

/**
 * Function gmaps script
 *
 * @author alispx
 **/
add_action( 'wp_enqueue_scripts', 'spx_load_gmaps_script', 20 );
function spx_load_gmaps_script() {
	// Load script
	if ( is_page_template( 'page-templates/footprint.php' ) ) {
		wp_enqueue_script( 'gmaps-api', 'http://maps.google.com/maps/api/js?sensor=true' , '', '3.0', false );
		wp_enqueue_script( 'gmaps', THEME_SCRIPT. 'gmaps.js' , '', '3.0', false );
	}
}



/* 
 * Funcion To Call Option
 * 
 * @since  1.0
 */
function indecon_option( $name ) {
    return vp_option( "indecon_option." . $name );
}

/** 
 * Funcion Change teheme option setting name by filter
 * 
 * @author alispx
 **/
add_action( 'after_setup_theme', 'indecon_init_option', 25 );
function indecon_init_option() {
	$tmpl_opt  = get_template_directory() . '/inc/option.php';
	$theme_options = new VP_Option(array(
		'is_dev_mode'           => false,                                  // dev mode, default to false
		'option_key'            => 'indecon_option',                           // options key in db, required
		'page_slug'             => 'indecon_option',                           // options page slug, required
		'template'              => $tmpl_opt,                              // template file path or array, required
		'menu_page'             => 'themes.php',                           // parent menu slug or supply `array` (can contains 'icon_url' & 'position') for top level menu
		'use_auto_group_naming' => true,                                   // default to true
		'use_util_menu'         => true,                                   // default to true, shows utility menu
		'minimum_role'          => 'edit_theme_options',                   // default to 'edit_theme_options'
		'layout'                => 'fixed',                                // fluid or fixed, default to fixed
		'page_title'            => __( 'Theme Options', 'indecon' ), // page title
		'menu_label'            => __( 'Theme Options', 'indecon' ), // menu label
	));
}

/**
 * Define Image Size
 *
 * @return void
 * @author alispx
 **/
add_action( 'after_setup_theme', 'indecon_image_size', 30 );
function indecon_image_size() {
	add_image_size( 'small', 50, 50, true );
	add_image_size( 'blog-tiny', 221, 124, true );
	add_image_size( 'blog-small', 303, 171, true );
	add_image_size( 'blog-thumbnail', 637, 358, true );
	add_image_size( 'network-thumbnail', 301, 169, true );
	add_image_size( 'team-thumbnail', 137, 137, true );
	add_image_size( 'learning-thumbnail', 637, 358, true );
	add_image_size( 'banner-widget', 273, 175, true );
	add_image_size( 'network_front_profile', 387, 387, true );
}

/**
 * Admin Head script
 *
 * @return void
 * @author 
 **/
add_action( 'admin_head', 'indecon_metabox_admin_script', 80 );
function indecon_metabox_admin_script() {
	?>
	
	<script>
		jQuery(document).ready(function($) {
			if($('#_publication_categories').val()=="photos"){$("#gallery-metabox").slideDown();}else{$("#gallery-metabox").slideUp();}
			if($('#_publication_categories').val()=="videos"){$("#publication_2").slideDown();}else{$("#publication_2").slideUp();}

			$('#_publication_categories').change(function(){
				if($(this).val()=="photos"){
					$("#gallery-metabox").slideDown();
				}else{
					$("#gallery-metabox").slideUp();
				}
			});

			$('#_publication_categories').change(function(){
				if($(this).val()=="videos"){
					$("#publication_2").slideDown();
				}else{
					$("#publication_2").slideUp();
				}
			});


		});
	</script>

<?php }

/**
 * Prevent plugin update
 *
 * @return void
 * @author alispx
 **/
add_filter('pre_site_transient_update_core','__return_null');
add_filter('pre_site_transient_update_plugins','__return_null');
add_filter('pre_site_transient_update_themes','__return_null');