<?php

/**
 * Additional core function 
 *
 * @return void
 * @author alispx
 **/
function indecon_site_logo() {

	echo '<div class="logo">'; ?>
		<a href="<?php echo home_url(); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-indecon.png" alt="Indecon Logo">
		</a>
		<p class="tagline"><?php echo bloginfo( 'description' ); ?></p>
	<?php		
	echo '</div>';
}


//attachment helper function   
function insert_attachment( $file_handler, $post_id, $setthumb = 'false' ) {
    if ( $_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) { return __return_false(); 
    } 
    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attach_id = media_handle_upload( $file_handler, $post_id );
    //set post thumbnail if setthumb is 1
    if ( $setthumb == 1 ) update_post_meta( $post_id, '_thumbnail_id', $attach_id );
    return $attach_id;
}

/**
 * Redirect non-admin users to home page
 *
 * This function is attached to the 'admin_init' action hook.
 */
// add_action( 'admin_init', 'redirect_non_admin_users' );
// function redirect_non_admin_users() {
// 	if ( ! current_user_can( 'manage_options' ) && '/wp-admin/admin-ajax.php' != $_SERVER['PHP_SELF'] ) {
// 		wp_redirect( home_url() );
// 		exit;
// 	}
// }

/**
 * Print custom favicon on header
 *
 * @return void
 * @author alispx
 **/
add_action( 'wp_head', 'custom_favicon', 20 );
function custom_favicon() {
	if ( indecon_option( 'custom_facivon' ) ) { ?>
		<link rel="shortcut icon" href="<?php echo esc_attr( indecon_option( 'custom_facivon' ) ); ?>" />
	<?php } else {?>
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
	<?php }
}

/**
 * Print custom header script
 *
 * @return void
 * @author alispx
 **/
add_action( 'wp_head', 'custom_header_script', 60 );
function custom_header_script() {
	if ( indecon_option( 'custom_header_script' ) ) {
		$header_script = "\n" . '<style type="text/javascript">' .indecon_option( 'custom_header_script' ). '</style>' . "\n";
		echo $header_script;
	}
}

/**
 * Print custom footer script
 *
 * @return void
 * @author alispx
 **/
add_action( 'wp_footer', 'custom_footer_script', 60 );
function custom_footer_script() {
	if ( indecon_option( 'custom_footer_script' ) ) {
		$footer_script = "\n" . '<style type="text/javascript">' .indecon_option( 'custom_footer_script' ). '</style>' . "\n";
		echo $footer_script;
	}
}

/**
 * Print custom footer script
 *
 * @return void
 * @author alispx
 **/
add_action( 'wp_head', 'google_analytic_script', 70 );
function google_analytic_script() {
	if ( indecon_option( 'google_analytic_code' ) ) {
		$google_script = "\n" . '<style type="text/javascript">' .indecon_option( 'google_analytic_code' ). '</style>' . "\n";
		echo $google_script;
	}
}

function indecon_select_language() {
	if ( !function_exists( "qtrans_getSortedLanguages" ) ) {
		// plugin qTranslate must be active
		echo '<!-- Please activate the qTranslate plugin -->';
		return;
	}
	global $q_config;
	if( is_404() ) {
		$url = get_option( 'home' );
	} else {
		$url = '';
	}
	$languages = qtrans_getSortedLanguages( true );
	foreach( $languages as $language ) {
		$classes = array( 'language', 'lang-'.$language );
		// add an extra style class to the active language
		if( $language == $q_config[ 'language' ] ) {
			$classes[] = 'active';
		}
		echo '<a href="'.qtrans_convertURL( $url, $language ).'"';
		echo ' hreflang="' . $language . '" title="' . $q_config[ 'language_name' ][ $language ] . '">';
		echo '<img src="' . get_stylesheet_directory_uri() . '/images/flags/' . $language . '.png" alt="' . $q_config[ 'language_name' ][ $language ] . '" /> '.$q_config['language_name'][$language].'</a>' . "\n";
	}
}

/**
 * Wrapping get_post_meta function 
 *
 * @return void
 * @author matamerah
 **/
function indecon_post_meta( $key = '' ) {
	$the_meta = get_post_meta( get_the_ID(), $key, true );
	
	if ( ! empty( $the_meta ) ) {
		echo $the_meta;
	} else {
		echo '';
	}

}

// Numeric Page Navi (built into the theme by default)
function indecon_pagination( $before = '', $after = '' ) {
	global $wpdb, $wp_query;
	$request 		= $wp_query->request;
	$posts_per_page = intval( get_query_var( 'posts_per_page' ) );
	$paged 			= intval( get_query_var( 'paged' ) );
	$numposts 		= $wp_query->found_posts;
	$max_page 		= $wp_query->max_num_pages;

	if ( $numposts <= $posts_per_page ) { return; }
	if ( empty( $paged ) || $paged == 0 ) {
		$paged = 1;
	}

	$pages_to_show 			= 7;
	$pages_to_show_minus_1 	= $pages_to_show - 1;
	$half_page_start 		= floor( $pages_to_show_minus_1/2 );
	$half_page_end 			= ceil( $pages_to_show_minus_1/2 );
	$start_page 			= $paged - $half_page_start;

	if ( $start_page <= 0 ) {
		$start_page = 1;
	}

	$end_page = $paged + $half_page_end;
	if ( ( $end_page - $start_page ) != $pages_to_show_minus_1 ) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}
	if ( $end_page > $max_page ) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page 	= $max_page;
	}
	if ( $start_page <= 0 ) {
		$start_page = 1;
	}
		
	echo $before.'<ul class="pagination">'."";
	if ( $paged > 1) {
		$first_page_text = "&laquo";
		echo '<li class="prev"><a href="'.get_pagenum_link().'" title="First">'.$first_page_text.'</a></li>';
	}
		
	$prevposts = get_previous_posts_link( '&larr; Previous' );
	if ( $prevposts) { echo '<li>' . $prevposts  . '</li>'; }
	else { echo '<li class="disabled"><a href="#">&larr; Previous</a></li>'; }
	
	for ( $i = $start_page; $i  <= $end_page; $i++ ) {
		if ( $i == $paged ) {
			echo '<li class="active"><a href="#">'.$i.'</a></li>';
		} else {
			echo '<li><a href="'.get_pagenum_link( $i ).'">'.$i.'</a></li>';
		}
	}
	echo '<li class="">';
	next_posts_link( 'Next &rarr;' );
	echo '</li>';
	if ( $end_page < $max_page ) {
		$last_page_text = "&raquo;";
		echo '<li class="next"><a href="'.get_pagenum_link( $max_page ).'" title="Last">'.$last_page_text.'</a></li>';
	}
	echo '</ul>'.$after."";
}

/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 * 
 * @since 2.0
 */
add_filter( 'wp_title', 'indecon_wp_title', 10, 2 );
function indecon_wp_title( $title, $sep ) {
	global $page, $paged;

	if ( is_feed() ) {
		return $title;
	}

	// Add the blog name
	$title .= get_bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title .= " $sep $site_description";
	}

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 ) {
		$title .= " $sep " . sprintf( __( 'Page %s', 'tokokoo' ), max( $paged, $page ) );
	}

	return $title;
}