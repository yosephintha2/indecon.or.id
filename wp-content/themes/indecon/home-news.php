<?php

/**
 * The Template for displaying home news section
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="section">
	<div class="row">
		<div class="grid-4 column">
			<h5 class="title-gray"><?php _e( 'News', 'indecon' ); ?>
				<a class="link-orange pull-right" href="<?php echo get_category_link( indecon_option( 'news_category_target' ) ); ?>"><?php _e( '+ view all', 'indecon' ); ?></a>
			</h5>

			<?php
				$post_category 	= indecon_option( 'news_category_target' );
				$news_cat 		= ( ! empty( $post_category ) ) ? indecon_option( 'news_category_target' ) : 1;
				$news_args = array(
					'post_type' 		=> 'post',
					'posts_per_page' 	=> 1,
					'orderby' 			=> 'date',
					'order' 			=> 'DESC',
					'cat' 				=> $news_cat
					);
				$news = new WP_Query( $news_args );
			 ?>
			 <?php if ( $news->have_posts() ) : while ( $news->have_posts() ) : $news->the_post(); ?>
				
				<a href="<?php the_permalink(); ?>">
					<?php if ( has_post_thumbnail() ) : ?>
						<?php the_post_thumbnail( 'blog-small' ); ?>
					<?php endif; ?>
				</a>
				<h5 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
				<p>
					<?php echo wp_trim_words( $post->post_content, 18 ); ?>
					<a class="read-more" href="<?php the_permalink(); ?>"><?php _e( 'Read More', 'indecon' ); ?></a>
				</p>
			 <?php endwhile; ?>
			 <?php else: ?>
					<p><?php _e( 'No Post Available', 'indecon' ); ?></p>
			 <?php endif; wp_reset_postdata(); ?>
		</div>

		<div class="grid-4 column">
			<h5 class="title-gray"><?php _e( 'Activities ', 'indecon' ); ?>
				<a class="link-orange pull-right" href="<?php echo get_permalink( indecon_option( 'activities_page_target' ) ); ?>"><?php _e( '+ view all', 'indecon' ); ?></a>
			</h5>

			<?php
				$act_category 			= indecon_option( 'activities_category_target' );
				$activities_cat 		= ( ! empty( $post_category ) ) ? indecon_option( 'activities_category_target' ) : 1;
				$act_args = array(
					'post_type' 		=> 'activity',
					'posts_per_page' 	=> 1,
					'orderby' 			=> 'date',
					'order' 			=> 'DESC',
					);

				$activities = new WP_Query( $act_args );
			 ?>
			 <?php if ( $activities->have_posts() ) : while ( $activities->have_posts() ) : $activities->the_post(); ?>
				
				<a href="<?php the_permalink(); ?>">
					<?php if ( has_post_thumbnail() ) : ?>
						<?php the_post_thumbnail( 'blog-small' ); ?>
					<?php endif; ?>
				</a>
				
				<h5 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
				<p>
					<?php echo wpautop( wp_trim_words( $post->post_content, 18 ) ); ?>   
					<a class="read-more" href="<?php the_permalink(); ?>"><?php _e( 'Read More', 'indecon' ); ?></a>
				</p>
			 <?php endwhile; ?>
			 <?php else: ?>
					<p><?php _e( 'No Post Available', 'indecon' ); ?></p>
			 <?php endif; wp_reset_postdata(); ?>
		</div>

		<div class="grid-4 column">
			<h5 class="title-gray"><?php _e( 'Media & Publication', 'indecon' ); ?>
			<a class="link-orange pull-right" href="<?php echo get_permalink( indecon_option( 'media_page_target' ) ); ?>"><?php _e( '+ view all', 'indecon' ); ?></a></h5>
			
			<?php
				$media_category 	= indecon_option( 'media_category_target' );
				$media_cat 			= ( ! empty( $post_category ) ) ? indecon_option( 'media_category_target' ) : 1;
				$media_args = array(
					'post_type' 		=> 'publication',
					'posts_per_page' 	=> 1,
					'orderby' 			=> 'date',
					'order' 			=> 'DESC',
					);

				$media = new WP_Query( $media_args );
			 ?>
			 <?php if ( $media->have_posts() ) : while ( $media->have_posts() ) : $media->the_post(); ?>
				
				<?php 
					$publication_categories = get_post_meta( get_the_ID(), '_publication_categories', true );
					
				if ( 'videos' == $publication_categories ) { ?>

					<?php 
						$youtube_video_url = get_post_meta( get_the_ID(), '_youtube_video_url', true );
						$video_url = esc_url( $youtube_video_url );
						echo do_shortcode( "[video width='303' height='171' src='{$video_url}']" );
					?>

					<h5 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
					
					<p>
						<?php echo wp_trim_words( $post->post_content, 18 ); ?>
						<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read More', 'indecon' ); ?></a>
					</p>

				<?php } else if ( 'photos' == $publication_categories ) { ?>

					<a href="<?php the_permalink(); ?>">
						<?php if ( has_post_thumbnail() ) : ?>
							<?php the_post_thumbnail( 'blog-small' ); ?>
						<?php endif; ?>
					</a>
							
					<h5 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
					
					<p>
						<?php echo wp_trim_words( $post->post_content, 18 ); ?>
						<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read More', 'indecon' ); ?></a>
					</p>
					
				<?php } else { ?>

					<h5 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>

					<p>
						<?php echo wp_trim_words( $post->post_content, 10 ); ?>
						<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read More', 'indecon' ); ?></a>
					</p>
					
				<?php }

				 ?>
			 <?php endwhile; ?>
			 <?php else: ?>
					<p><?php _e( 'No Post Available', 'indecon' ); ?></p>
			 <?php endif; wp_reset_postdata(); ?>

		</div>
	</div>
</div>