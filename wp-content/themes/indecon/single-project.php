<?php

/**
 * The Template for displaying single project
 *
 * @author 		alispx
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header(); ?>
	
	<div class="row content">

		<div class="grid-8 column post-content">
	
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<h3 class="title-post"><?php the_title() ?></h3>
					<p class="date">
						<?php printf( 'Since %s', get_the_time() ); ?>
					</p>

					<div class="framebox">
						<?php if ( has_post_thumbnail() ) : ?>
							<?php the_post_thumbnail( 'blog-thumbnail' ); ?>
						<?php endif; ?>
					</div>

					 <div class="entry-content">
						<?php the_content(); ?>
					 </div>

				</article>
				
			<?php endwhile; ?>
			<?php endif; ?>
		
		</div>
		
		<?php get_sidebar( 'primary' ); ?>

	</div>

<?php get_footer(); ?>