<?php 
/*
Template Name: Home
*/
get_header(); // Loads the header.php template. ?>

		<?php get_template_part( 'home', 'slider' ); ?>
		<?php get_template_part( 'home', 'research' ); ?>
		<?php get_template_part( 'home', 'news' ); ?>
		<?php get_template_part( 'home', 'footprint' ); ?>
		<?php get_template_part( 'home', 'partner' ); ?>

<?php get_footer(); // Loads the footer.php template. ?>