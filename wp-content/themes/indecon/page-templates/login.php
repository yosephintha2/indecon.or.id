<?php

/**
 * Template Name: Login
 *
 *
 */

global $user_ID;

if ( ! $user_ID ) {

	if ( $_POST ) {
		//We shall SQL escape all inputs
		$username = esc_sql( $_REQUEST['username'] );
		$password = esc_sql( $_REQUEST['password'] );
		$remember = ( isset( $_POST['rememberme'] ) ) ? $_POST['rememberme'] : false;
	

		$login_data 					= array();
		$login_data['user_login'] 		= $username;
		$login_data['user_password'] 	= $password;
		$login_data['remember'] 		= $remember;
		$user_verify 					= wp_signon( $login_data, false ); 
		//wp_signon is a wordpress function which authenticates a user. It accepts user info parameters as an array.
		
		if ( is_wp_error( $user_verify ) ) {
		   echo "<span class='error'>Invalid username or password. Please try again!</span>";
		   exit();
		} else {	
			echo "<script type='text/javascript'>window.location='". home_url( get_the_title( indecon_option( 'user_after_login' ) ) ) ."'</script>";
			exit();
		}
	} else { 

get_header(); ?>

<div class="row content">
	<div class="section-fixcenter text-center">

		<div id="result"></div> <!-- To hold validation results -->
		<form id="wp_login_form" action="" method="post">
			<h3 class="title-gray"><?php _e( 'Login Member', 'indecon' ); ?></h3>
			<label class="text-left"><?php _e( 'Username:', 'indecon' ); ?></label>
			<input name="username" type="text" class="input-block">
			<label class="text-left"><?php _e( 'Password:', 'indecon' ); ?></label>
			<input name="password" type="password" class="input-block">
			<div class="clearfix">
				<label class="checkbox pull-left">
					<input name="rememberme" type="checkbox" value="forever"> <?php _e( 'Remember me', 'indecon' ); ?>
					</label>
				<div class="pull-right"><a class="link-green" href="<?php echo home_url( '/wp-login.php?action=lostpassword' ); ?>"><?php _e( 'Forgot password?', 'indecon' ); ?></a></div>
			</div>
			<button name="wp-submit" id="submitbtn" class="button button-primary"><?php _e( 'Login Account', 'indecon' ); ?></button>

		</form>
		
	</div>
</div>

<script type="text/javascript">  
	$ = jQuery.noConflict();						
	$("#submitbtn").click(function() {

	$('#result').html('<img src="<?php echo get_template_directory_uri(); ?>/images/loader.gif" class="loader" />').fadeIn();
	
	var input_data = $('#wp_login_form').serialize();
	$.ajax({
	type: "POST",
	url:  "<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>",
	data: input_data,
	success: function(msg){
	$('.loader').remove();
	$('<div>').html(msg).appendTo('div#result').hide().fadeIn('slow');
	}
	});
	return false;

	});
</script>

<?php get_footer(); } 

} else {
	echo "<script type='text/javascript'>window.location='". home_url() ."'</script>";
}

?>