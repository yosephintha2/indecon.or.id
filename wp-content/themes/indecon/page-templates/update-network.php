<?php

/*
Template Name: Update Network
*/


get_header(); 

if ( is_user_logged_in() ) {

$query = new WP_Query( array( 'post_type' => 'network', 'posts_per_page' => 1 ) );

if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
	
	if(isset($_GET['post'])) {
		
		if($_GET['post'] == $post->ID)
		{
			$current_post = $post->ID;

			$title = get_the_title();
			$content = get_the_content();

		}
	}


endwhile; endif;
wp_reset_query();

global $current_post;

$postTitleError = '';

$website 			= ( isset( $_POST['website'] ) ) ? $_POST['website'] : get_post_meta( $current_post, 'website_url', true );
$description 		= ( isset( $_POST['description'] ) ) ? $_POST['description'] : get_post_meta( $current_post, 'description', true );
$fullname 			= ( isset( $_POST['fullname'] ) ) ? $_POST['fullname'] : get_post_meta( $current_post, 'full_name', true );
$organization 		= ( isset( $_POST['organization'] ) ) ? $_POST['organization'] : get_post_meta( $current_post, 'organization', true );
$profession 		= ( isset( $_POST['profession'] ) ) ? $_POST['profession'] : get_post_meta( $current_post, 'profession', true );
$division 			= ( isset( $_POST['division'] ) ) ? $_POST['division'] : get_post_meta( $current_post, 'division', true );
$address 			= ( isset( $_POST['address'] ) ) ? $_POST['address'] : get_post_meta( $current_post, 'full_address', true );
$phone_number		= ( isset( $_POST['phone_number'] ) ) ? $_POST['phone_number'] : get_post_meta( $current_post, 'phone_number', true );
$motivasi			= ( isset( $_POST['motivation'] ) ) ? $_POST['motivation'] : get_post_meta( $current_post, 'motivation', true );
$contact_of_pj		= ( isset( $_POST['pj_contact'] ) ) ? $_POST['pj_contact'] : get_post_meta( $current_post, 'pj_contact', true );
$position_of_pj		= ( isset( $_POST['pj_position'] ) ) ? $_POST['pj_position'] : get_post_meta( $current_post, 'pj_position', true );
$personal_address	= ( isset( $_POST['personal_address'] ) ) ? $_POST['personal_address'] : get_post_meta( $current_post, 'personal_address', true );
$phone_of_pj		= ( isset( $_POST['pj_phone'] ) ) ? $_POST['pj_phone'] : get_post_meta( $current_post, 'pj_phone', true );
$email_of_pj		= ( isset( $_POST['pj_email'] ) ) ? $_POST['pj_email'] : get_post_meta( $current_post, 'pj_email', true );
$gambar_profile		= ( isset( $_POST['thumbnail'] ) ) ? $_POST['thumbnail'] : '';
$member_type		= ( isset( $_POST['member_type'] ) ) ? $_POST['member_type'] : get_post_meta( $current_post, 'member-type', true );

if ( isset($_POST['submitted']) && isset($_POST['network_nonce_field']) && wp_verify_nonce( $_POST['network_nonce_field'], 'network_nonce' )) {

	
	$post = array( //our wp_insert_post args
				'ID' 			=> $current_post,
				'post_title'	=> wp_strip_all_tags( $organization ),
				'post_status'	=> 'publish',
				'post_type' 	=> 'network',

			);
		 
	$my_post_id = wp_update_post( $post ); //send our post, save the resulting ID

	if ( $my_post_id ) {
		update_post_meta( $my_post_id, 'member-type', $member_type );
		update_post_meta( $my_post_id, 'full_name', esc_attr( $fullname ) );
		update_post_meta( $my_post_id, 'organization', esc_attr( $organization ) );
		update_post_meta( $my_post_id, 'profession', esc_attr( $profession ) );
		update_post_meta( $my_post_id, 'division', esc_attr( $division ) );
		update_post_meta( $my_post_id, 'full_address', esc_attr( $address ) );
		update_post_meta( $my_post_id, 'phone_number', esc_attr( $phone_number ) );
		update_post_meta( $my_post_id, 'motivation', esc_attr( $motivasi ) );
		update_post_meta( $my_post_id, 'description', esc_attr( $description ) );
		update_post_meta( $my_post_id, 'website_url', esc_attr( $website ) );

		update_post_meta( $my_post_id, 'pj_contact', esc_attr( $contact_of_pj ) );
		update_post_meta( $my_post_id, 'personal_address', esc_attr( $personal_address ) );
		update_post_meta( $my_post_id, 'pj_position', esc_attr( $position_of_pj ) );
		update_post_meta( $my_post_id, 'pj_phone', esc_attr( $phone_of_pj ) );
		update_post_meta( $my_post_id, 'pj_email', esc_attr( $email_of_pj ) );

		 if (!function_exists('wp_generate_attachment_metadata')){
		    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
		    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
		    require_once(ABSPATH . "wp-admin" . '/includes/media.php');
		}
		if ( isset ($_FILES['thumbnail']) && $_FILES['thumbnail']['error'] == UPLOAD_ERR_OK ) { 
			$attach_id = media_handle_upload( 'thumbnail', $my_post_id );
			update_post_meta( $my_post_id,'_thumbnail_id', $attach_id ); 
		}

		$error = '';
		$success = '';
		if ( is_wp_error( $my_post_id ) ) {
			$error = 'Error on update data.';
		} else {
			$dashboard_user = get_permalink( indecon_option( 'user_dashboard' ) );
			$back_to = '<a href="'.$dashboard_user.'"> '.__( 'back to profile page', 'indecon' ).'</a>';
			$success = 'Your data updated successfully' . $back_to;
		}
		
	}


} ?>

		<div class="row content">
			<div class="grid-10 offset-grid-1 column">
				
				<!--display error/success message-->
				<div id="message">
					<?php 
						if ( ! empty( $error ) ) :
							echo '<p class="error">'.$error.'</p>';
						endif;
					?>
					
					<?php 
						if ( ! empty( $success ) ) :
							echo '<p class="error">'.$success.'</p>';
						endif;
					?>
				</div>
				
				<div id="result"></div> <!-- To hold validation results -->
				
				<form method="post" enctype="multipart/form-data">
						
						<h3 class="title-gray"><?php _e( 'Update Community', 'indecon' ); ?></h3>
						<hr>
						<h4 class="title-gray"><?php _e( 'Tipe Keanggotaan', 'indecon' ); ?></h4>
						<div class="row">
							<div class="grid-6 column">
								<?php $type = get_post_meta( $current_post, 'member-type', true ); ?>

								<label><?php _e( 'Pilih tipe keanggotaan :', 'indecon' ); ?></label>
								<input type="radio" name="member_type" value="individu" <?php checked( $type, 'individu' ); ?>>
								<?php _e( 'Profesional/Individu', 'indecon' ); ?>
								<input id="oraganisasi" type="radio" name="member_type" value="organisasi" <?php checked( $type, 'organisasi' ); ?>>
								<?php _e( 'Organisasi/Lembaga', 'indecon' ); ?>
							</div>
						</div>

						<hr>
						<h4 class="title-gray"><?php _e( 'Personal Information', 'indecon' ); ?></h4>
						<div class="row">
							<div class="grid-6 column">
								<label><?php _e( 'Nama Lengkap :', 'indecon' ); ?> </label>
								<input name="fullname" type="text" class="input-block" value="<?php echo $fullname; ?>">
							</div>
							<div class="grid-6 column">
								<label><?php _e( 'Profesi :', 'indecon' ); ?></label>
								<input name="profession" type="text" class="input-block" value="<?php echo $profession; ?>">
							</div>	
							<div class="grid-6 column">
								<label><?php _e( 'Bidang Gerak :', 'indecon' ); ?></label>
								<input name="division" type="text" class="input-block" value="<?php echo $division; ?>">
							</div>	
							<div class="grid-6 column">
								<label><?php _e( 'Organisasi Tempat Bekerja :', 'indecon' ); ?></label>
								<input name="organization" type="text" class="input-block" value="<?php echo $organization; ?>">
							</div>	
							<div class="grid-6 column">
								<label><?php _e( 'Alamat :', 'indecon' ); ?></label>
								<textarea name="address" class="input-block" rows="4"><?php echo $address; ?></textarea>
							</div>	
							<div class="grid-6 column">
								<label><?php _e( 'No tlp/fax/Handphone :', 'indecon' ); ?></label>
								<input name="phone_number" type="text" class="input-block" value="<?php echo $phone_number; ?>">
							</div>	
							<div class="grid-6 column">
								<label><?php _e( 'Website/blog (jika ada) :', 'indecon' ); ?></label>
								<input name="website" type="text" class="input-block" value="<?php echo $website; ?>">
							</div>
							<div class="grid-6 column">
								<label><?php _e( 'Motivasi bergabung:', 'indecon' ); ?></label>
								<textarea name="motivation" class="input-block" rows="4"><?php echo $motivasi; ?></textarea>
							</div>
							<div class="grid-6 column">
								<label><?php _e( 'Deskripsi singkat tentang anda :', 'indecon' ); ?></label>
								<textarea name="description" class="input-block" rows="4"><?php echo $description; ?></textarea>
							</div>
						</div>
						
						<div class="penanggung-jawab">
							<hr>
							<h4 class="title-gray"><?php _e( 'Detil kontak penanggung jawab', 'indecon' ); ?></h4>
							<div class="row">
								<div class="grid-6 column">
									<label><?php _e( 'Nama Kontak ', 'indecon' ); ?>:</label>
									<input name ="pj_contact" type="text" class="input-block" value="<?php echo $contact_of_pj; ?>">
									<label><?php _e( 'Jabatan di Lembaga ', 'indecon' ); ?>:</label>
									<input name="pj_position" type="text" class="input-block" value="<?php echo $position_of_pj; ?>">
								</div>	
								<div class="grid-6 column">
									<label><?php _e( 'Alamat Pribadi ', 'indecon' ); ?>:</label>
									<input name="personal_address" type="text" class="input-block" value="<?php echo $personal_address; ?>">
									<label><?php _e( 'No tlp/fax/Handpone :', 'indecon' ); ?></label>
									<input name="pj_phone" type="text" class="input-block" value="<?php echo $phone_of_pj; ?>">
								</div>
								<div class="grid-6 column">
									<label><?php _e( 'Email :', 'indecon' ); ?></label>
									<input name="pj_email" type="text" class="input-block" value="<?php echo $email_of_pj; ?>">
								</div>
							</div>
						</div>

						<hr>
						<div class="row">
							<div class="grid-5 column">
								<figure class="profile-photos">
									<?php echo get_the_post_thumbnail( $current_post, 'network_front_profile' ); ?>
								</figure>
								<div class="clearfix" style="margin-top: 10px"></div>
								<fieldset class="images">
									<label for="images"><?php _e( 'Gambar Profil', 'indecon' ) ?></label>
									<input type="file" name="thumbnail" size="50">
								</fieldset>
							</div>
						</div>


						<hr>
						<input type="submit" id="submitbtn" name="update_network" class="button button-primary"/>
						<?php wp_nonce_field( 'network_nonce', 'network_nonce_field' ); ?>
						<input type="hidden" name="submitted" id="submitted" value="true" />
				</form>

				</div>	

			</div>

<?php } else { ?>
	<div class="row content">
		<div class="grid-10 offset-grid-1 column">
			<h3><?php _e( '"You must logged in first to access this page"', 'indecon' ); ?></h3>
		</div>
	</div>
<?php } ?>

			
<?php get_footer();  ?>
	

