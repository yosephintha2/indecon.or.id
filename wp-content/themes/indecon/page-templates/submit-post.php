<?php

/*
Template Name: Submit Activities
*/

get_header(); 

if ( is_user_logged_in() ) {
	# code...
$error = '';
$success = '';
$current_user = wp_get_current_user();

$email 				= ( isset( $_POST['email'] ) ) ? $_POST['email'] : '';
$fullname 			= ( isset( $_POST['fullname'] ) ) ? $_POST['fullname'] : '';
$featured_image		= ( isset( $_POST['thumbnail'] ) ) ? $_POST['thumbnail'] : '';
$subject			= ( isset( $_POST['subject'] ) ) ? $_POST['subject'] : '';
$stories			= ( isset( $_POST['stories'] ) ) ? $_POST['stories'] : '';

if ( isset( $_POST['task_activities'] ) && $_POST['task_activities'] == 'submit_activities' ) {
	
	if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
		$error = 'Invalid email address.';
	} else {
		$post = array( //our wp_insert_post args
				'post_title'	=> wp_strip_all_tags( $subject ),
				'post_content' 	=> $stories,
				'post_author' 	=> esc_attr( $current_user->ID ),
				'post_status'	=> 'pending',
				'post_type' 	=> 'activity',

			);
				 
		$my_activity_id = wp_insert_post( $post ); //send our post, save the resulting ID

		if ( $my_activity_id ) {
			
			 if (!function_exists('wp_generate_attachment_metadata')){
				require_once(ABSPATH . "wp-admin" . '/includes/image.php');
				require_once(ABSPATH . "wp-admin" . '/includes/file.php');
				require_once(ABSPATH . "wp-admin" . '/includes/media.php');
			}
			 if ( $_FILES ) {
				foreach ( $_FILES as $file => $array ) {
					if ( $_FILES[$file]['error'] !== UPLOAD_ERR_OK ) {
						return "upload error : " . $_FILES[$file]['error'];
					}
					$attach_id = media_handle_upload( $file, $my_activity_id );
				}   
			}
			if ( $attach_id > 0 ){
				//and if you want to set that image as Post  then use:
				update_post_meta( $my_activity_id,'_thumbnail_id', $attach_id );
			}
		}

		if ( is_wp_error( $my_activity_id ) ) {
			$error = __( 'Failed submitting stories', 'indecon' );
		} else {
			$success = __( 'Congratulation ! your stories successfully submitted', 'indecon' );
		}

	}

} ?>

<div class="row content">
	<div class="grid-10 offset-grid-1 column">
		
		<!--display error/success message-->
		<div id="message">
			<?php 
				if ( ! empty( $error ) ) :
					echo '<p class="error">'.$error.'</p>';
				endif;
			?>
			
			<?php 
				if ( ! empty( $success ) ) :
					echo '<p class="error">'.$success.'</p>';
				endif;
			?>
		</div>
		
		<div id="result"></div> <!-- To hold validation results -->
		
		<form method="post" id="wp_submit_activity" enctype="multipart/form-data">
				
				<h3 class="title-gray"><?php _e( 'Submit Your Stories', 'indecon' ); ?></h3>
				<hr>

				<div class="row">
					<div class="grid-12 column">
						<label><?php _e( 'Nama :', 'indecon' ); ?> <span class="text-error"> *</span></label>
						<input name="fullname" type="text" class="input-block" required value="<?php echo $fullname; ?>">
					</div>
					<div class="grid-12 column">
						<label><?php _e( 'Email :', 'indecon' ); ?><span class="text-error"> *</span></label>
						<input name="email" type="text" class="input-block" required value="<?php echo $email; ?>">
					</div>	
					<div class="grid-12 column">
						<label><?php _e( 'Subject :', 'indecon' ); ?><span class="text-error"> *</span></label>
						<input name="subject" type="text" class="input-block" required value="<?php echo $subject; ?>">
					</div>	

					<div class="grid-4 column">
						<fieldset class="images">
							<label for="images"><?php _e( 'Featured Image', 'indecon' ) ?><span class="text-error"> *</span></label>
							<input type="file" required name="thumbnail" size="50">
						</fieldset>
					</div>

					<div class="grid-12 column">
						<label><?php _e( 'Story :', 'indecon' ); ?><span class="text-error"> *</span></label>
						<?php 
						// default settings - Kv_front_editor.php
						$content = $stories;
						$editor_id = 'stories';
						$settings =   array(
							'wpautop' 		=> true, // use wpautop?
							'media_buttons' => true, // show insert/upload button(s)
							'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
							'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
							'tabindex' 		=> '',
							'editor_css' 	=> '', //  extra styles for both visual and HTML editors buttons, 
							'editor_class' 	=> '', // add extra class(es) to the editor textarea
							'teeny' 		=> false, // output the minimal editor config used in Press This
							'dfw' 			=> false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
							'tinymce' 		=> true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
							'quicktags' 	=> true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
						);
						wp_editor( $content, $editor_id, $settings ); ?>
					</div>	
					
				</div>
				
				<hr>
				<button id="submitbtn" name="submit_activities" class="button button-primary"><?php _e( 'Submit Activities', 'indecon' ) ?></button>
				<input type="hidden" name="task_activities" value="submit_activities" />
		</form>

		</div>	

	</div>

<?php } else { ?>
	
	<div class="row content">
		<div class="grid-10 offset-grid-1 column">
			<h3><?php _e( '"You must logged in first to submit your stories"', 'indecon' ); ?></h3>
		</div>
	</div>

<?php } ?>		
			
<?php get_footer();  ?>
	

